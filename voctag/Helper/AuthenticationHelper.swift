//
//  AuthenticationHelper.swift
//  voctag
//
//  Created by Jan Maron on 13.07.17.
//  Copyright © 2017 voctag. All rights reserved.
//

import Foundation
import JWT
import KeychainAccess

class AuthenticationHelper {
    
    static let shared = AuthenticationHelper()

    fileprivate let KEYCHAIN_SERVICE_KEY = "com.voctag.ios.authenticationservice"
    fileprivate let SECRET = "c12ccc271ef5f8b8edb072a40ad6988f9700c51c25dd096e01107f96b04f22460c790459ef057d2c35379487c1953e4bc32b"
    fileprivate let STAGING_SECRET = "staging_shared_secret"
    
    func getAuthToken() -> String? {
        let keychain = Keychain(service: KEYCHAIN_SERVICE_KEY)
        return keychain[VoctagKeys.AUTHENTICATION_AUTH_TOKEN_KEYCHAIN_ACCESS] 
    }
    
    func saveAuthToken(_ token: String) {
        let keychain = Keychain(service: KEYCHAIN_SERVICE_KEY)
        keychain[VoctagKeys.AUTHENTICATION_AUTH_TOKEN_KEYCHAIN_ACCESS] = token
    }
    
    func encrypt(_ token: String, forKey key: String) -> String {
        var dictionaryToEncode: [String: Any] = [key: token]
        if let user = PersistDataHelper.shared.loadUser() {
            if user.hasLocation {
                dictionaryToEncode[VoctagKeys.LATITUDE] = user.latitude
                dictionaryToEncode[VoctagKeys.LONGITUDE] = user.longitude
            }
        }
        
        if let deviceToken = VoctagAppDelegate.shared.deviceToken {
            dictionaryToEncode[VoctagKeys.APNS_DEVICE_TOKEN] = deviceToken
        }

        return JWT.encode(claims: dictionaryToEncode, algorithm: .hs256(STAGING_SECRET.data(using: .utf8)!))
    }
    
}

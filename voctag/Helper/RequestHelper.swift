//
//  RequestHelper.swift
//  voctag
//
//  Created by Jan Maron on 10.07.17.
//  Copyright © 2017 voctag. All rights reserved.
//

import Foundation
import Alamofire

class RequestHelper {
    
    fileprivate struct VoctagRequest {
        let requestType: RequestType
        let parameters: [String: Any]?
        let success: (([String: Any]?) -> Void)?
        let error: ((HTTPURLResponse?) -> Void)?
    }
    
    enum RequestType {
        case register
        case refresh
        case loadUser
        case acceptTerms
        case registerNickname
        case loadVocDetail(Int)
        case upload
        case search
        case played(Int)
        case upVote(Int)
        case downVote(Int)
        case follow(Int)
        case unfollow(Int)
        case report(Int)
        case delete(Int)
        case getClientVocs(String)
        case loadClientVocDetail(String, Int)
        case getClients
        case uploadClientVoc(String)
        case followClient(String)
        case unfollowClient(String)
        case redeemAccessCode(String)
        
        func url() -> String {
            switch self {
            case .register:
                return "auth/register"
            case .refresh:
                return "auth/refresh"
            case .loadUser:
                return "users/me"
            case .acceptTerms:
                return "users/me/accept_terms"
            case .registerNickname:
                return "users/me"
            case .loadVocDetail(let vocID):
                return "vocs/\(vocID)"
            case .upload:
                return "vocs"
            case .search:
                return "vocs/search"
            case .played(let vocID):
                return "vocs/\(vocID)/notify_played"
            case .upVote(let vocID):
                return "vocs/\(vocID)/vote_up"
            case .downVote(let vocID):
                return "vocs/\(vocID)/vote_down"
            case .follow(let vocID):
                return "vocs/\(vocID)/follow"
            case .unfollow(let vocID):
                return "vocs/\(vocID)/unfollow"
            case .report(let vocID):
                return "vocs/\(vocID)/report"
            case .delete(let vocID):
                return "vocs/\(vocID)"
            case .getClientVocs(let slug):
                return "clients/\(slug)/vocs"
            case .loadClientVocDetail(let slug, let vocID):
                return "clients/\(slug)/vocs/\(vocID)"
            case .getClients:
                return "clients"
            case .uploadClientVoc(let slug):
                return "clients/\(slug)/vocs"
            case .followClient(let slug):
                return "clients/\(slug)/membership"
            case .unfollowClient(let slug):
                return "clients/\(slug)/membership"
            case .redeemAccessCode(let slug):
                return "clients/\(slug)/access"
            }
        }
        
        func method() -> Alamofire.HTTPMethod {
            switch self {
            case .acceptTerms, .downVote, .follow, .unfollow, .played, .report, .upVote, .registerNickname:
                return .patch
            case .refresh, .register, .search, .upload, .uploadClientVoc, .followClient, .redeemAccessCode:
                return .post
            case .loadVocDetail, .loadUser, .getClients, .getClientVocs, .loadClientVocDetail:
                return .get
            case .delete, .unfollowClient:
                return .delete
            }
        }
    }
    
    fileprivate let TIMEOUT_INTERVAL = 300
    fileprivate static let API_VERSION = "v2/"
    fileprivate let STAGING_URL = "https://api.staging.upspeak.de/\(API_VERSION)"
    fileprivate let VOCTAG_URL = "https://api.upspeak.de/\(API_VERSION)"
    
    static let shared = RequestHelper()
    
    // MARK: - Auth Requests

    //Register Device
    fileprivate func register(WithVoctagRequest voctagRequest: VoctagRequest) {
        guard let deviceID = UIDevice.current.identifierForVendor?.uuidString else {
            LoggingHelper.logError(ForClass: RequestHelper.self, with: "identifierForVendor could not be retrieved")
            voctagRequest.error?(nil)
            return
        }
        
        let encryptedDeviceToken = AuthenticationHelper.shared.encrypt(deviceID, forKey: VoctagKeys.DEVICE_ID)
        let registerParameters: [String: Any] = [VoctagKeys.TOKEN: encryptedDeviceToken]
        let urlRequest = buildURLRequest(WithRequestType: .register, parameters: registerParameters)

        Alamofire.request(urlRequest).responseJSON { response in
            if response.result.isFailure {
                if let statusCode = response.response?.statusCode {
                    LoggingHelper.logError(ForRequest: response.request!, error: response.result.error, statusCode: statusCode)
                }
                voctagRequest.error?(response.response)
            } else {
                if let result = response.result.value as? [String: Any] {
                    if let dataObject = self.getDataObject(FromJson: result) {
                        let auth = Authentication.init(fromDataObject: dataObject)
                        PersistDataHelper.shared.saveAuthentication(auth)
                        PersistDataHelper.shared.saveUser(User.init(forUserID: auth.userID))
                        self.request(WithVoctagRequest: voctagRequest)
                        return
                    }
                }
                voctagRequest.error?(response.response)
            }
        }
    }
    
    //Refresh Auth Token
    fileprivate func refresh(WithVoctagRequest voctagRequest: VoctagRequest) {
        guard let refreshToken = PersistDataHelper.shared.loadAuthentication()?.refreshToken else {
            LoggingHelper.logError(ForClass: RequestHelper.self, with: "No refresh token")
            self.register(WithVoctagRequest: voctagRequest)
            return
        }
        
        let encryptedRefreshToken = AuthenticationHelper.shared.encrypt(refreshToken, forKey: VoctagKeys.REFRESH_TOKEN)
        let refreshParameters: [String: Any] = [VoctagKeys.TOKEN: encryptedRefreshToken]
        let urlRequest = buildURLRequest(WithRequestType: .refresh, parameters: refreshParameters)

        Alamofire.request(urlRequest).responseJSON { response in
            if response.result.isFailure {
                if let statusCode = response.response?.statusCode {
                    if statusCode == 401 {
                        //refreshtoken is invalid and need to be refreshed
                        self.register(WithVoctagRequest: voctagRequest)
                        return
                    }
                    LoggingHelper.logError(ForRequest: response.request!, error: response.result.error, statusCode: statusCode)
                }
                voctagRequest.error?(response.response)
            } else {
                if let result = response.result.value as? [String: Any] {
                    if let dataObject = self.getDataObject(FromJson: result) {
                        let auth = Authentication.init(fromDataObject: dataObject)
                        PersistDataHelper.shared.saveAuthentication(auth)
                        self.request(WithVoctagRequest: voctagRequest)
                        return
                    }
                }
                voctagRequest.error?(response.response)
            }
        }
    }
    
    //Register Device Token for Notifications
    func registerDeviceToken() {
        guard let refreshToken = PersistDataHelper.shared.loadAuthentication()?.refreshToken else {
            LoggingHelper.logError(ForClass: RequestHelper.self, with: "No refresh token")
            return
        }
        
        let encryptedRefreshToken = AuthenticationHelper.shared.encrypt(refreshToken, forKey: VoctagKeys.REFRESH_TOKEN)
        let refreshParameters: [String: Any] = [VoctagKeys.TOKEN: encryptedRefreshToken]
        let urlRequest = buildURLRequest(WithRequestType: .refresh, parameters: refreshParameters)
        
        Alamofire.request(urlRequest).responseJSON { response in
            if response.result.isFailure {
                if let statusCode = response.response?.statusCode {
                    LoggingHelper.logError(ForRequest: response.request!, error: response.result.error, statusCode: statusCode)
                }
            } else {
                if let result = response.result.value as? [String: Any] {
                    if let dataObject = self.getDataObject(FromJson: result) {
                        let auth = Authentication.init(fromDataObject: dataObject)
                        PersistDataHelper.shared.saveAuthentication(auth)
                    }
                }
            }
        }
    }

    // MARK: - User Requests

    //Load User and save to persist data
    func loadUser(successListener: @escaping (User) -> Void) {
        request(WithVoctagRequest: VoctagRequest.init(requestType: .loadUser, parameters: nil, success: { (json) in
            if let dataObject = self.getDataObject(FromJson: json) {
                let user = User.init(fromDataObject: dataObject)
                PersistDataHelper.shared.saveUser(user)
                successListener(user)
            }
        }, error: nil))
    }
    
    //Accept Terms
    func acceptTerms(completion: @escaping (Bool) -> Void) {
        request(WithVoctagRequest: VoctagRequest.init(requestType: .acceptTerms, parameters: nil, success: { (json) in
            if let dataObject = self.getDataObject(FromJson: json) {
                let user = User.init(fromDataObject: dataObject)
                PersistDataHelper.shared.saveUser(user)
                completion(true)
            } else {
                completion(false)
            }
        }) { _ in
            completion(false)
        })
    }
    
    //Register Nickname
    func registerNickname(nickname: String, successListener: @escaping () -> Void, errorListener: @escaping (Bool) -> Void) {
        let parameters = [VoctagKeys.USER_NICKNAME: nickname]
        request(WithVoctagRequest: VoctagRequest.init(requestType: .registerNickname, parameters: parameters, success: { _ in
            successListener()
        }) { response in
            if let statusCode = response?.statusCode, statusCode == 422 {
                errorListener(true)
            } else {
                errorListener(false)
            }
        })
    }
    
    // MARK: - Voc Requests

    //Load Voc Detail
    func loadVocDetail(id: Int, completion: @escaping (Voc?) -> Void) {
        request(WithVoctagRequest: VoctagRequest.init(requestType: .loadVocDetail(id), parameters: nil, success: { (json) in
            if let dataObject = self.getDataObject(FromJson: json) {
                completion(Voc.init(fromDataObject: dataObject))
            } else {
                completion(nil)
            }
        }) { _ in
            completion(nil)
        })
    }
    
    //Upload Voc
    func uploadVoc(parameters: [String: Any], success: @escaping (Voc) -> Void, error: @escaping () -> Void) {
        request(WithVoctagRequest: VoctagRequest.init(requestType: .upload, parameters: parameters, success: { (json) in
            if let dataObject = self.getDataObject(FromJson: json) {
                success(Voc.init(fromDataObject: dataObject))
            } else {
                error()
            }
        }) { _ in
            error()
        })
    }

    //Search Vocs
    func searchVocs(parameters: [String: Any], completion: @escaping ([Voc]?) -> Void) {
        request(WithVoctagRequest: VoctagRequest.init(requestType: .search, parameters: parameters, success: { (json) in
            if let dataArray = self.getDataArray(FromJson: json) {
                completion(Voc.createArray(fromDataArray: dataArray))
            } else {
                completion(nil)
            }
        }) { _ in
            completion(nil)
        })
    }

    //Voc played
    func played(VocWithID id: Int) {
        request(WithVoctagRequest: VoctagRequest.init(requestType: .played(id), parameters: nil, success: nil, error: nil))
    }
    
    //UpVote Voc
    func upVote(VocWithID id: Int) {
        request(WithVoctagRequest: VoctagRequest.init(requestType: .upVote(id), parameters: nil, success: nil, error: nil))
    }
    
    //DownVote Voc
    func downVote(VocWithID id: Int) {
        request(WithVoctagRequest: VoctagRequest.init(requestType: .downVote(id), parameters: nil, success: nil, error: nil))
    }

    //Follow Voc
    func follow(VocWithID id: Int) {
        request(WithVoctagRequest: VoctagRequest.init(requestType: .follow(id), parameters: nil, success: nil, error: nil))
    }

    //Unfollow Voc
    func unfollow(VocWithID id: Int) {
        request(WithVoctagRequest: VoctagRequest.init(requestType: .unfollow(id), parameters: nil, success: nil, error: nil))
    }

    //Report Voc
    func report(VocWithID id: Int, reason: String) {
        let parameters: [String: Any] = [VoctagKeys.REASON: reason]
        request(WithVoctagRequest: VoctagRequest.init(requestType: .report(id), parameters: parameters, success: nil, error: nil))
    }
    
    //Delete Voc
    func delete(VocWithID id: Int, completion: @escaping (Bool) -> Void) {
        request(WithVoctagRequest: VoctagRequest.init(requestType: .delete(id), parameters: nil, success: { (_) in
            completion(true)
        }) { _ in
            completion(false)
        })
    }
    
    // MARK: - Client Requests
    
    //Load Vocs from Client
    func getClientVocs(clientSlug: String, parameters: [String: Any], completion: @escaping ([Voc]?) -> Void) {
        request(WithVoctagRequest: VoctagRequest.init(requestType: .getClientVocs(clientSlug), parameters: parameters, success: { (json) in
            if let dataArray = self.getDataArray(FromJson: json) {
                completion(Voc.createArray(fromDataArray: dataArray))
            } else {
                completion(nil)
            }
        }) { _ in
            completion(nil)
        })
    }
    
    //Load Client Voc Detail
    func loadClientVocDetail(clientSlug: String, id: Int, completion: @escaping (Voc?) -> Void) {
        request(WithVoctagRequest: VoctagRequest.init(requestType: .loadClientVocDetail(clientSlug, id), parameters: nil, success: { (json) in
            if let dataObject = self.getDataObject(FromJson: json) {
                completion(Voc.init(fromDataObject: dataObject))
            } else {
                completion(nil)
            }
        }) { _ in
            completion(nil)
        })
    }
    
    //Get Clients
    func getClients(completion: @escaping ([Client]?) -> Void) {
        let parameters = [VoctagKeys.CLIENT_SCOPE: "all"]
        request(WithVoctagRequest: VoctagRequest.init(requestType: .getClients, parameters: parameters, success: { (json) in
            if let dataArray = self.getDataArray(FromJson: json) {
                completion(Client.createArray(fromDataArray: dataArray))
            } else {
                completion(nil)
            }
        }) { _ in
            completion(nil)
        })
    }
    
    //upload Client Voc
    func uploadClientVoc(clientSlug: String, parameters: [String: Any], success: @escaping (Voc) -> Void, error: @escaping () -> Void) {
        request(WithVoctagRequest: VoctagRequest.init(requestType: .uploadClientVoc(clientSlug), parameters: parameters, success: { (json) in
            if let dataObject = self.getDataObject(FromJson: json) {
                success(Voc.init(fromDataObject: dataObject))
            } else {
                error()
            }
        }) { _ in
            error()
        })
    }
    
    //Follow Client
    func followClient(clientSlug: String, completion: @escaping (Bool) -> Void) {
        request(WithVoctagRequest: VoctagRequest.init(requestType: .followClient(clientSlug), parameters: nil, success: { _ in
            completion(true)
        }) { _ in
            completion(false)
        })
    }
    
    //unfollow Client
    func unfollowClient(clientSlug: String, completion: @escaping (Bool) -> Void) {
        request(WithVoctagRequest: VoctagRequest.init(requestType: .unfollowClient(clientSlug), parameters: nil, success: { _ in
            completion(true)
        }) { _ in
            completion(false)
        })
    }
    
    //redeem client access code
    func redeemAccessCode(clientSlug: String, accessCode: String, successListener: @escaping () -> Void, errorListener: @escaping (Bool) -> Void) {
        let parameters = [VoctagKeys.CLIENT_ACCESS_CODE: accessCode]
        request(WithVoctagRequest: VoctagRequest.init(requestType: .redeemAccessCode(clientSlug), parameters: parameters, success: { _ in
            successListener()
        }) { response in
            if let statusCode = response?.statusCode, (statusCode == 422 || statusCode == 423) {
                errorListener(true)
            } else {
                errorListener(false)
            }
        })
    }
    
    // MARK: - Private Methods
    
    fileprivate func request(WithVoctagRequest voctagRequest: VoctagRequest) {
        let urlRequest = buildURLRequest(WithRequestType: voctagRequest.requestType, parameters: voctagRequest.parameters)
        Alamofire.request(urlRequest).responseJSON { response in
            
            
            if let statusCode = response.response?.statusCode {
                switch statusCode {
                case 200..<300: do {
                    /*
                    if response.result.isFailure {
                        LoggingHelper.logError(ForRequest: response.request!, error: response.result.error, statusCode: statusCode)
                        voctagRequest.error?(response.response)
                        return
                    }*/
                    
                    let result = response.result.value as? [String: Any]
                    voctagRequest.success?(result)
                    }
                case 401: do {
                    //auth token is invalid and need to be refreshed
                    self.refresh(WithVoctagRequest: voctagRequest)
                    }
                /*case 422, 423: do {
                    //some requests return 422 or 423 if input is already in use or invalid
                    LoggingHelper.logError(ForRequest: response.request!, error: response.result.error, statusCode: statusCode)
                    voctagRequest.error?(response.response)
                    }*/
                default:
                    LoggingHelper.logError(ForRequest: response.request!, error: response.result.error, statusCode: statusCode)
                    voctagRequest.error?(response.response)
                }
            } else {
                voctagRequest.error?(nil)
            }
            
            
            
            
            
            
            
            
            /* TODO
            if response.result.isFailure {
                if let statusCode = response.response?.statusCode {
                    if statusCode == 401 {
                        //auth token is invalid and need to be refreshed
                        self.refresh(WithVoctagRequest: voctagRequest)
                        return
                    }
                    LoggingHelper.logError(ForRequest: response.request!, error: response.result.error, statusCode: statusCode)
                }
                
                voctagRequest.error?(response.response)
            } else {
                if let statusCode = response.response?.statusCode {
                    if statusCode == 422 || statusCode == 423 {
                        //some requests return 422 or 423 if input is already in use or invalid
                        LoggingHelper.logError(ForRequest: response.request!, error: response.result.error, statusCode: statusCode)
                        voctagRequest.error?(response.response)
                        return
                    }
                }
                
                if let result = response.result.value as? [String: Any] {
                    voctagRequest.success?(result)
                } else {
                    voctagRequest.success?(nil)
                }
            }*/
        }
    }
    
    fileprivate func buildURLRequest(WithRequestType requestType: RequestType, parameters: [String : Any]?) -> URLRequest {
        //let url = VOCTAG_URL + requestType.url()
        let url = STAGING_URL + requestType.url()
        
        var urlRequest = URLRequest(url: URL.init(string: url)!)
        urlRequest.httpMethod = requestType.method().rawValue
        urlRequest.allHTTPHeaderFields = getHeaders(WithRequestType: requestType)
        urlRequest.timeoutInterval = TimeInterval(TIMEOUT_INTERVAL)
        
        if parameters != nil {
            // add parameters to the url if get request
            if requestType.method() == .get {
                var url = urlRequest.url!.absoluteString
                for (key, value) in parameters! {
                    url.append("?\(key)=\(value)")
                }
                urlRequest.url = URL.init(string: url)!
                return urlRequest
            } else {
                do {
                    return try Alamofire.JSONEncoding.default.encode(urlRequest, with: parameters)
                } catch {
                    LoggingHelper.logError(ForClass: RequestHelper.self, with: "request couldnt be encoded")
                    return urlRequest
                }
            }
        } else {
            return urlRequest
        }
    }
    
    fileprivate func getHeaders(WithRequestType requestType: RequestType) -> [String: String] {
        let applicationTypeString = "application/json"
        var header = [VoctagKeys.ACCEPT: applicationTypeString]
        
        if requestType.method() == .post {
            header[VoctagKeys.CONTENT_TYPE] = applicationTypeString
        }
        
        switch requestType {
        case .refresh, .register:
            // No auth required
            break
        default:
            if let authToken = AuthenticationHelper.shared.getAuthToken() {
                header[VoctagKeys.AUTHORIZATION] = authToken
            }
        }
        
        return header
    }
    
    fileprivate func getDataObject(FromJson json: [String: Any]?) -> [String: Any]? {
        if let dataObject = json?[VoctagKeys.DATA] as? [String: Any] {
            return dataObject
        } else {
            return nil
        }
    }
    
    fileprivate func getDataArray(FromJson json: [String: Any]?) -> [[String: Any]]? {
        if let dataArray = json?[VoctagKeys.DATA] as? [[String: Any]] {
            return dataArray
        } else {
            return nil
        }
    }

}

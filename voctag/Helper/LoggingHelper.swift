//
//  LoggingHelper.swift
//  voctag
//
//  Created by Jan Maron on 10.07.17.
//  Copyright © 2017 voctag. All rights reserved.
//

import UIKit

class LoggingHelper {
    
    //fileprivate static var networkErrorView: UIView?
    
    class func logError(ForClass errorClass: AnyClass, with message: String) {
        let errorDomain = "com.voctag.error.\(String(describing: errorClass.self))"
        print("\(errorDomain): \(message)")
    }
    
    class func logError(ForRequest request: URLRequest, error: Error?, statusCode: Int) {
        let errorURL = String(describing: request)
        let message = error != nil ? error!.localizedDescription : "error"
        print("\(errorURL): \(statusCode) - \(message)")
    }
    
    // MARK: - Network Error handling
    /*
    class func showNetworkError(ForViewController controller: UIViewController, andError error: Error) {
        //first hide existing network error if existing
        hideNetworkError()
        networkErrorView = setupNetworkError(ForMessage: error.localizedDescription, andController: controller)
        
        if controller.navigationController != nil {
            controller.navigationController?.view.addSubview(networkErrorView!)
        } else {
            controller.view.addSubview(networkErrorView!)
        }
    }

    fileprivate static func setupNetworkError(ForMessage message: String, andController controller: UIViewController) -> UIView {
        let screenWidth = Int(UIScreen.main.bounds.size.width)
        let screenHeight = Int(UIScreen.main.bounds.size.height)

        let backgroundView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: screenWidth, height: screenHeight))
        backgroundView.backgroundColor = UIColor.init(red: 0.0, green: 0.0, blue: 0.0, alpha: 0.2)
        
        let errorView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: screenWidth, height: 70))
        errorView.backgroundColor = UIColor.red
        
        let label = UILabel.init(frame: CGRect.init(x: 16, y: 20, width: screenWidth - 32, height: 50))
        label.text = message
        label.textColor = UIColor.white
        label.font = UIFont.boldSystemFont(ofSize: 16)
        label.adjustsFontSizeToFitWidth = true
        label.minimumScaleFactor = 0.2
        label.numberOfLines = 2
        label.baselineAdjustment = .alignCenters
        label.textAlignment = .center
        errorView.addSubview(label)
        
        //im splashViewController kann der Request nicht manuell ausgelöst werden, deshalb muss die Fehlermeldung immer angezeigt werden
        if !controller.isKind(of: SplashViewController.self) {
            let tapGestureRecognizer = UITapGestureRecognizer.init(target: self, action: #selector(hideNetworkError))
            backgroundView.addGestureRecognizer(tapGestureRecognizer)
        }
        
        backgroundView.addSubview(errorView)
        return backgroundView
    }
    
    @objc fileprivate static func hideNetworkError() {
        if networkErrorView != nil {
            networkErrorView?.removeFromSuperview()
            networkErrorView = nil
        }
    }*/

}

//
//  VocReportHelper.swift
//  voctag
//
//  Created by Jan Maron on 27.07.17.
//  Copyright © 2017 voctag. All rights reserved.
//

import UIKit

class VocReportHelper {
    
    fileprivate enum ReportType: String {
        case myvoc = "support"
        case copyright = "copyright"
        case racism = "racism"
        case porn = "porn"
        case spam = "spam"
        case other = "other"
        
        func title() -> String {
            switch self {
            case .myvoc:
                return "report_type_myvoc".getTranslation()
            case .copyright:
                return "report_type_copyright".getTranslation()
            case .racism:
                return "report_type_racism".getTranslation()
            case .porn:
                return "report_type_porn".getTranslation()
            case .spam:
                return "report_type_spam".getTranslation()
            case .other:
                return "report_type_other".getTranslation()
            }
        }
    }
    
    fileprivate class func alertAction(forVocID vocID: Int, reportType: ReportType) -> UIAlertAction {
        return UIAlertAction(title: reportType.title(), style: .default, handler:  { _ in
            RequestHelper.shared.report(VocWithID: vocID, reason: reportType.rawValue)
        })
    }
    
    class func report(vocID: Int, forViewController viewController: UIViewController) {
        let actionSheetMessage = "report_action_sheet_message".getTranslation()
        let actionSheetTitle = "report_action_sheet_title".getTranslation()
        let actionSheetAlertController = UIAlertController.init(title: actionSheetTitle, message: actionSheetMessage, preferredStyle: .actionSheet)
        
        let myvocAction = VocReportHelper.alertAction(forVocID: vocID, reportType: ReportType.myvoc)
        let copyrightAction = VocReportHelper.alertAction(forVocID: vocID, reportType: ReportType.copyright)
        let racismAction = VocReportHelper.alertAction(forVocID: vocID, reportType: ReportType.racism)
        let pornAction = VocReportHelper.alertAction(forVocID: vocID, reportType: ReportType.porn)
        let spamAction = VocReportHelper.alertAction(forVocID: vocID, reportType: ReportType.spam)
        let otherAction = VocReportHelper.alertAction(forVocID: vocID, reportType: ReportType.other)
        let cancelAction = UIAlertAction(title: "feed_cell_action_cancel".getTranslation(), style: .cancel, handler:  nil)
        
        actionSheetAlertController.addAction(myvocAction)
        actionSheetAlertController.addAction(copyrightAction)
        actionSheetAlertController.addAction(racismAction)
        actionSheetAlertController.addAction(pornAction)
        actionSheetAlertController.addAction(spamAction)
        actionSheetAlertController.addAction(otherAction)
        actionSheetAlertController.addAction(cancelAction)
        viewController.present(actionSheetAlertController, animated: true, completion: nil)
    }
    
}

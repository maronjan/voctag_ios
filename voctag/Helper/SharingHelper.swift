//
//  SharingHelper.swift
//  voctag
//
//  Created by Jan Maron on 27.07.17.
//  Copyright © 2017 voctag. All rights reserved.
//

import Foundation
import FacebookCore
import FacebookLogin
import FacebookShare
import TwitterKit

class SharingHelper {
    
    static let shared = SharingHelper()
    
    // MARK: - Twitter
    
    func loginWithTwitter(completion: @escaping (Bool) -> Void) {
        TWTRTwitter.sharedInstance().logIn { session, error in
            if let error = error {
                LoggingHelper.logError(ForClass: SharingHelper.self, with: "\(error)")
            }
            
            completion(session != nil)
        }
    }
    
    func shareToTwitter(url: URL, title: String) {
        var error: NSError?
        let statusString = "\(title) \(url.absoluteString)"
        let params = [VoctagKeys.STATUS: statusString]
        let urlRequest = TWTRAPIClient
            .withCurrentUser()
            .urlRequest(withMethod: "POST",
                        urlString: "https://api.twitter.com/1.1/statuses/update.json",
                        parameters: params, error: &error)
        TWTRAPIClient.withCurrentUser().sendTwitterRequest(urlRequest, completion: { (_, _, error) in
            if let error = error {
                LoggingHelper.logError(ForClass: SharingHelper.self, with: "\(error)")
            }
        })
    }
    
    // MARK: - Facebook
    
    /// https://developers.facebook.com/docs/facebook-login/permissions/
    fileprivate let publishPermissions: [PublishPermission] = [.managePages, .publishPages]
    
    func loginWithFacebook(viewController: UIViewController, completion: @escaping (Bool) -> Void) {
        let loginManager = LoginManager()
        loginManager.logIn(publishPermissions: publishPermissions, viewController: viewController) { (result) in
            switch result {
            case .failed(let error):
                LoggingHelper.logError(ForClass: SharingHelper.self, with: error.localizedDescription)
                completion(false)
            case .cancelled:
                LoggingHelper.logError(ForClass: SharingHelper.self, with: "facebook login cancelled")
                completion(false)
            case .success(let grantedPermissions, _, _):
                print("logged in w/ permission \(grantedPermissions)")
                completion(true)
            }
        }
    }
    
    // https://developers.facebook.com/docs/swift/sharing/graph-api
    func shareToFacebook(url: URL, title: String) {
        let content = LinkShareContent.init(url: url, quote: title)
        let sharer = GraphSharer(content: content)
        sharer.failsOnInvalidData = true
        sharer.completion = { result in
            print(result)
        }
        
        try! sharer.share()
    }
    
}

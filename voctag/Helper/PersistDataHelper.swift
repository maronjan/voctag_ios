//
//  PersistDataHelper.swift
//  voctag
//
//  Created by Jan Maron on 14.07.17.
//  Copyright © 2017 voctag. All rights reserved.
//

import Foundation

class PersistDataHelper {
    
    static let shared = PersistDataHelper()

    func saveAuthentication(_ auth: Authentication) {
        if auth.authToken != nil {
            AuthenticationHelper.shared.saveAuthToken(auth.authToken!)
            auth.authToken = nil
        }
        
        UserDefaults.standard.set(NSKeyedArchiver.archivedData(withRootObject: auth), forKey: VoctagKeys.PERSIST_AUTH)
    }
    
    func loadAuthentication() -> Authentication? {
        if let decoded = UserDefaults.standard.object(forKey: VoctagKeys.PERSIST_AUTH) as? Data {
            let auth = NSKeyedUnarchiver.unarchiveObject(with: decoded) as! Authentication
            auth.authToken = AuthenticationHelper.shared.getAuthToken()
            return auth
        } else {
            return nil
        }
    }
  
    func saveFilter(_ filter: Filter) {
        UserDefaults.standard.set(NSKeyedArchiver.archivedData(withRootObject: filter), forKey: VoctagKeys.PERSIST_FILTER)
    }
    
    func loadFilter() -> Filter? {
        if let decoded = UserDefaults.standard.object(forKey: VoctagKeys.PERSIST_FILTER) as? Data {
            return NSKeyedUnarchiver.unarchiveObject(with: decoded) as? Filter
        } else {
            return nil
        }
    }

    func saveUser(_ user: User) {
        UserDefaults.standard.set(NSKeyedArchiver.archivedData(withRootObject: user), forKey: VoctagKeys.PERSIST_USER)
    }
    
    func loadUser() -> User? {
        if let decoded = UserDefaults.standard.object(forKey: VoctagKeys.PERSIST_USER) as? Data {
            return NSKeyedUnarchiver.unarchiveObject(with: decoded) as? User
        } else {
            return nil
        }
    }
    
    // MARK: - Voc Counts
    
    func saveVocCountForClient(slug: String, count: Int) {
        UserDefaults.standard.set(count, forKey: slug)
    }
    
    func loadVocCountForClient(_ client: Client) -> Int? {
        if let count = UserDefaults.standard.object(forKey: client.slug) as? Int {
            return count
        } else {
            return nil
        }
    }
    
    func saveVocCountForVoc(vocID: Int, count: Int) {
        UserDefaults.standard.set(count, forKey: String(vocID))
    }
    
    func loadVocCountForVoc(_ voc: Voc) -> Int? {
        if let count = UserDefaults.standard.object(forKey: String(voc.vocID)) as? Int {
            return count
        } else {
            return nil
        }
    }
    
}

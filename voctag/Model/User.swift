//
//  User.swift
//  voctag
//
//  Created by Jan Maron on 14.07.17.
//  Copyright © 2017 voctag. All rights reserved.
//

import Foundation

class User: NSObject, NSCoding {
    
    var id = 0
    var acceptedTerms = false
    var points = 0
    var latitude: Double?
    var longitude: Double?
    var nickname: String?
    var vocs = [Voc]()
    var replies = [Voc]()
    var favorites = [Voc]()

    convenience init(forUserID id: Int) {
        self.init()
        self.id = id
    }
    
    convenience init(fromDataObject data: [String: Any]) {
        self.init()
        self.id = data[VoctagKeys.USER_ID] as! Int
        self.acceptedTerms = data[VoctagKeys.USER_ACCEPTED_TERMS] as! Bool
        self.points = data[VoctagKeys.USER_POINTS] as! Int
        self.vocs = Voc.createArray(fromDataArray: data[VoctagKeys.USER_VOCS] as? [[String: Any]])
        self.replies = Voc.createArray(fromDataArray: data[VoctagKeys.USER_REPLIES] as? [[String: Any]])
        self.favorites = Voc.createArray(fromDataArray: data[VoctagKeys.USER_FAVORITES] as? [[String: Any]])
        
        if let _nickname = data[VoctagKeys.USER_NICKNAME] as? String {
            if !_nickname.isEmpty {
                self.nickname = _nickname
            }
        }
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(id, forKey: VoctagKeys.USER_ID)
        aCoder.encode(acceptedTerms, forKey: VoctagKeys.USER_ACCEPTED_TERMS)
        aCoder.encode(points, forKey: VoctagKeys.USER_POINTS)
        aCoder.encode(latitude, forKey: VoctagKeys.USER_LATITUDE)
        aCoder.encode(longitude, forKey: VoctagKeys.USER_LONGITUDE)
        aCoder.encode(nickname, forKey: VoctagKeys.USER_NICKNAME)
        aCoder.encode(vocs, forKey: VoctagKeys.USER_VOCS)
        aCoder.encode(replies, forKey: VoctagKeys.USER_REPLIES)
        aCoder.encode(favorites, forKey: VoctagKeys.USER_FAVORITES)
    }
    
    //decoding sometimes have variables not are not yet created, for example if you added a new variable
    required convenience init?(coder aDecoder: NSCoder) {
        self.init()
        self.id = aDecoder.decodeInteger(forKey: VoctagKeys.USER_ID)
        self.acceptedTerms = aDecoder.decodeBool(forKey: VoctagKeys.USER_ACCEPTED_TERMS)
        self.points = aDecoder.decodeInteger(forKey: VoctagKeys.USER_POINTS)
        
        self.latitude = aDecoder.decodeObject(forKey: VoctagKeys.USER_LATITUDE) as? Double
        self.longitude = aDecoder.decodeObject(forKey: VoctagKeys.USER_LONGITUDE) as? Double
        /*
        //decodeObjects können wieder weg sobald alle die neue Version benutzen da dann kein nil mehr im speicher hier ist
        if let latitude = aDecoder.decodeObject(forKey: VoctagKeys.USER_LATITUDE) {
            self.latitude = latitude as! Double
        } else {
            self.latitude = aDecoder.decodeDouble(forKey: VoctagKeys.USER_LATITUDE)
        }
        if let longitude = aDecoder.decodeObject(forKey: VoctagKeys.USER_LONGITUDE) {
            self.longitude = longitude as! Double
        } else {
            self.longitude = aDecoder.decodeDouble(forKey: VoctagKeys.USER_LONGITUDE)
        }
        */
        self.nickname = aDecoder.decodeObject(forKey: VoctagKeys.USER_NICKNAME) as? String
        self.vocs = aDecoder.decodeObject(forKey: VoctagKeys.USER_VOCS) as! [Voc]
        if aDecoder.containsValue(forKey: VoctagKeys.USER_REPLIES) { self.replies = aDecoder.decodeObject(forKey: VoctagKeys.USER_REPLIES) as! [Voc] }
        if aDecoder.containsValue(forKey: VoctagKeys.USER_FAVORITES) { self.favorites = aDecoder.decodeObject(forKey: VoctagKeys.USER_FAVORITES) as! [Voc] }
    }
    
    // MARK: - ViewModel
    
    var pointsString: String {
        return "\(points)"
    }
    
    var hasLocation: Bool {
        return latitude != nil && longitude != nil
        //return latitude != 0.0 && longitude != 0.0
    }
    
}

//
//  Filter.swift
//  voctag
//
//  Created by Jann Driessen on 18.09.16.
//  Copyright © 2016 voctag. All rights reserved.
//

import Foundation

class Filter: NSObject, NSCoding {
    
    enum OrderType: String {
        case Newest = "newest"
        case MostPlayed = "most_played"
        case MostReplies = "most_replies"
        case MostUpVotes = "most_up_votes"
        
        func title() -> String {
            switch self {
            case .Newest:
                return "feed_filter_order_type_newest".getTranslation()
            case .MostPlayed:
                return "feed_filter_order_type_most_played".getTranslation()
            case .MostReplies:
                return "feed_filter_order_type_most_replies".getTranslation()
            case .MostUpVotes:
                return "feed_filter_order_type_most_upvotes".getTranslation()
            }
        }
    }
    
    var orderType = OrderType.Newest
    
    convenience init(orderType: OrderType) {
        self.init()
        self.orderType = orderType
    }

    func encode(with aCoder: NSCoder) {
        aCoder.encode(orderType.rawValue, forKey: VoctagKeys.FILTER_ORDER)
    }
    
    required convenience init?(coder aDecoder: NSCoder) {
        self.init()
        self.orderType = OrderType.init(rawValue: aDecoder.decodeObject(forKey: VoctagKeys.FILTER_ORDER) as! String)!
    }
    
}

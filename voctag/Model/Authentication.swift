//
//  Authentication.swift
//  voctag
//
//  Created by Jann Driessen on 24.08.16.
//  Copyright © 2016 voctag. All rights reserved.
//

import Foundation

class Authentication: NSObject, NSCoding {
    
    var userID = 0
    var authToken: String?
    var refreshToken: String?

    convenience init(fromDataObject data: [String: Any]) {
        self.init()
        self.userID = data[VoctagKeys.AUTHENTICATION_USER_ID] as! Int
        self.authToken = data[VoctagKeys.AUTHENTICATION_AUTH_TOKEN] as? String
        self.refreshToken = data[VoctagKeys.AUTHENTICATION_REFRESH_TOKEN] as? String
    }

    func encode(with aCoder: NSCoder) {
        aCoder.encode(userID, forKey: VoctagKeys.AUTHENTICATION_USER_ID)
        aCoder.encode(authToken, forKey: VoctagKeys.AUTHENTICATION_AUTH_TOKEN)
        aCoder.encode(refreshToken, forKey: VoctagKeys.AUTHENTICATION_REFRESH_TOKEN)
    }
    
    required convenience init?(coder aDecoder: NSCoder) {
        self.init()
        self.userID = aDecoder.decodeInteger(forKey: VoctagKeys.AUTHENTICATION_USER_ID)
        self.authToken = aDecoder.decodeObject(forKey: VoctagKeys.AUTHENTICATION_AUTH_TOKEN) as? String
        self.refreshToken = aDecoder.decodeObject(forKey: VoctagKeys.AUTHENTICATION_REFRESH_TOKEN) as? String
    }
    
}

//
//  Voc.swift
//  voctag
//
//  Created by Jann Driessen on 15.09.16.
//  Copyright © 2016 voctag. All rights reserved.
//

import Foundation

class Voc: NSObject, NSCoding {
    
    fileprivate static let VOC_DATEFORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
    
    var vocID = 0
    var userID = 0
    var userNickname: String?
    var repliesCount = 0
    var title: String?
    var playCount = 0
    var audioURL: URL?
    var duration: Float = 0.0
    var webURL: URL?
    var city: String?
    var score = 0
    var transcript = ""
    
    var parentID: Int?
    var voted = 0
    var secret = false
    var secretRepliesAllowed = false
    var following = false
    var replies = [Voc]()
    
    var createdAt = Date()
    
    var createdByClientOwner = false
    var parentClient: Client?
    
    convenience init(withVocID id: Int) {
        self.init()
        self.vocID = id
    }

    convenience init(fromDataObject data: [String: Any]) {
        self.init()
        self.vocID = data[VoctagKeys.VOC_ID] as! Int
        self.userID = data[VoctagKeys.VOC_USER_ID] as! Int
        self.userNickname = data[VoctagKeys.VOC_USER_NICKNAME] as? String
        self.repliesCount = data[VoctagKeys.VOC_REPLIES_COUNT] as! Int
        self.title = data[VoctagKeys.VOC_TITLE] as? String
        self.playCount = data[VoctagKeys.VOC_PLAY_COUNT] as! Int
        self.audioURL = URL.init(string: data[VoctagKeys.VOC_AUDIO_URL] as! String)!
        self.duration = Float(data[VoctagKeys.VOC_DURATION] as! Double)
        self.webURL = URL.init(string: data[VoctagKeys.VOC_WEB_URL] as! String)!
        self.city = data[VoctagKeys.VOC_CITY] as? String
        self.score = data[VoctagKeys.VOC_SCORE] as! Int
        self.transcript = data[VoctagKeys.VOC_TRANSCRIPT] as! String

        //those parameters can be not in response included
        if let _parentID = data[VoctagKeys.VOC_PARENT_ID] as? Int { self.parentID = _parentID }
        if let _voted = data[VoctagKeys.VOC_VOTED] as? Int { self.voted = _voted }
        if let _secret = data[VoctagKeys.VOC_SECRET] as? Bool { self.secret = _secret }
        if let _secretRepliesAllowed = data[VoctagKeys.VOC_SECRET_REPLIES_ALLOWED] as? Bool { self.secretRepliesAllowed = _secretRepliesAllowed }
        if let _following = data[VoctagKeys.VOC_FOLLOWING] as? Bool { self.following = _following }
        if let _replies = data[VoctagKeys.VOC_REPLIES] as? [[String: Any]] { self.replies = Voc.createArray(fromDataArray: _replies) }
        if let _createdByClientOwner = data[VoctagKeys.VOC_CREATED_BY_CLIENT_OWNER] as? Bool { self.createdByClientOwner = _createdByClientOwner }
        
        if let _parentClient = data[VoctagKeys.VOC_PARENT_CLIENT] as? [String: Any] {
            self.parentClient = Client.init(fromDataObject: _parentClient)
        } else if let _clientSlug = data[VoctagKeys.CLIENT_SLUG] as? String, let _clientColor = data[VoctagKeys.CLIENT_THEMECOLOR] as? String {
            self.parentClient = Client.init(clientSlug: _clientSlug, themeColor: _clientColor)
        }
        
        // Voc date format:
        // 2016-10-25T08:42:46.293Z
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = Voc.VOC_DATEFORMAT
        if let createdAtDate = dateFormatter.date(from: data[VoctagKeys.VOC_CREATED_AT] as! String) { self.createdAt = createdAtDate }
    }

    func encode(with aCoder: NSCoder) {
        aCoder.encode(vocID, forKey: VoctagKeys.VOC_ID)
        aCoder.encode(userID, forKey: VoctagKeys.VOC_USER_ID)
        aCoder.encode(userNickname, forKey: VoctagKeys.VOC_USER_NICKNAME)
        aCoder.encode(repliesCount, forKey: VoctagKeys.VOC_REPLIES_COUNT)
        aCoder.encode(title, forKey: VoctagKeys.VOC_TITLE)
        aCoder.encode(playCount, forKey: VoctagKeys.VOC_PLAY_COUNT)
        aCoder.encode(audioURL, forKey: VoctagKeys.VOC_AUDIO_URL)
        aCoder.encode(duration, forKey: VoctagKeys.VOC_DURATION)
        aCoder.encode(webURL, forKey: VoctagKeys.VOC_WEB_URL)
        aCoder.encode(city, forKey: VoctagKeys.VOC_CITY)
        aCoder.encode(score, forKey: VoctagKeys.VOC_SCORE)
        aCoder.encode(transcript, forKey: VoctagKeys.VOC_TRANSCRIPT)
        aCoder.encode(parentID, forKey: VoctagKeys.VOC_PARENT_ID)
        aCoder.encode(voted, forKey: VoctagKeys.VOC_VOTED)
        aCoder.encode(secret, forKey: VoctagKeys.VOC_SECRET)
        aCoder.encode(secretRepliesAllowed, forKey: VoctagKeys.VOC_SECRET_REPLIES_ALLOWED)
        aCoder.encode(following, forKey: VoctagKeys.VOC_FOLLOWING)
        aCoder.encode(replies, forKey: VoctagKeys.VOC_REPLIES)
        aCoder.encode(createdAt, forKey: VoctagKeys.VOC_CREATED_AT)
    }
    
    //decoding sometimes have variables not are not yet created, for example if you added a new variable
    required convenience init?(coder aDecoder: NSCoder) {
        self.init()
        self.vocID = aDecoder.decodeInteger(forKey: VoctagKeys.VOC_ID)
        self.userID = aDecoder.decodeInteger(forKey: VoctagKeys.VOC_USER_ID)
        self.userNickname = aDecoder.decodeObject(forKey: VoctagKeys.VOC_USER_NICKNAME) as? String
        self.repliesCount = aDecoder.decodeInteger(forKey: VoctagKeys.VOC_REPLIES_COUNT)
        self.title = aDecoder.decodeObject(forKey: VoctagKeys.VOC_TITLE) as? String
        self.playCount = aDecoder.decodeInteger(forKey: VoctagKeys.VOC_PLAY_COUNT)
        self.audioURL = aDecoder.decodeObject(forKey: VoctagKeys.VOC_AUDIO_URL) as? URL
        self.duration = aDecoder.decodeFloat(forKey: VoctagKeys.VOC_DURATION)
        self.webURL = aDecoder.decodeObject(forKey: VoctagKeys.VOC_WEB_URL) as? URL
        self.city = aDecoder.decodeObject(forKey: VoctagKeys.VOC_CITY) as? String
        self.score = aDecoder.decodeInteger(forKey: VoctagKeys.VOC_SCORE)
        self.transcript = aDecoder.decodeObject(forKey: VoctagKeys.VOC_TRANSCRIPT) as! String
        self.parentID = aDecoder.decodeObject(forKey: VoctagKeys.VOC_PARENT_ID) as? Int
        self.voted = aDecoder.decodeInteger(forKey: VoctagKeys.VOC_VOTED)
        self.secret = aDecoder.decodeBool(forKey: VoctagKeys.VOC_SECRET)
        self.secretRepliesAllowed = aDecoder.decodeBool(forKey: VoctagKeys.VOC_SECRET_REPLIES_ALLOWED)
        self.following = aDecoder.decodeBool(forKey: VoctagKeys.VOC_FOLLOWING)
        self.replies = aDecoder.decodeObject(forKey: VoctagKeys.VOC_REPLIES) as! [Voc]
        self.createdAt = aDecoder.decodeObject(forKey: VoctagKeys.VOC_CREATED_AT) as! Date
    }
    
    // MARK: - ViewModel

    var didVote: Bool {
        return voted != 0
    }
    
    var didVoteUp: Bool {
        return voted == 1
    }
    
    var playsString: String {
        return "\(playCount) Plays"
    }
    
    var repliesString: String {
        if repliesCount == 1 {
            return "\(repliesCount) \("reply_title".getTranslation())"
        } else {
            return "\(repliesCount) \("replies_title".getTranslation())"
        }
    }
    
    var titleString: String {
        return title == nil ? "" : title!
    }
    
    var shareTitleString: String {
        return titleString.isEmpty ? "Audio" : titleString
    }
    
    var scoreString: String {
        return "\(score)"
    }
    
    var locationString: String {
        return city == nil ? "-" : city!
    }
    
    var transcriptString: String {
        //Erster Buchstabe soll groß sein
        let first = String(self.transcript.prefix(1)).capitalized
        let other = String(self.transcript.dropFirst())
        return first + other
    }
    
    var shareWebURL: URL? {
        if webURL != nil {
            let lang = Locale.current.languageCode == "de" ? "de" : "en"
            let components = webURL?.absoluteString.components(separatedBy: ".de")
            return URL.init(string: components![0] + ".de/" + lang + components![1])
        } else {
            return nil
        }
    }
    
    var dateString: String {
        let now = Date.init().convertToServerTimeZone()
        let calendar = Calendar.current
        var str = ""
        
        if let years = calendar.dateComponents([.year], from: createdAt, to: now).year, years > 0 {
            str = "\(years)Y"
        } else if let months = calendar.dateComponents([.month], from: createdAt, to: now).month, months > 0 {
            str = "\(months)M"
        } else if let days = calendar.dateComponents([.day], from: createdAt, to: now).day, days > 0 {
            str = "\(days)d"
        } else if let hours = calendar.dateComponents([.hour], from: createdAt, to: now).hour, hours > 0 {
            str = "\(hours)h"
        } else if let minutes = calendar.dateComponents([.minute], from: createdAt, to: now).minute, minutes > 0 {
            str = "\(minutes)m"
        } else if let seconds = calendar.dateComponents([.second], from: createdAt, to: now).second, seconds > 0 {
            str = "\(seconds)s"
        } else {
            str = "<1s"
        }
        
        //replace placeholder
        return "date_before".getTranslation().replacingOccurrences(of: "%%", with: str)
    }
    
    var dateLocationString: String {
        return dateString + " in " + locationString
    }
    
    var hasNewReplies: Bool {
        if let count = PersistDataHelper.shared.loadVocCountForVoc(self) {
            if count == 0 || repliesCount == 0 {
                return false
            } else {
                return count < repliesCount
            }
        } else {
            return false
        }
    }
    
    var newRepliesCountString: String {
        if let count = PersistDataHelper.shared.loadVocCountForVoc(self) {
            let diff = repliesCount - count
            return "\(diff)"
        } else {
            return ""
        }
    }
    
    func voteUp(_ up: Bool) {
        if up {
            voted = 1
            score += 1
        } else {
            voted = -1
            score -= 1
        }
    }
    
    class func createArray(fromDataArray dataArray: [[String: Any]]?) -> [Voc] {
        var vocs = [Voc]()
        if dataArray != nil {
            for voc in dataArray! {
                vocs.append(Voc.init(fromDataObject: voc))
            }
        }
        return vocs
    }

}

//
//  Client.swift
//  voctag
//
//  Created by Jan Maron on 24.04.18.
//  Copyright © 2018 voctag. All rights reserved.
//

import UIKit

class Client: NSObject, NSCoding {
    
    var slug = ""
    var name = ""
    var clientDescription = ""
    var memberships_count = 0
    var smallIconURL: URL?
    var mediumIconURL: URL?
    var audioURL: URL?
    var themeColorString = ""
    var following = false
    var vocsCount = 0
    var vocs = [Voc]()
    
    var isClosed = false
    var hasAccess = false
    var salesURL: String?
    
    convenience init(clientSlug: String, themeColor: String) {
        self.init()
        self.slug = clientSlug
        self.themeColorString = themeColor
    }
    
    convenience init(fromDataObject data: [String: Any]) {
        self.init()
        self.slug = data[VoctagKeys.CLIENT_SLUG] as! String
        self.name = data[VoctagKeys.CLIENT_NAME] as! String
        self.clientDescription = data[VoctagKeys.CLIENT_DESCRIPTION] as! String
        self.memberships_count = data[VoctagKeys.CLIENT_MEMBERSHIPS_COUNT] as! Int
        self.themeColorString = data[VoctagKeys.CLIENT_THEMECOLOR] as! String
        self.vocsCount = data[VoctagKeys.CLIENT_VOCS_COUNT] as! Int
        
        self.following = data[VoctagKeys.CLIENT_FOLLOWING] as? Bool ?? false
        if let _audioURL = data[VoctagKeys.CLIENT_AUDIO_URL] as? String, !_audioURL.isEmpty, _audioURL != "null" {
            self.audioURL = URL.init(string: _audioURL)!
        }
        
        //get image urls
        if let urls = data[VoctagKeys.CLIENT_ICON_URLS] as? [String: Any], !urls.isEmpty {
            self.smallIconURL = URL.init(string: urls[VoctagKeys.CLIENT_ICON_SMALL] as! String)!
            self.mediumIconURL = URL.init(string: urls[VoctagKeys.CLIENT_ICON_MEDIUM] as! String)!
        }
        
        self.isClosed = data[VoctagKeys.CLIENT_IS_CLOSED] as! Bool
        self.hasAccess = data[VoctagKeys.CLIENT_HAS_ACCESS] as? Bool ?? false
        if let _salesLink = data[VoctagKeys.CLIENT_SALES_LINK] as? String, !_salesLink.isEmpty {
            self.salesURL = _salesLink
        }
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(slug, forKey: VoctagKeys.CLIENT_SLUG)
        aCoder.encode(name, forKey: VoctagKeys.CLIENT_NAME)
        aCoder.encode(clientDescription, forKey: VoctagKeys.CLIENT_DESCRIPTION)
        aCoder.encode(memberships_count, forKey: VoctagKeys.CLIENT_MEMBERSHIPS_COUNT)
        aCoder.encode(themeColorString, forKey: VoctagKeys.CLIENT_THEMECOLOR)
        aCoder.encode(audioURL, forKey: VoctagKeys.CLIENT_AUDIO_URL)
        aCoder.encode(following, forKey: VoctagKeys.CLIENT_FOLLOWING)
        aCoder.encode(smallIconURL, forKey: VoctagKeys.CLIENT_ICON_SMALL)
        aCoder.encode(mediumIconURL, forKey: VoctagKeys.CLIENT_ICON_MEDIUM)
        aCoder.encode(vocs, forKey: VoctagKeys.CLIENT_VOCS)
        
        aCoder.encode(isClosed, forKey: VoctagKeys.CLIENT_IS_CLOSED)
        aCoder.encode(hasAccess, forKey: VoctagKeys.CLIENT_HAS_ACCESS)
        aCoder.encode(salesURL, forKey: VoctagKeys.CLIENT_SALES_LINK)
    }
    
    required convenience init?(coder aDecoder: NSCoder) {
        self.init()
        self.slug = aDecoder.decodeObject(forKey: VoctagKeys.CLIENT_SLUG) as! String
        self.name = aDecoder.decodeObject(forKey: VoctagKeys.CLIENT_NAME) as! String
        self.clientDescription = aDecoder.decodeObject(forKey: VoctagKeys.CLIENT_DESCRIPTION) as! String
        self.memberships_count = aDecoder.decodeObject(forKey: VoctagKeys.CLIENT_MEMBERSHIPS_COUNT) as! Int
        self.themeColorString = aDecoder.decodeObject(forKey: VoctagKeys.CLIENT_THEMECOLOR) as! String
        self.following = aDecoder.decodeObject(forKey: VoctagKeys.CLIENT_FOLLOWING) as! Bool
        self.audioURL = aDecoder.decodeObject(forKey: VoctagKeys.CLIENT_AUDIO_URL) as? URL
        self.smallIconURL = aDecoder.decodeObject(forKey: VoctagKeys.CLIENT_ICON_SMALL) as? URL
        self.mediumIconURL = aDecoder.decodeObject(forKey: VoctagKeys.CLIENT_ICON_MEDIUM) as? URL
        self.vocs = aDecoder.decodeObject(forKey: VoctagKeys.CLIENT_VOCS) as! [Voc]
        
        self.isClosed = aDecoder.decodeObject(forKey: VoctagKeys.CLIENT_IS_CLOSED) as! Bool
        self.hasAccess = aDecoder.decodeObject(forKey: VoctagKeys.CLIENT_HAS_ACCESS) as! Bool
        self.salesURL = aDecoder.decodeObject(forKey: VoctagKeys.CLIENT_SALES_LINK) as? String
    }
    
    // MARK: - ViewModel

    var membershipsCountString: String {
        return "\(memberships_count)"
    }
    
    var hasAudio: Bool {
        return audioURL != nil && !audioURL!.absoluteString.isEmpty
    }
    
    var themeColor: UIColor {
        var hexInt: UInt32 = 0
        let scanner = Scanner.init(string: themeColorString)
        scanner.scanHexInt32(&hexInt)
        return UIColor.init(rgb: Int(hexInt))
    }
    
    var hasNewVocs: Bool {
        if let count = PersistDataHelper.shared.loadVocCountForClient(self) {
            if count == 0 || vocsCount == 0 {
                return false
            } else {
                return count < vocsCount
            }
        } else {
            return false
        }
    }
    
    var newVocsCountString: String {
        if let count = PersistDataHelper.shared.loadVocCountForClient(self) {
            if count == 0 || vocsCount == 0 {
                return ""
            } else {
                let diff = vocsCount - count
                if diff >= 100 {
                    return "99"
                } else {
                    return "\(diff)"
                }
            }
        } else {
            return ""
        }
    }
    
    var mainAudioFakeVoc: Voc? {
        if self.hasAudio {
            let voc = Voc.init(withVocID: -1)
            voc.audioURL = self.audioURL
            voc.parentClient = self
            return voc
        } else {
            return nil
        }
    }
    
    class func createArray(fromDataArray dataArray: [[String: Any]]?) -> [Client] {
        var clients = [Client]()
        if dataArray != nil {
            for client in dataArray! {
                clients.append(Client.init(fromDataObject: client))
            }
        }
        return clients
    }
    
}

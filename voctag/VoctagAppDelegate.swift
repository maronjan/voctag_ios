//
//  VoctagAppDelegate.swift
//  voctag
//
//  Created by Jann Driessen on 19.08.16.
//  Copyright © 2016 voctag. All rights reserved.
//

import UIKit
import TwitterKit
import FBSDKCoreKit
import UserNotifications
import Firebase

@UIApplicationMain
class VoctagAppDelegate: UIResponder, UIApplicationDelegate {
    
    static let shared = UIApplication.shared.delegate as! VoctagAppDelegate
    
    fileprivate let TWITTER_CONSUMER_KEY = "o9wz6NOX2mJeIrkhh2cKa4E9o"
    fileprivate let TWITTER_CONSUMER_SECRET = "CCXNu1NiMKNOnMoTi6dC0jDCKu8Y6ae2tbDx0ujwslet70gVUh"

    var window: UIWindow? // required
    
    var shouldOpenVoc: Voc?
    var shouldOpenClient: Client?
    var tabBarController: VoctagTabBarController?
    var deviceToken: String?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)
        TWTRTwitter.sharedInstance().start(withConsumerKey: TWITTER_CONSUMER_KEY, consumerSecret: TWITTER_CONSUMER_SECRET)
        
        FirebaseApp.configure()
        
        // Check if launched from notification
        if let userInfo = launchOptions?[.remoteNotification] as? [AnyHashable: Any] {
            parseRemoteNotification(userInfo)
        }
        
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        //AudioPlayer.shared.stopIfNecessary()
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        
        //refresh communityViewController
        refreshCommunityViewControllerIfNecessary()
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    //Facebook, Twitter and Deeplinking (voctag://vocid=2344)
    func application(_ app: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey : Any] = [:]) -> Bool {
        if let host = url.host {
            if host.contains("vocs") {
                //shouldOpenVoc = Voc.init(withVocID: Int(host.replacingOccurrences(of: "vocid=", with: ""))!)
                let voc = Voc.init(withVocID: Int(host.replacingOccurrences(of: "vocid=", with: ""))!)
                if tabBarController != nil {
                    tabBarController!.openVoc(voc, shouldScrollToVoc: nil, deletedVocListener: nil)
                } else {
                    shouldOpenVoc = voc
                }
            }
        }

        if url.absoluteString.contains("twitter") {
            return TWTRTwitter.sharedInstance().application(app, open: url, options: options)
        }
        
        return FBSDKApplicationDelegate.sharedInstance().application(app, open: url, sourceApplication: options[UIApplicationOpenURLOptionsKey.sourceApplication] as! String?, annotation: options[UIApplicationOpenURLOptionsKey.annotation])
    }

    // MARK: - Push Notifications
    
    func registerRemoteNotifications() {
        UNUserNotificationCenter.current().delegate = self
        UNUserNotificationCenter.current().requestAuthorization(options: [.badge, .sound, .alert], completionHandler: { (granted, error) in
            if granted {
                DispatchQueue.main.async {
                    UIApplication.shared.registerForRemoteNotifications()
                }
            }
        })
    }

    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        LoggingHelper.logError(ForClass: VoctagAppDelegate.self, with: "Did fail to register for remote notifications \(error)")
    }

    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        // Must be hex for Amazon Push Service
        let deviceTokenString = deviceToken.map { String(format: "%02.2hhx", arguments: [$0]) }.joined()
        self.deviceToken = deviceTokenString
        RequestHelper.shared.registerDeviceToken()
    }
    
    fileprivate func parseRemoteNotification(_ userInfo: [AnyHashable : Any]) {
        if let clientSlug = userInfo["client_slug"] as? String,
            let clientName = userInfo["client_name"] as? String,
            let clientDescription = userInfo["client_description"] as? String,
            let clientMembershipsCount = userInfo["client_memberships_count"] as? Int,
            let clientThemeColor = userInfo["client_theme_color"] as? String,
            let clientVocsCount = userInfo["client_vocs_count"] as? Int {
            
            let client = Client.init(clientSlug: clientSlug, themeColor: clientThemeColor)
            client.name = clientName
            client.clientDescription = clientDescription
            client.memberships_count = clientMembershipsCount
            client.vocsCount = clientVocsCount
            client.following = true
            
            if let _audioURL = userInfo["client_main_audio_url"] as? String, !_audioURL.isEmpty, _audioURL != "null" {
                client.audioURL = URL.init(string: _audioURL)!
            }
            
            if let urls = userInfo["client_logo_urls"] as? [String: Any], !urls.isEmpty {
                client.smallIconURL = URL.init(string: urls[VoctagKeys.CLIENT_ICON_SMALL] as! String)!
                client.mediumIconURL = URL.init(string: urls[VoctagKeys.CLIENT_ICON_MEDIUM] as! String)!
            }
            
            if tabBarController != nil {
                tabBarController!.openClient(client, followingListener: nil)
            } else {
                shouldOpenClient = client
            }
        } else if let vocID = userInfo["voc_id"] as? Int {
            let voc = Voc.init(withVocID: vocID)
            
            if let clientSlug = userInfo["client_slug"] as? String, let clientThemeColor = userInfo["client_theme_color"] as? String {
                voc.parentClient = Client.init(clientSlug: clientSlug, themeColor: clientThemeColor)
            }
            
            if tabBarController != nil {
                tabBarController!.openVoc(voc, shouldScrollToVoc: nil, deletedVocListener: nil)
            } else {
                shouldOpenVoc = voc
            }
        }
    }
    
    fileprivate func refreshCommunityViewControllerIfNecessary() {
        if tabBarController != nil {
            guard let naviController = tabBarController!.selectedViewController as? UINavigationController else {
                return
            }
            
            if let communityVC = naviController.visibleViewController as? CommunityViewController {
                communityVC.loadData()
            }
        }
    }
    
}

extension VoctagAppDelegate: UNUserNotificationCenterDelegate {
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        let userInfo = response.notification.request.content.userInfo
        parseRemoteNotification(userInfo)
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        completionHandler([.badge, .sound, .alert])
    }
    
}


//
//  StoryboardManager.swift
//  voctag
//
//  Created by Jann Driessen on 29.08.16.
//  Copyright © 2016 voctag. All rights reserved.
//

import UIKit

class StoryboardManager {
    
    fileprivate class func mainStoryboard() -> UIStoryboard {
        return UIStoryboard(name: "Main", bundle: nil)
    }
    
    // MARK: - Main ViewControllers

    class func tabBarViewController() -> VoctagTabBarController {
        return StoryboardManager.mainStoryboard().instantiateViewController(withIdentifier: "TabBarViewController") as! VoctagTabBarController
    }
    
    class func feedViewController() -> FeedViewController {
        return StoryboardManager.mainStoryboard().instantiateViewController(withIdentifier: "FeedViewController") as! FeedViewController
    }
    
    class func communityViewController() -> CommunityViewController {
        return StoryboardManager.mainStoryboard().instantiateViewController(withIdentifier: "CommunityViewController") as! CommunityViewController
    }
    
    class func feedDetailViewController() -> FeedDetailViewController {
        return StoryboardManager.mainStoryboard().instantiateViewController(withIdentifier: "FeedDetailViewController") as! FeedDetailViewController
    }
    
    class func feedFilterViewController() -> FeedFilterViewController {
        return StoryboardManager.mainStoryboard().instantiateViewController(withIdentifier: "FeedFilterViewController") as! FeedFilterViewController
    }
    
    class func publishViewController() -> PublishViewController {
        return StoryboardManager.mainStoryboard().instantiateViewController(withIdentifier: "PublishViewController") as! PublishViewController
    }
    
    class func recordViewController() -> RecordViewController {
        return StoryboardManager.mainStoryboard().instantiateViewController(withIdentifier: "RecordViewController") as! RecordViewController
    }
    
    class func playerViewController() -> PlayerViewController {
        return StoryboardManager.mainStoryboard().instantiateViewController(withIdentifier: "PlayerViewController") as! PlayerViewController
    }
    
    class func settingsViewController() -> SettingsViewController {
       return StoryboardManager.mainStoryboard().instantiateViewController(withIdentifier: "SettingsViewController") as! SettingsViewController
    }
    
    class func searchViewController() -> SearchViewController {
        return StoryboardManager.mainStoryboard().instantiateViewController(withIdentifier: "SearchViewController") as! SearchViewController
    }
    
    class func voteViewController() -> VoteViewController {
        return StoryboardManager.mainStoryboard().instantiateViewController(withIdentifier: "VoteViewController") as! VoteViewController
    }
    
    class func splashViewController() -> SplashViewController {
        return StoryboardManager.mainStoryboard().instantiateViewController(withIdentifier: "SplashViewController") as! SplashViewController
    }
    
    class func registerViewController() -> RegisterViewController {
        return StoryboardManager.mainStoryboard().instantiateViewController(withIdentifier: "RegisterViewController") as! RegisterViewController
    }
    
    class func clientDetailViewController() -> ClientDetailViewController {
        return StoryboardManager.mainStoryboard().instantiateViewController(withIdentifier: "ClientDetailViewController") as! ClientDetailViewController
    }
    
    class func clientAccessViewController() -> ClientAccessViewController {
        return StoryboardManager.mainStoryboard().instantiateViewController(withIdentifier: "ClientAccessViewController") as! ClientAccessViewController
    }
    
}

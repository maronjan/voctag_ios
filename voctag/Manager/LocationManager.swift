//
//  LocationManager.swift
//  voctag
//
//  Created by Jann Driessen on 16.09.16.
//  Copyright © 2016 voctag. All rights reserved.
//

import CoreLocation
import UIKit

class LocationManager: NSObject, CLLocationManagerDelegate {

    static let shared = LocationManager()
    
    fileprivate lazy var locationManager = CLLocationManager()
    fileprivate var currentViewController: UIViewController?
    
    func getLocationIfPossible(ForController controller: UIViewController) {
        currentViewController = controller
        switch CLLocationManager.authorizationStatus() {
        case .authorizedWhenInUse:
            startReceivingLocationUpdates()
        case .notDetermined:
            locationManager.requestWhenInUseAuthorization()
        case .restricted, .denied:
            showGoToSettingsAlert()
        default:
            return
        }
    }
    
    fileprivate func showGoToSettingsAlert() {
        let alertController = UIAlertController(
            title: "location_service_alert_button_title".getTranslation(),
            message: "location_service_alert_button_message".getTranslation(),
            preferredStyle: .alert)
        
        let cancelAction = UIAlertAction(title: "location_service_alert_button_cancel".getTranslation(), style: .cancel, handler: nil)
        alertController.addAction(cancelAction)
        
        let openAction = UIAlertAction(title: "location_service_alert_button_open".getTranslation(), style: .default) { (action) in
            if let url = URL(string: UIApplicationOpenSettingsURLString) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            }
        }
        alertController.addAction(openAction)
        
        currentViewController?.present(alertController, animated: true, completion: nil)
    }
    
    fileprivate func startReceivingLocationUpdates() {
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyKilometer
        locationManager.startUpdatingLocation()
    }
    
    // MARK: - CLLocationManagerDelegate
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        manager.stopUpdatingLocation()
        
        if let user = PersistDataHelper.shared.loadUser() {
            user.latitude = locations.last!.coordinate.latitude
            user.longitude = locations.last!.coordinate.longitude
            PersistDataHelper.shared.saveUser(user)
        }

    }
    
}

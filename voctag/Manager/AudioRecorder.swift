//
//  AudioRecorder.swift
//  voctag
//
//  Created by Jann Driessen on 07.09.16.
//  Copyright © 2016 voctag. All rights reserved.
//

import AVFoundation

class AudioRecorder {
    
    // MARK: - Reactive Variables
    
    var onStartRecording: (() -> Void)?
    
    // MARK: - AudioSession
    
    class func activateAudioSession() {
        do {
            try AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayAndRecord, with: [.defaultToSpeaker, .allowBluetooth])
            try AVAudioSession.sharedInstance().setActive(true)
        } catch {
            LoggingHelper.logError(ForClass: AudioRecorder.self, with: error.localizedDescription)
        }
    }

    class func requestPermissionIfNecessary(completion: @escaping (Bool) -> Void) {
        switch AVAudioSession.sharedInstance().recordPermission() {
        case .granted:
            completion(true)
        default:
            // Request permission to record if not granted or undetermined
            AVAudioSession.sharedInstance().requestRecordPermission { (allowed) in
                completion(allowed)
            }
        }
    }
    
    // MARK: - Recorder
    
    static let recordingMinInSeconds: Int = 1
    static let recordingMaxInSeconds: Int = 90
    
    fileprivate var audioRecorder: AVAudioRecorder!
    
    var isRecording = false
    var recordAudioURL: URL?
    
    // MARK: - Public
    
    func didRecordAudio() -> Bool {
        return audioRecorder != nil
    }

    func startRecording() {
        DispatchQueue.main.async {
            AudioRecorder.activateAudioSession()
            
            // Create audio file path
            let timestamp = Date().timeIntervalSince1970
            self.recordAudioURL = self.urlForNewVoc(timestamp: timestamp)
            
            let recordSettings = [
                AVFormatIDKey: Int(kAudioFormatMPEG4AAC),
                AVSampleRateKey: 44100.0,
                AVNumberOfChannelsKey: 2,
                AVEncoderAudioQualityKey: AVAudioQuality.high.rawValue
                ] as [String : Any]
            
            do {
                self.audioRecorder = try AVAudioRecorder(url: self.recordAudioURL!, settings: recordSettings)
                if self.audioRecorder.prepareToRecord() {
                    self.audioRecorder.record(forDuration: TimeInterval(AudioRecorder.recordingMaxInSeconds))
                    self.isRecording = true
                    self.onStartRecording?()
                } else {
                    LoggingHelper.logError(ForClass: AudioRecorder.self, with: "AudioRecorder couldnt be prepared")
                }
            } catch {
                LoggingHelper.logError(ForClass: AudioRecorder.self, with: error.localizedDescription)
            }
        }
    }
    
    func stopRecording() {
        audioRecorder.stop()
        isRecording = false
    }
    
    func discardRecording() {
        if audioRecorder != nil {
            if isRecording {
                stopRecording()
            }
            
            deleteFile(ForURL: recordAudioURL!)
            audioRecorder = nil
            recordAudioURL = nil
        }
    }
    
    // MARK: - Private
    
    fileprivate func deleteFile(ForURL fileURL: URL) {
        do {
            try FileManager.default.removeItem(at: fileURL)
        } catch {
            LoggingHelper.logError(ForClass: AudioRecorder.self, with: "File could not be removed.")
        }
    }
    
    fileprivate func urlForNewVoc(timestamp: TimeInterval) -> URL {
        let cacheDir = FileManager.default.urls(for: .cachesDirectory, in: .userDomainMask)[0]
        let fileName = "voc_audio_\(Int(timestamp)).aac"
        let url = cacheDir.appendingPathComponent(fileName)
        return url
    }
    
}

//
//  VocsPlaylistManager.swift
//  voctag
//
//  Created by Jann Driessen on 13.11.16.
//  Copyright © 2016 voctag. All rights reserved.
//

import UIKit

class PlaylistManager {
    
    // MARK: - Reactive Variables
    
    var onPlaylistDidChange: (() -> Void)?
    
    // MARK: - PlaylistManager
    
    static let shared = PlaylistManager()
    
    fileprivate var playQueue = [Voc]()
    fileprivate var currentIndex = 0
    fileprivate var didCountPlayArray = [Int]() //vocIDs
    
    // MARK: - Public
    
    func playVoc(_ voc: Voc) {
        playOrDownload(voc)
    }
    
    func isPreviousAvailable() -> Bool {
        return currentIndex > 0
    }
    
    func isNextAvailable() -> Bool {
        return currentIndex < playQueue.count-1
    }
    
    func addVocsToPlaylist(vocs: [Voc]) {
        reset()
        playQueue.append(contentsOf: vocs)
    }
    
    func playPrevious() {
        currentIndex -= 1
        if let voc = getCurrentVoc() {
            playVoc(voc)
        }
    }
    
    func playNext() {
        currentIndex += 1
        if let voc = getCurrentVoc() {
            playVoc(voc)
        }
    }
    
    func getCurrentVoc() -> Voc? {
        if playQueue.count >= 1 && currentIndex < playQueue.count {
            return playQueue[currentIndex]
        } else {
            LoggingHelper.logError(ForClass: PlaylistManager.self, with: "index for voc not found")
            return nil
        }
    }
    
    func reset() {
        playQueue.removeAll()
        didCountPlayArray.removeAll()
        currentIndex = 0
        onPlaylistDidChange = nil
    }
    
    // MARK: - Private

    fileprivate func playOrDownload(_ voc: Voc) {
        if VocDownloadManager.shared.isLocalAudioFileDownloaded(vocID: voc.vocID) {
            //Handle audio
            AudioPlayer.shared.onPrepared = {
                self.onPlaylistDidChange?()
            }
            
            AudioPlayer.shared.onDidFinishPlaying = {
                if (self.isNextAvailable()) {
                    self.playNext()
                } else {
                    self.onPlaylistDidChange?()
                }
            }
            
            if (AudioPlayer.shared.handlePlayingVoc(voc: voc)) {
                if (!didCountPlayArray.contains(voc.vocID)) {
                    didCountPlayArray.append(voc.vocID)
                    RequestHelper.shared.played(VocWithID: voc.vocID)
                }
            }
        } else {
            //Handle download
            VocDownloadManager.shared.onDidFinishDownload = { downloadedVoc in
                self.setIndexToVocID(vocID: downloadedVoc.vocID)
                self.playOrDownload(downloadedVoc)
            }
            
            AudioPlayer.shared.stopAndReset()
            VocDownloadManager.shared.downloadVocAudioFile(voc: voc)
        }
        onPlaylistDidChange?()
    }
    
    fileprivate func setIndexToVocID(vocID: Int) {
        var i = 0
        for v in playQueue {
            if v.vocID == vocID {
                currentIndex = i
                break
            } else {
                i += 1
            }
        }
    }

}

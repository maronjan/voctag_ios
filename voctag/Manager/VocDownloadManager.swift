//
//  VocDownloadManager.swift
//  voctag
//
//  Created by Jann Driessen on 13.11.16.
//  Copyright © 2016 voctag. All rights reserved.
//

import UIKit

class VocDownloadManager: NSObject  {
    
    // MARK: - Reactive Variables
    
    var onDidFinishDownload: ((Voc) -> Void)?
    
    // MARK: - VocDownloadManager
    
    static let shared = VocDownloadManager()
    
    fileprivate var downloadURL: URL?
    
    func isDownloadingURL(audioURL: URL?) -> Bool {
        return downloadURL != nil && audioURL != nil && downloadURL! == audioURL!
    }
    
    func isLocalAudioFileDownloaded(vocID: Int) -> Bool {
        let vocAudioFileURL = urlForAudioFile(vocID: vocID)
        return FileManager.default.fileExists(atPath: vocAudioFileURL.path)
    }
    
    // TODO unschön aber löst das problem erstmal
    func stopDownload() {
        downloadURL = nil
    }
    
    func downloadVocAudioFile(voc: Voc) {
        if voc.audioURL == nil || isDownloadingURL(audioURL: voc.audioURL) {
            return
        }
        
        downloadURL = voc.audioURL
        download(voc: voc) { (audioURL) in
            if audioURL != nil && self.isDownloadingURL(audioURL: audioURL) {
                self.downloadURL = nil
                self.onDidFinishDownload?(voc)
            }
        }
    }
    
    fileprivate func download(voc: Voc, completion: @escaping (_ audioURL: URL?) -> Void) {
        DispatchQueue.global(qos: .background).async {
            do {
                let data = try Data.init(contentsOf: voc.audioURL!)
                
                let path = self.urlForAudioFile(vocID: voc.vocID).path
                FileManager.default.createFile(atPath: path, contents: data, attributes: nil)
                
                DispatchQueue.main.async {
                    completion(voc.audioURL)
                }
            } catch {
                DispatchQueue.main.async {
                    completion(nil)
                }
            }
        }
    }
    
    func urlForAudioFile(vocID: Int) -> URL {
        let cacheDir = FileManager.default.urls(for: .cachesDirectory, in: .userDomainMask)[0]
        let fileName = "voc_audio_\(vocID)"
        let url = cacheDir.appendingPathComponent(fileName)
        return url
    }
    
    func deleteChannelMainAudioFile() {
        if isLocalAudioFileDownloaded(vocID: -1) {
            do {
                try FileManager.default.removeItem(at: urlForAudioFile(vocID: -1))
            } catch {
                LoggingHelper.logError(ForClass: VocDownloadManager.self, with: "File could not be removed.")
            }
        }
    }

}

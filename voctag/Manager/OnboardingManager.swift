//
//  OnboardingManager.swift
//  voctag
//
//  Created by Jan Maron on 15.05.18.
//  Copyright © 2018 voctag. All rights reserved.
//

import UIKit

class OnboardingManager: NSObject {
    
    static let shared = OnboardingManager()
    
    enum OnboardingType {
        case Community
        case General
        case Profile
        
        func text() -> String {
            switch self {
            case .Community:
                return "onboarding_text_community".getTranslation()
            case .General:
                return "onboarding_text_general".getTranslation()
            case .Profile:
                return "onboarding_text_profile".getTranslation()
            }
        }
        
        func key() -> String {
            switch self {
            case .Community:
                return VoctagKeys.ONBOARDING_TYPE_COMMUNITY
            case .General:
                return VoctagKeys.ONBOARDING_TYPE_GENERAL
            case .Profile:
                return VoctagKeys.ONBOARDING_TYPE_PROFILE
            }
        }
    }
    
    // MARK: - Public
    
    func showOnboarding(controller: UIViewController, type: OnboardingType) {
        let alertController = UIAlertController(title: nil, message: type.text(), preferredStyle: .alert)
        
        let confirmAction = UIAlertAction(title: "onboarding_confirm_title".getTranslation(), style: .default) { _ in
            self.userSeenOnboarding(type: type)
        }
        
        alertController.addAction(confirmAction)
        controller.present(alertController, animated: true, completion: nil)
    }
    
    func hasUserSeenOnboarding(type: OnboardingType) -> Bool {
        if let bool = UserDefaults.standard.object(forKey: type.key()) as? Bool {
            return bool
        } else {
            return false
        }
    }
    
    // MARK: - Private
    
    fileprivate func userSeenOnboarding(type: OnboardingType) {
        UserDefaults.standard.set(true, forKey: type.key())
    }
    
}

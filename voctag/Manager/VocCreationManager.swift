//
//  VocCreationManager.swift
//  voctag
//
//  Created by Jann Driessen on 29.09.16.
//  Copyright © 2016 voctag. All rights reserved.
//
import UIKit
import AVFoundation

class VocCreationManager {
    
    // MARK: - Reactive Variables
    
    var onPublished: (() -> Void)?
    var onStartRecording: (() -> Void)?
    
    // MARK: - VocCreationManager
    
    static let shared = VocCreationManager()
    
    fileprivate let audioRecorder = AudioRecorder()
    
    var parentVoc: Voc?
    var parentClient: Client?
    var title: String = ""
    var recordDuration: Float = 0
    
    // MARK: - Public
    
    func didRecordVoc() -> Bool {
        return audioRecorder.didRecordAudio()
    }
    
    func isRecordingAudio() -> Bool {
        return audioRecorder.isRecording
    }
    
    func publishingParameters(secret: Bool) -> [String: Any]? {
        if let base64EncodedAudioFileString = base64EncodedStringForCurrentRecord() {
            let audioBase64DataURIScheme = "data:audio/aac;base64," + base64EncodedAudioFileString
            
            var parameters: [String: Any] = [VoctagKeys.AUDIO: audioBase64DataURIScheme]
            
            if let user = PersistDataHelper.shared.loadUser() {
                if user.hasLocation {
                    parameters[VoctagKeys.LATITUDE] = user.latitude!
                    parameters[VoctagKeys.LONGITUDE] = user.longitude!
                }
            }
            
            if !title.isEmpty {
                parameters[VoctagKeys.TITLE] = title
            }
            
            if parentVoc != nil {
                parameters[VoctagKeys.PARENT_ID] = parentVoc?.vocID
            }
            
            parameters[VoctagKeys.SECRET] = secret
            
            return parameters
        }
        
        return nil
    }
    
    func recordedAudioFileURL() -> URL? {
        return audioRecorder.recordAudioURL
    }
    
    func openRecordView(FromViewController controller: UIViewController) {
        let navigationController = UINavigationController(rootViewController: StoryboardManager.recordViewController())
        navigationController.setupForVoctag()
        controller.present(navigationController, animated: false, completion: nil)
    }

    // MARK: - Record
    
    func startRecordingVoc() {
        audioRecorder.onStartRecording = self.onStartRecording
        audioRecorder.startRecording()
    }
    
    func stopRecordingVoc(recordDuration: Float) {
        audioRecorder.stopRecording()
        self.recordDuration = recordDuration
    }
    
    // MARK: - Reset
    
    func discardRecording() {
        audioRecorder.discardRecording()
    }
    
    func reset() {
        discardRecording()
        parentVoc = nil
        parentClient = nil
        title = ""
        recordDuration = 0
    }
    
    // MARK: - private
    
    fileprivate func base64EncodedStringForCurrentRecord() -> String? {
        if let recordURL = audioRecorder.recordAudioURL {
            do {
                let audioFileData = try Data(contentsOf: recordURL as URL, options: .mappedIfSafe)
                let base64EncodedString = audioFileData.base64EncodedString(options: [])
                return base64EncodedString
            } catch {
                LoggingHelper.logError(ForClass: VocCreationManager.self, with: "Error encoding audio file to base64.")
                return nil
            }
        }
        
        LoggingHelper.logError(ForClass: VocCreationManager.self, with: "Error encoding audio fil; no record audio Url")
        return nil
    }
    
}

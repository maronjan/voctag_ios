//
//  AudioPlayer.swift
//  voctag
//
//  Created by Jann Driessen on 14.10.16.
//  Copyright © 2016 voctag. All rights reserved.
//

import Foundation
import AVFoundation

class AudioPlayer: NSObject {
    
    // MARK: - Reactive Variables
    
    var onPrepared: (() -> Void)?
    var onDidFinishPlaying: (() -> Void)?
    
    // MARK: - AudioPlayer
    
    static let shared = AudioPlayer()
    
    fileprivate var player: AVAudioPlayer!
    fileprivate var currentVocID = 0
    
    // MARK: - Public
    
    func getProgress() -> Float {
        if isPlayerSetUp() {
            return Float(player.currentTime)
        } else {
            return 0
        }
    }
    
    func setProgress(progress: Float) {
        if isPlayerSetUp() {
            player.currentTime = Double(progress)
        }
    }
    
    func handlePlayingVoc(voc: Voc) -> Bool {
        if currentVocID != voc.vocID || !isPlayerSetUp() {
            setupAudioPlayer(voc: voc)
            return true
        }
        return toggle()
    }
    
    func isPlaying() -> Bool {
        if isPlayerSetUp() {
            return player.isPlaying
        } else {
            return false
        }
    }
    
    func isPlayingVoc(vocID: Int) -> Bool {
        return isPlaying() && currentVocID == vocID
    }
    
    func stopAndReset() {
        if isPlayerSetUp() {
            player.stop()
            player = nil
        }
    }
    
    func stop() {
        if isPlayerSetUp() {
            player.pause()
            player.currentTime = 0
        }
    }
    
    // MARK: - Private
    
    fileprivate func isPlayerSetUp() -> Bool {
        return player != nil
    }
    
    fileprivate func toggle() -> Bool {
        if player.isPlaying {
            player.pause()
            return false
        } else {
            player.play()
            return true
        }
    }

    fileprivate func setupAudioPlayer(voc: Voc) {
        stopAndReset()
        currentVocID = voc.vocID
        
        do {
            AudioRecorder.activateAudioSession()
            self.player = try AVAudioPlayer(contentsOf: VocDownloadManager.shared.urlForAudioFile(vocID: self.currentVocID))
            self.player.delegate = self
            self.player.numberOfLoops = 0
            self.player.prepareToPlay()
            self.player.play()
            self.onPrepared?()
        } catch {
            LoggingHelper.logError(ForClass: AudioPlayer.self, with: error.localizedDescription)
        }
    }
    
}

extension AudioPlayer: AVAudioPlayerDelegate {
    
    func audioPlayerDidFinishPlaying(_ player: AVAudioPlayer, successfully flag: Bool) {
        onDidFinishPlaying?()
    }
    
}

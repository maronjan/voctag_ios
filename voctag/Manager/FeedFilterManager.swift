//
//  FeedFilterManager.swift
//  voctag
//
//  Created by Jann Driessen on 18.09.16.
//  Copyright © 2016 voctag. All rights reserved.
//

import Foundation

class FeedFilterManager {
    
    static let shared = FeedFilterManager()
    
    // MARK: - Public
    
    func indexForCurrentlySelectedOrderType() -> Int {
        return currentFilter().orderType.hashValue
    }
    
    func searchParametersForCurrentFilter(query: String?, page: Int = 1) -> [String: Any] {
        let filter = currentFilter()
        
        var parameters = [String: Any]()
        parameters[VoctagKeys.FILTER_ORDER] = filter.orderType.rawValue
        parameters[VoctagKeys.FILTER_PAGE] = page

        if query != nil {
            parameters[VoctagKeys.FILTER_QUERY] = query
        }
        
        return parameters
    }
    
    func updateFilter(withOrderType orderType: Filter.OrderType) {
        let filter = currentFilter()
        filter.orderType = orderType
        PersistDataHelper.shared.saveFilter(filter)
    }
    
    // MARK: - Private
    
    fileprivate func currentFilter() -> Filter {
        var filter = PersistDataHelper.shared.loadFilter()
        if filter == nil {
            filter = Filter()
            PersistDataHelper.shared.saveFilter(filter!)
        }
        
        return filter!
    }

}

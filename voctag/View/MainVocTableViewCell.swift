//
//  MainVocTableViewCell.swift
//  voctag
//
//  Created by Jan Maron on 13.02.18.
//  Copyright © 2018 voctag. All rights reserved.
//

import UIKit

class MainVocTableViewCell: AbstractVocTableViewCell {
    
    @IBOutlet fileprivate var background: UIImageView!
    @IBOutlet fileprivate var moreButton: UIButton!
    
    static let height: CGFloat = 220
    static let cellIdentifier = "MainVocTableViewCell"
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func updateViews() {
        super.updateViews()
        
        if voc.didVote {
            voteImageView.image = voc.didVoteUp ? UIImage.init(named: "upvoted_small_white_24dp") : UIImage.init(named: "downvoted_small_white_24dp")
        } else {
            voteImageView.image = UIImage.init(named: "vote_small_white_24dp")
        }
        
        if voc.parentClient != nil {
            background.image = background.image?.withRenderingMode(.alwaysTemplate)
            background.tintColor = voc.parentClient!.themeColor
        } else {
            background.tintColor = nil
        }
    }
    
    @IBAction fileprivate func onSliderValueChanged(sender: UISlider) {
        AudioPlayer.shared.setProgress(progress: slider.value)
    }
    
    // MARK: - More Actions
    
    @IBAction func more(_ sender: AnyObject) {
        let moreAlertController = UIAlertController.init(title: nil, message: nil, preferredStyle: .alert)
        
        let transcriptAction = UIAlertAction.init(title: "feed_detail_more_transcript_title".getTranslation(), style: .default) { (_) in
            self.showTranscript()
        }
        let followTitle = voc.following ? "feed_detail_more_unfollow_title".getTranslation() : "feed_detail_more_follow_title".getTranslation()
        let followAction = UIAlertAction.init(title: followTitle, style: .default) { (_) in
            self.followVoc()
        }
        let shareAction = UIAlertAction.init(title: "feed_detail_more_share_title".getTranslation(), style: .default) { (_) in
            self.shareVoc()
        }
        let reportAction = UIAlertAction.init(title: "feed_detail_more_report_title".getTranslation(), style: .default) { (_) in
            self.reportVoc()
        }
        let deleteAction = UIAlertAction.init(title: "feed_detail_more_delete_title".getTranslation(), style: .default) { (_) in
            self.showDeleteDialog()
        }
        
        let cancelAction = UIAlertAction(title: "feed_cell_action_cancel".getTranslation(), style: .cancel, handler:  nil)
        
        moreAlertController.addAction(transcriptAction)
        moreAlertController.addAction(shareAction)
        moreAlertController.addAction(followAction)
        moreAlertController.addAction(reportAction)
        if let user = PersistDataHelper.shared.loadUser() {
            if voc.userID == user.id {
                moreAlertController.addAction(deleteAction)
            }
        }
        moreAlertController.addAction(cancelAction)

        controller.present(moreAlertController, animated: true, completion: nil)
    }
    
    @objc fileprivate func reportVoc() {
        VocReportHelper.report(vocID: voc.vocID, forViewController: controller)
    }
    
    @objc fileprivate func shareVoc() {
        if let webURL = voc.shareWebURL {
            let activityViewController = UIActivityViewController(activityItems: [webURL], applicationActivities: [])
            activityViewController.excludedActivityTypes = [.airDrop, .assignToContact, .openInIBooks, .print, .saveToCameraRoll]
            controller.present(activityViewController, animated: true, completion: nil)
        }
    }
    
    @objc fileprivate func followVoc() {
        if voc.following {
            voc.following = false
            RequestHelper.shared.unfollow(VocWithID: voc.vocID)
        } else {
            voc.following = true
            RequestHelper.shared.follow(VocWithID: voc.vocID)
        }
    }
    
    @objc fileprivate func showDeleteDialog() {
        let alertController = UIAlertController(
            title: "delete_voc_alert_title".getTranslation(),
            message: "delete_voc_alert_message".getTranslation(),
            preferredStyle: .alert)
        
        let cancelAction = UIAlertAction(title: "delete_voc_alert_action_cancel".getTranslation(), style: .cancel, handler: nil)
        let deleteAction = UIAlertAction(title: "delete_voc_alert_action_delete".getTranslation(), style: .destructive) { (action) in
            RequestHelper.shared.delete(VocWithID: self.voc.vocID, completion: { [weak self] (success) in
                if success {
                    guard let _self = self else {
                        return
                    }
                    
                    if let detailController = _self.controller as? FeedDetailViewController {
                        detailController.onDeletedVoc?()
                        detailController.dismiss(animated: true, completion: nil)
                    }
                }
            })
        }
        
        alertController.addAction(cancelAction)
        alertController.addAction(deleteAction)
        
        controller.present(alertController, animated: true, completion: nil)
    }
    
}

//
//  CircleProgressView.swift
//  voctag
//
//  Created by Jann Driessen on 23.10.16.
//  Copyright © 2016 voctag. All rights reserved.
//

import UIKit

class CircleProgressView: UIView {
    
    fileprivate var backgroundCircleLayer = CAShapeLayer()
    fileprivate var progressCircleLayer = CAShapeLayer()
    
    fileprivate let startAngle: CGFloat = -90
    
    fileprivate let lineWidth: CGFloat = 10
    fileprivate let progressBackgroundColor = UIColor.init(red: 1.0, green: 1.0, blue: 1.0, alpha: 0.2)
    fileprivate let progressColor = UIColor.white
        
    func setProgress(progress: CGFloat, animated: Bool) {
        if !animated {
            CATransaction.begin()
            CATransaction.setDisableActions(true)
            progressCircleLayer.strokeEnd = progress
            CATransaction.commit()
            return
        }
        
        progressCircleLayer.strokeEnd = progress
    }

    // MARK: - Drawing
    
    override func draw(_ rect: CGRect) {
        drawBackgroundCircle()
        drawProgressCircle()
    }
    
    // MARK: - Helpers
    
    fileprivate func center() -> CGPoint {
        return CGPoint(x: bounds.size.width * 0.5, y: bounds.size.height * 0.5)
    }
    
    fileprivate func degreesToRadians(_ value: CGFloat) -> CGFloat {
        return value * CGFloat(Double.pi) / CGFloat(180.0)
    }
    
    fileprivate func radius() -> CGFloat {
        let center = self.center()
        return min(center.x, center.y)
    }
    
    // MARK: - Layers
    
    // https://developer.apple.com/reference/uikit/uibezierpath/1624358-init
    fileprivate func cgPathForCircle() -> CGPath {
        return UIBezierPath(arcCenter: center(), radius: radius(), startAngle: degreesToRadians(startAngle), endAngle: degreesToRadians(startAngle + 360), clockwise: true).cgPath
    }
    
    fileprivate func drawBackgroundCircle() {
        backgroundCircleLayer.path = cgPathForCircle()
        backgroundCircleLayer.fillColor = UIColor.clear.cgColor
        backgroundCircleLayer.lineWidth = lineWidth
        backgroundCircleLayer.lineCap = kCALineCapRound
        backgroundCircleLayer.strokeColor = progressBackgroundColor.cgColor
        layer.addSublayer(backgroundCircleLayer)
    }
    
    fileprivate func drawProgressCircle() {
        progressCircleLayer.path = cgPathForCircle()
        progressCircleLayer.fillColor = UIColor.clear.cgColor
        progressCircleLayer.lineWidth = lineWidth
        progressCircleLayer.lineCap = kCALineCapRound
        progressCircleLayer.strokeColor = progressColor.cgColor
        progressCircleLayer.rasterizationScale = UIScreen.main.scale
        progressCircleLayer.shouldRasterize = true
        layer.addSublayer(progressCircleLayer)
    }
    
}

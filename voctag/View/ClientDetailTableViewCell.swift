//
//  ClientDetailTableViewCell.swift
//  voctag
//
//  Created by Jan Maron on 26.04.18.
//  Copyright © 2018 voctag. All rights reserved.
//

import UIKit
import Kingfisher

class ClientDetailTableViewCell: UITableViewCell {
    
    // MARK: - Reactive Variables
    
    //var onFollowingChanged: (() -> Void)?
    
    // MARK: - ClientDetailTableViewCell
    
    @IBOutlet fileprivate var playButton: PlayButton!
    @IBOutlet fileprivate var logo: UIImageView!
    @IBOutlet fileprivate var nameLabel: UILabel!
    @IBOutlet fileprivate var descriptionLabel: UILabel!
    @IBOutlet fileprivate var membershipsCountLabel: UILabel!
    @IBOutlet fileprivate var membershipsLabel: UILabel!
    //@IBOutlet fileprivate var followButton: UIButton!
    @IBOutlet fileprivate var gradient: UIImageView!
    
    fileprivate var client: Client!
    fileprivate var tableView: UITableView!
    fileprivate var controller: UIViewController!
    
    static let height: CGFloat = 260
    static let cellIdentifier = "ClientDetailTableViewCell"
    
    //@IBAction func onClickFollowButton(sender: UIButton) {
    //    handleFollow()
    //}
    
    // MARK: - Setup
    
    override func awakeFromNib() {
        membershipsLabel.text = "community_memberships".getTranslation()
    }
    
    func setup(withClient client: Client, TableView tableView: UITableView, andViewController controller: UIViewController) {
        self.client = client
        self.tableView = tableView
        self.controller = controller
        setupClientColor()
        updateViews()
    }
    
    fileprivate func updateViews() {
        nameLabel.text = client.name
        descriptionLabel.text = client.clientDescription
        membershipsCountLabel.text = client.membershipsCountString
        
        if client.mediumIconURL != nil {
            logo.kf.setImage(with: client.mediumIconURL!)
        } else {
            logo.image = nil
        }
        
        /*if client.following {
            followButton.isHidden = false
            followButton.titleLabel?.text = "community_client_unfollow".getTranslation()
        } else {
            followButton.isHidden = true
            followButton.titleLabel?.text = "community_client_follow".getTranslation()
        }*/
        
        if let voc = client.mainAudioFakeVoc {
            playButton.isHidden = false
            playButton.setVoc(voc)
        } else {
            playButton.isHidden = true
        }
    }
    
    fileprivate func setupClientColor() {
        let color = client.themeColor
        gradient.image = gradient.image?.withRenderingMode(.alwaysTemplate)
        gradient.tintColor = color
        //followButton.titleLabel?.textColor = color
        //followButton.tintColor = color
    }
    
    fileprivate func handleFollow() {
        //followButton.isEnabled = false
        if client.following {
            RequestHelper.shared.unfollowClient(clientSlug: client.slug, completion: { success in
                //self.followButton.isEnabled = true
                if success {
                    self.client.following = false
                    self.updateViews()
                    //self.onFollowingChanged?()
                }
            })
        } else {
            RequestHelper.shared.followClient(clientSlug: client.slug, completion: { success in
                //self.followButton.isEnabled = true
                if success {
                    self.client.following = true
                    self.updateViews()
                    //self.onFollowingChanged?()
                }
            })
        }
    }
    
}

//
//  PlayPauseButton.swift
//  voctag
//
//  Created by Jann Driessen on 20.10.16.
//  Copyright © 2016 voctag. All rights reserved.
//

import UIKit

@IBDesignable
class PlayButton: UIView {
    
    @IBOutlet fileprivate var gradient: UIImageView!
    @IBOutlet fileprivate var tapIcon: UIImageView!
    @IBOutlet fileprivate var loadingIndicator: UIActivityIndicatorView!
    @IBOutlet fileprivate var tapArea: UIButton!
    
    fileprivate var voc: Voc!
    
    // MARK: - Setup
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    fileprivate func loadViewFromNib() -> UIView {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: "PlayButton", bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
        return view
    }
    
    fileprivate func setup() {
        let view = loadViewFromNib()
        view.frame = self.bounds
        view.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        addSubview(view)
        
        gradient.layer.masksToBounds = false
        gradient.layer.shadowColor = UIColor.black.cgColor
        gradient.layer.shadowOffset = CGSize(width: 1, height: 1)
        gradient.layer.shadowOpacity = 0.2
        
        tapArea.addTarget(self, action: #selector(onClick), for: .touchUpInside)
    }
    
    func setVoc(_ voc: Voc) {
        self.voc = voc
        updateButtonState()
        
        if let client = voc.parentClient {
            gradient.image = gradient.image!.withRenderingMode(.alwaysTemplate)
            gradient.tintColor = client.themeColor
        } else {
            gradient.tintColor = nil
        }
    }
    
    fileprivate func updateButtonState() {
        if voc == nil {
            hideDownloadAnimation()
            tapIcon.image = UIImage(named: "play_white_24dp")
            return
        }
        
        if VocDownloadManager.shared.isDownloadingURL(audioURL: voc!.audioURL) {
            showDownloadAnimation()
        } else {
            hideDownloadAnimation()
            if AudioPlayer.shared.isPlayingVoc(vocID: voc!.vocID) {
                tapIcon.image = UIImage(named: "pause_white_24dp")
            } else {
                tapIcon.image = UIImage(named: "play_white_24dp")
            }
        }
    }
    
    fileprivate func showDownloadAnimation() {
        loadingIndicator.isHidden = false
        loadingIndicator.startAnimating()
        tapIcon.isHidden = true
        tapArea.isUserInteractionEnabled = false
    }
    
    fileprivate func hideDownloadAnimation() {
        loadingIndicator.isHidden = true
        loadingIndicator.stopAnimating()
        tapIcon.isHidden = false
        tapArea.isUserInteractionEnabled = true
    }
    
    @objc fileprivate func onClick() {
        PlaylistManager.shared.playVoc(self.voc)
    }
    
}

//
//  ClientTableViewCell.swift
//  voctag
//
//  Created by Jan Maron on 26.04.18.
//  Copyright © 2018 voctag. All rights reserved.
//

import UIKit
import Kingfisher

class ClientTableViewCell: UITableViewCell {
    
    // MARK: - Reactive Variables
    
    var onPressedCell: (() -> Void)?
    var onFollowingChanged: (() -> Void)?
    
    // MARK: - ClientTableViewCell
    
    @IBOutlet fileprivate var icon: UIImageView!
    @IBOutlet fileprivate var iconBackground: UIView!
    @IBOutlet fileprivate var badge: UIImageView!
    @IBOutlet fileprivate var badgeCountLabel: UILabel!
    @IBOutlet fileprivate var nameLabel: UILabel!
    @IBOutlet fileprivate var membershipsLabel: UILabel!
    @IBOutlet fileprivate var followButton: UIButton!
    @IBOutlet fileprivate var loadingIndicator: UIActivityIndicatorView!
    
    fileprivate var client: Client!
    fileprivate var tableView: UITableView!
    fileprivate var controller: UIViewController!
    
    static let height: CGFloat = 80
    static let cellIdentifier = "ClientTableViewCell"
    
    @IBAction func onClickFollowButton() {
        handleFollow()
    }
    
    // MARK: - Setup

    override func awakeFromNib() {
        super.awakeFromNib()
        
        iconBackground.layer.cornerRadius = 12
        
        //press on cell
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(handleTapOnCell(gestureRecognizer:)))
        self.addGestureRecognizer(tapGestureRecognizer)
        
        showLoading(show: false)
    }
    
    func setup(withClient client: Client, TableView tableView: UITableView, andViewController controller: UIViewController) {
        self.client = client
        self.tableView = tableView
        self.controller = controller
        
        nameLabel.text = client.name
        membershipsLabel.text = "\(client.membershipsCountString) \("community_memberships".getTranslation())"
        
        iconBackground.backgroundColor = client.themeColor
        
        if client.smallIconURL != nil {
            icon.kf.setImage(with: client.smallIconURL!)
        } else {
            icon.image = nil
        }
        
        badge.isHidden = !client.hasNewVocs
        badgeCountLabel.isHidden = !client.hasNewVocs
        badgeCountLabel.text = client.newVocsCountString
        
        if client.following {
            followButton.setBackgroundImage(UIImage.init(named: "client_unfollow_24dp"), for: .normal)
        } else {
            followButton.setBackgroundImage(UIImage.init(named: "client_follow_24dp"), for: .normal)
        }
    }
    
    fileprivate func handleFollow() {
        if client.isClosed && !client.hasAccess {
            let controller = VoctagAppDelegate.shared.tabBarController
            controller?.openClient(client, followingListener: self.onFollowingChanged)
            return
        }
        
        showLoading(show: true)
        if client.following {
            RequestHelper.shared.unfollowClient(clientSlug: client.slug, completion: { success in
                self.showLoading(show: false)
                if success {
                    self.client.following = false
                    self.onFollowingChanged?()
                }
            })
        } else {
            RequestHelper.shared.followClient(clientSlug: client.slug, completion: { success in
                self.showLoading(show: false)
                if success {
                    self.client.following = true
                    self.onFollowingChanged?()
                }
            })
        }
    }
    
    fileprivate func showLoading(show: Bool) {
        if show {
            followButton.isHidden = true
            loadingIndicator.isHidden = false
            loadingIndicator.startAnimating()
        } else {
            followButton.isHidden = false
            loadingIndicator.isHidden = true
            loadingIndicator.stopAnimating()
        }
    }
    
    @objc fileprivate func handleTapOnCell(gestureRecognizer: UITapGestureRecognizer) {
        onPressedCell?()
    }
    
}

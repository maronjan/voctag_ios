//
//  AbstractVocTableViewCell.swift
//  voctag
//
//  Created by Jan Maron on 18.02.18.
//  Copyright © 2018 voctag. All rights reserved.
//

import UIKit

class AbstractVocTableViewCell: UITableViewCell {
    
    @IBOutlet fileprivate var playButton: PlayButton!
    @IBOutlet fileprivate var nicknameBadgeContainer: UIStackView!
    @IBOutlet fileprivate var clientBadge: UIImageView!
    @IBOutlet fileprivate var nicknameLabel: UILabel!
    @IBOutlet fileprivate var titleLabel: UILabel!
    @IBOutlet fileprivate var transcriptLabel: UILabel!
    @IBOutlet var slider: CustomUISlider!
    @IBOutlet fileprivate var dateLocationLabel: UILabel!
    @IBOutlet fileprivate var voteContainer: UIView!
    @IBOutlet var voteImageView: UIImageView!
    @IBOutlet fileprivate var votingLabel: UILabel!
    @IBOutlet fileprivate var playsLabel: UILabel!
    @IBOutlet var repliesLabel: UILabel!
    @IBOutlet fileprivate var secretImageView: UIImageView!
    
    var voc: Voc!
    fileprivate var tableView: UITableView!
    var controller: UIViewController!
    
    // MARK: - Setup

    override func awakeFromNib() {
        //long press
        let longPressGestureRecognizer = UILongPressGestureRecognizer(target: self, action: #selector(handleLongPress(gestureRecognizer:)))
        longPressGestureRecognizer.minimumPressDuration = 0.3
        self.addGestureRecognizer(longPressGestureRecognizer)
        
        //tap on vote
        let voteTapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(handleTapOnVote(gestureRecognizer:)))
        voteContainer.addGestureRecognizer(voteTapGestureRecognizer)
        
        //tap on transcript
        let transcriptTapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(showTranscript))
        transcriptLabel.addGestureRecognizer(transcriptTapGestureRecognizer)
        transcriptLabel.isUserInteractionEnabled = true
        
        slider.setup(target: self, selector: #selector(updateSlider))
        hideSlider()
    }

    func setup(withVoc voc: Voc, TableView tableView: UITableView, andViewController controller: UIViewController) {
        self.voc = voc
        self.tableView = tableView
        self.controller = controller
        updateViews()
    }
    
    func updateViews() {
        //title label
        if let title = voc.title, !title.isEmpty {
            titleLabel.text = title
            titleLabel.isHidden = false
        } else {
            titleLabel.isHidden = true
        }
        
        transcriptLabel.text = voc.transcriptString
        
        //badge & nickname
        clientBadge.isHidden = !voc.createdByClientOwner
        if let nickname = voc.userNickname, !nickname.isEmpty {
            nicknameLabel.isHidden = false
            nicknameLabel.text = nickname
        } else {
            nicknameLabel.isHidden = true
        }
        nicknameBadgeContainer.isHidden = clientBadge.isHidden && nicknameLabel.isHidden
        
        //play button
        playButton.setVoc(voc)
        
        //voting
        votingLabel.text = voc.scoreString
        
        //meta data
        playsLabel.text = voc.playsString
        repliesLabel.text = voc.repliesString
        dateLocationLabel.text = voc.dateLocationString
        
        secretImageView.isHidden = !voc.secret
        
        //slider
        slider.maximumValue = voc.duration
        
        if AudioPlayer.shared.isPlayingVoc(vocID: voc.vocID) {
            showSlider()
        } else {
            hideSlider()
        }
    }
    
    fileprivate func didVote(up: Bool) {
        if up {
            RequestHelper.shared.upVote(VocWithID: voc.vocID)
        } else {
            RequestHelper.shared.downVote(VocWithID: voc.vocID)
        }
        
        voc.voteUp(up)
        updateViews()
    }
    
    fileprivate func showSlider() {
        transcriptLabel.isHidden = true
        slider.isHidden = false
        slider.updater.isPaused = false
    }
    
    fileprivate func hideSlider() {
        transcriptLabel.isHidden = false
        slider.isHidden = true
        slider.updater.isPaused = true
    }
    
    @objc fileprivate func updateSlider() {
        let progress = AudioPlayer.shared.getProgress()
        slider.setProgress(progress: progress)
    }
    
    // MARK: - Tap
    
    @objc fileprivate func handleTapOnVote(gestureRecognizer: UITapGestureRecognizer) {
        if !voc.didVote {
            showVoteViewController()
        }
    }
    
    fileprivate func showVoteViewController() {
        let voteViewController = StoryboardManager.voteViewController()
        voteViewController.client = voc.parentClient
        voteViewController.onVoted = { up in
            self.didVote(up: up)
        }
        voteViewController.modalPresentationStyle = .overFullScreen
        voteViewController.modalTransitionStyle = .crossDissolve
        controller.present(voteViewController, animated: true, completion: nil)
    }

    // MARK: - Long Press
    
    @objc fileprivate func handleLongPress(gestureRecognizer: UILongPressGestureRecognizer) {
        if gestureRecognizer.state == .began {
            self.showTranscript()
        }
    }
    
    @objc func showTranscript() {
        if voc.transcriptString.isEmpty { return }
        
        let transcriptAlertController = UIAlertController.init(title: voc.title, message: voc.transcriptString, preferredStyle: .actionSheet)
        let cancelAction = UIAlertAction(title: "feed_cell_action_done".getTranslation(), style: .cancel, handler:  nil)
        transcriptAlertController.addAction(cancelAction)
        controller.present(transcriptAlertController, animated: true, completion: nil)
    }

}

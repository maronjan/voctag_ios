//
//  VocTableViewCell
//
//  Created by Jan Maron on 19.12.17.
//

import UIKit

class VocTableViewCell: AbstractVocTableViewCell {
    
    // MARK: - Reactive Variables
    
    var onPressedCell: ((Bool) -> Void)?
    
    // MARK: - VocTableViewCell
        
    @IBOutlet fileprivate var background: UIView!
    @IBOutlet fileprivate var replyButton: UIButton!
    @IBOutlet fileprivate var newRepliesLabel: UILabel!
    
    static let height: CGFloat = 166
    static let cellIdentifier = "VocTableViewCell"
    
    // MARK: - Setup
    
    override func awakeFromNib() {
        super.awakeFromNib()
        background.layer.cornerRadius = 4
        
        background.layer.masksToBounds = false
        background.layer.shadowColor = UIColor.black.cgColor
        background.layer.shadowOffset = CGSize(width: 0, height: 0)
        background.layer.shadowRadius = 2
        background.layer.shadowOpacity = 0.2
        
        //press on cell
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(handleTapOnCell(gestureRecognizer:)))
        self.addGestureRecognizer(tapGestureRecognizer)
        
        slider.setThumbImage(UIImage(), for: .normal)
        slider.isUserInteractionEnabled = false
        
        replyButton.setTitle("feed_detail_reply_button_title".getTranslation(), for: .normal)
    }
    
    override func updateViews() {
        super.updateViews()
        
        if voc.didVote {
            voteImageView.image = voc.didVoteUp ? UIImage.init(named: "upvoted_small_main_light_24dp") : UIImage.init(named: "downvoted_small_main_light_24dp")
        } else {
            voteImageView.image = UIImage.init(named: "vote_small_mid_24dp")
        }
        
        if voc.repliesCount == 0 {
            repliesLabel.isHidden = true
            newRepliesLabel.isHidden = true
        } else if voc.hasNewReplies {
            repliesLabel.isHidden = false
            newRepliesLabel.isHidden = false
            newRepliesLabel.text = "\(voc.newRepliesCountString) \("new".getTranslation())"
        } else {
            repliesLabel.isHidden = false
            newRepliesLabel.isHidden = true
        }
        
        if voc.parentClient != nil {
            let color = voc.parentClient!.themeColor
            slider.minimumTrackTintColor = color
            if (voc.didVote) {
                voteImageView.image = voteImageView.image?.withRenderingMode(.alwaysTemplate)
                voteImageView.tintColor = color
            } else {
                voteImageView.tintColor = nil
            }
        } else {
            voteImageView.tintColor = nil
        }
    }
    
    fileprivate func isMyReply() -> Bool {
        guard let user = PersistDataHelper.shared.loadUser() else {
            return false
        }
        return voc.userID == user.id && voc.parentID != nil
    }
    
    @IBAction fileprivate func onPressReply(_ sender: UIButton) {
        VocCreationManager.shared.parentVoc = voc
        VoctagAppDelegate.shared.tabBarController?.openVoc(voc, shouldScrollToVoc: nil, deletedVocListener: nil)
        VocCreationManager.shared.openRecordView(FromViewController: controller)
    }
    
    @objc fileprivate func handleTapOnCell(gestureRecognizer: UITapGestureRecognizer) {
        onPressedCell?(isMyReply())
    }

}

//
//  ProfileViewController.swift
//  voctag
//
//  Created by Jann Driessen on 29.08.16.
//  Copyright © 2016 voctag. All rights reserved.
//

import UIKit
import MaterialComponents

class ProfileViewController: UIViewController {
    
    @IBOutlet fileprivate var tableView: UITableView!
    @IBOutlet fileprivate var tabBarContainer: UIView!
    @IBOutlet fileprivate var headerBackground: UIImageView!
    @IBOutlet fileprivate var pointsLabel: UILabel!
    @IBOutlet fileprivate var pointsDescriptionLabel: UILabel!
    
    var tabbar: MDCTabBar!
    fileprivate var dataSource: FeedDataSource!
    fileprivate var user: User!
    
    override func awakeFromNib() {
        //needed for tabbar and navigationbar titles
        title = "tabbar_profile_title".getTranslation()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationItem.title = ""
        navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(named: "settings_white_24dp"), style: .plain, target: self, action: #selector(openSettings))
        navigationController?.navigationBar.shadowImage = UIImage()
        
        pointsDescriptionLabel.text = "profile_points_description".getTranslation()
        
        setupTabbar()
        dataSource = FeedDataSource.init(vocs: [Voc](), tableView: tableView, controller: self)
        
        setUser(user: PersistDataHelper.shared.loadUser()!)
        
        if let nickname = user.nickname {
            navigationItem.title = nickname
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        VocCreationManager.shared.parentVoc = nil
        VocCreationManager.shared.parentClient = nil
        VocCreationManager.shared.onPublished = nil
        
        if !OnboardingManager.shared.hasUserSeenOnboarding(type: .Profile) {
            OnboardingManager.shared.showOnboarding(controller: self, type: .Profile)
        }

        loadData()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        AudioPlayer.shared.stopAndReset()
    }
    
    // MARK: - Private
    
    @objc fileprivate func openSettings() {
        let navigationController = UINavigationController(rootViewController: StoryboardManager.settingsViewController())
        navigationController.setupForVoctag()
        present(navigationController, animated: true, completion: nil)
    }
    
    fileprivate func setUser(user: User) {
        self.user = user
        pointsLabel.text = user.pointsString
        dataSource.update(vocs: getVocsForTab())
        
        if let nickname = user.nickname {
            navigationItem.title = nickname
        }
    }
    
    fileprivate func loadData() {
        RequestHelper.shared.loadUser { user in
            self.setUser(user: user)
        }
    }
    
    fileprivate func getVocsForTab() -> [Voc] {
        var arr = [Voc]()
        switch tabbar.selectedItem!.tag {
        case 0:
            arr.append(contentsOf: user.vocs)
        case 1:
            arr.append(contentsOf: user.replies)
        case 2:
            arr.append(contentsOf: filterVocs(user.favorites))
        default:
            break
        }
        return arr
    }
    
    fileprivate func filterVocs(_ favorites: [Voc]) -> [Voc] {
        var arr = [Voc]()
        for voc in favorites {
            if voc.userID != user.id {
                arr.append(voc)
            }
        }
        return arr
    }
    
    fileprivate func setupTabbar() {
        tabbar = MDCTabBar(frame: tabBarContainer.bounds)
        tabbar.delegate = self
        tabbar.items = [
            UITabBarItem(title: "profile_myvocs_title".getTranslation(), image: nil, tag: 0),
            UITabBarItem(title: "profile_replies_title".getTranslation(), image: nil, tag: 1),
            UITabBarItem(title: "profile_myfavorites_title".getTranslation(), image: nil, tag: 2),
        ]
        tabbar.itemAppearance = .titles
        tabbar.alignment = .justified
        tabbar.tintColor = UIColor.white
        tabbar.autoresizingMask = [.flexibleWidth]
        tabbar.sizeToFit()
        tabBarContainer.addSubview(tabbar)
    }

}

extension ProfileViewController: MDCTabBarDelegate {
    
    func tabBar(_ tabBar: MDCTabBar, didSelect item: UITabBarItem) {
        dataSource.update(vocs: getVocsForTab())
    }
    
}

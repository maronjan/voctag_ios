//
//  ClientDetailViewController.swift
//  voctag
//
//  Created by Jan Maron on 11.05.18.
//  Copyright © 2018 voctag. All rights reserved.
//

import UIKit

class ClientDetailViewController: UIViewController {
    
    // MARK: - Reactive Variables
    
    var onFollowingChanged: (() -> Void)?
    
    // MARK: - ClientDetailViewController
    
    @IBOutlet fileprivate var clientDetailTableView: UITableView!
    @IBOutlet fileprivate var recordButton: UIButton!
    @IBOutlet fileprivate var shouldFollowView: UIView!
    @IBOutlet fileprivate var shouldFollowLabel: UILabel!
    @IBOutlet fileprivate var shouldFollowButton: UIButton!
    
    fileprivate var refreshControl: UIRefreshControl!
    fileprivate var dataSource: ClientDetailDataSource!
    
    //fileprivate var filterButton: UIBarButtonItem!
    //fileprivate var searchButton: UIBarButtonItem!
    fileprivate var playerButton: UIBarButtonItem!
    
    var client: Client!
    fileprivate var shouldReload = false
    
    @IBAction func onClickRecordButton(_ sender: UIButton) {
        VocCreationManager.shared.openRecordView(FromViewController: self)
    }
    
    @IBAction func onClickShouldFollowButton(_ sender: UIButton) {
        handleFollow()
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationItem.initBackButton(target: self, selector: #selector(close))
        //filterButton = UIBarButtonItem(image: UIImage(named: "filter_white_24dp"), style: .plain , target: self, action: #selector(showFilterView))
        //searchButton = UIBarButtonItem(image: UIImage(named: "search_white_24dp"), style: .plain , target: self, action: #selector(showSearchView))
        playerButton = UIBarButtonItem(image: UIImage(named: "headphone_white_24dp"), style: .plain , target: self, action: #selector(showPlayerView))
        navigationItem.rightBarButtonItems = [playerButton/*, searchButton, filterButton*/]

        shouldFollowLabel.text = "community_client_overlay".getTranslation()
        shouldFollowButton.setTitle("community_client_follow".getTranslation(), for: .normal)
        
        setupTableView()
        setupShouldFollowView()
        
        PersistDataHelper.shared.saveVocCountForClient(slug: client.slug, count: client.vocsCount)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        tabBarController?.tabBar.isHidden = true
        
        let color = client.themeColor
        navigationController?.navigationBar.barTintColor = color
        
        VocDownloadManager.shared.stopDownload()
        VocDownloadManager.shared.deleteChannelMainAudioFile()
        VocCreationManager.shared.parentClient = client
        VocCreationManager.shared.parentVoc = nil
        VocCreationManager.shared.onPublished = {
            self.shouldReload = true
        }
        
        if client.vocs.isEmpty || shouldReload {
            loadVocs(showLoading: false)
        } else {
            setVocs(client.vocs)
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        AudioPlayer.shared.stopAndReset()
        
        navigationController?.navigationBar.barTintColor = UIColor.init(named: VoctagKeys.COLOR_GRADIENT)
        tabBarController?.tabBar.isHidden = false
    }
    
    @objc fileprivate func close() {
        self.navigationController?.popViewController(animated: false)
        //dismiss(animated: true, completion: nil)
    }
    
    // MARK: - NavigationBar Actions
/*
    @objc fileprivate func showFilterView() {
        let feedFilterViewController = StoryboardManager.feedFilterViewController()
        feedFilterViewController.client = client
        feedFilterViewController.onFilterChanged = {
            self.shouldReload = true
        }
        let navigationController = UINavigationController(rootViewController: feedFilterViewController)
        navigationController.setupForVoctag()
        present(navigationController, animated: true, completion: nil)
    }
    
    @objc fileprivate func showSearchView() {
        let searchController = StoryboardManager.searchViewController()
        searchController.client = client
        let navigationController = UINavigationController(rootViewController: searchController)
        navigationController.setupForVoctag()
        present(navigationController, animated: false, completion: nil)
    }
*/
    @objc fileprivate func showPlayerView() {
        let playerController = StoryboardManager.playerViewController()
        playerController.client = client
        let navigationController = UINavigationController(rootViewController: playerController)
        navigationController.setupForVoctag()
        present(navigationController, animated: false, completion: nil)
    }
    
    // MARK: - Private
    
    fileprivate func setupShouldFollowView() {
        let following = client.following
        //filterButton.isEnabled = following
        //searchButton.isEnabled = following
        shouldFollowView.isHidden = following
        clientDetailTableView.isScrollEnabled = following
        
        setupPlayerButton()
    }
    
    fileprivate func setupPlayerButton() {
        playerButton.isEnabled = !client.vocs.isEmpty && client.following
    }
    
    @objc fileprivate func loadData() {
        loadVocs(showLoading: true)
    }
    
    fileprivate func loadMore(pageIndex: Int) {
        let parameters = FeedFilterManager.shared.searchParametersForCurrentFilter(query: nil, page: pageIndex)
        RequestHelper.shared.getClientVocs(clientSlug: client.slug, parameters: parameters, completion: { response in
            if response != nil {
                self.client.vocs.append(contentsOf: response!)
                self.dataSource.update(client: self.client, resetPageIndex: false)
            }
        })
    }
    
    fileprivate func loadVocs(showLoading: Bool) {
        if showLoading {
            refreshControl.beginRefreshing()
        }
        
        let parameters = FeedFilterManager.shared.searchParametersForCurrentFilter(query: nil)
        RequestHelper.shared.getClientVocs(clientSlug: client.slug, parameters: parameters) { (vocs) in
            if self.refreshControl.isRefreshing {
                self.refreshControl.endRefreshing()
            }
            
            if vocs != nil {
                self.setVocs(vocs!)
            }
        }
    }
    
    fileprivate func setVocs(_ vocs: [Voc]) {
        self.client.vocs = vocs
        shouldReload = false
        AudioPlayer.shared.stopAndReset()
        dataSource.update(client: client)
        setupPlayerButton()
    }
    
    fileprivate func setupTableView() {
        dataSource = ClientDetailDataSource.init(client: client, tableView: clientDetailTableView, controller: self)
        dataSource.onDeletedVoc = {
            self.shouldReload = true
        }
        dataSource.onFollowingChanged = {
            self.setupShouldFollowView()
        }
        
        //pagination
        dataSource.onLoadMore = { pageIndex in
            self.loadMore(pageIndex: pageIndex)
        }
        
        //let color = client.themeColor
        //navigationController?.navigationBar.barTintColor = color
        
        // Refresh control
        refreshControl = UIRefreshControl()
        refreshControl.tintColor = client.themeColor
        refreshControl.addTarget(self, action: #selector(loadData), for: .valueChanged)
        clientDetailTableView.addSubview(refreshControl)
        clientDetailTableView.contentOffset = CGPoint(x: 0, y: -refreshControl.frame.size.height) //bugfix for black refreshcontrol
    }
    
    fileprivate func handleFollow() {
        shouldFollowButton.isEnabled = false
        if client.following {
            RequestHelper.shared.unfollowClient(clientSlug: client.slug) { (success) in
                self.shouldFollowButton.isEnabled = true
                if success {
                    self.client.following = false
                    self.setupShouldFollowView()
                    self.dataSource.update(client: self.client)
                    self.onFollowingChanged?()
                }
            }
        } else {
            RequestHelper.shared.followClient(clientSlug: client.slug) { (success) in
                self.shouldFollowButton.isEnabled = true
                if success {
                    self.client.following = true
                    self.setupShouldFollowView()
                    self.dataSource.update(client: self.client)
                    self.onFollowingChanged?()
                }
            }
        }
    }

}

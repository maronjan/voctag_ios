//
//  SearchViewController.swift
//  voctag
//
//  Created by Jan Maron on 12.02.18.
//  Copyright © 2018 voctag. All rights reserved.
//

import UIKit

class SearchViewController: UIViewController {
    
    @IBOutlet fileprivate var tableView: UITableView!
    @IBOutlet fileprivate var searchHeader: UIView!
    @IBOutlet fileprivate var searchTextField: UITextField!
    @IBOutlet fileprivate var emptyView: UIView!
    @IBOutlet fileprivate var emptyViewLabel: UILabel!
    @IBOutlet fileprivate var loadingIndicator: UIActivityIndicatorView!
    
    var vocs = [Voc]()
    var client: Client?
    fileprivate var dataSource: FeedDataSource!
    
    override func awakeFromNib() {
        //needed for tabbar and navigationbar titles
        title = "tabbar_search_title".getTranslation()
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationController?.navigationBar.shadowImage = UIImage()
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "clear_white_24dp"), style: .plain , target: self, action: #selector(close))

        searchTextField.placeholder = "title_search_placeholder".getTranslation()
        searchTextField.delegate = self
        
        if client != nil {
            self.vocs = client!.vocs
        }
        
        dataSource = FeedDataSource.init(vocs: vocs, tableView: tableView, controller: self)
        
        //setup emptyView
        emptyView.layer.cornerRadius = 8
        emptyViewLabel.text = "search_no_data_label".getTranslation()
        emptyView.isHidden = true
        
        showLoading(false)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        //VoctagAppDelegate.shared.openVoc = { voc in
        //    self.dataSource.openVoc(voc, shouldScrollToVoc: nil)
        //}
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        if client != nil {
            let color = client!.themeColor
            navigationController?.navigationBar.barTintColor = color
            searchHeader.backgroundColor = color
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        AudioPlayer.shared.stopAndReset()
    }
    
    @objc fileprivate func close() {
        dismiss(animated: false, completion: nil)
    }
    
    fileprivate func search(query: String?) {
        showLoading(true)
        let parameters = FeedFilterManager.shared.searchParametersForCurrentFilter(query: query)
        if client != nil {
            RequestHelper.shared.getClientVocs(clientSlug: client!.slug, parameters: parameters) { (response) in
                self.showLoading(false)
                if response != nil {
                    self.setVocs(response!)
                }
            }
        } else {
            RequestHelper.shared.searchVocs(parameters: parameters) { (response) in
                self.showLoading(false)
                if response != nil {
                    self.setVocs(response!)
                }
            }
        }
    }
    
    fileprivate func setVocs(_ vocs: [Voc]) {
        AudioPlayer.shared.stopAndReset()
        dataSource.update(vocs: vocs)
        showEmptyViewIfNecessary(vocs: vocs)
    }
    
    fileprivate func showLoading(_ show: Bool) {
        if show {
            loadingIndicator.isHidden = false
            tableView.isHidden = true
        } else {
            loadingIndicator.isHidden = true
            tableView.isHidden = false
        }
    }

    fileprivate func showEmptyViewIfNecessary(vocs: [Voc]) {
        if vocs.isEmpty {
            emptyView.isHidden = false
        } else {
            emptyView.isHidden = true
        }
    }

}

extension SearchViewController: UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let text = textField.text else {
            return true
        }
        
        let charCount = text.utf16.count + string.utf16.count - range.length
        if charCount > 1 {
            search(query: text)
        } else {
            setVocs(vocs)
        }
        
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.endEditing(true)
        return false
    }
    
}

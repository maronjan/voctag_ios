//
//  VoteViewController.swift
//  voctag
//
//  Created by Jan Maron on 17.02.18.
//  Copyright © 2018 voctag. All rights reserved.
//

import UIKit

class VoteViewController: UIViewController {
    
    // MARK: - Reactive Variables
    
    var onVoted: ((Bool) -> Void)?
    
    // MARK: - VoteViewController
    
    @IBOutlet fileprivate var background: UIView!
    @IBOutlet fileprivate var container: UIView!
    
    @IBOutlet fileprivate var downVoteView: UIView!
    @IBOutlet fileprivate var downVoteImageView: UIImageView!
    @IBOutlet fileprivate var downVoteLabel: UILabel!
    @IBOutlet fileprivate var upVoteView: UIView!
    @IBOutlet fileprivate var upVoteImageView: UIImageView!
    @IBOutlet fileprivate var upVoteLabel: UILabel!
    
    var client: Client?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        container.layer.cornerRadius = 5
        
        //background tap
        let backgroundTapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(onTapBackground(gestureRecognizer:)))
        background.addGestureRecognizer(backgroundTapGestureRecognizer)
        
        //downvote tap
        let downVoteTapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(onTapDownVote(gestureRecognizer:)))
        downVoteView.addGestureRecognizer(downVoteTapGestureRecognizer)
        
        //upvote tap
        let upVoteTapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(onTapUpVote(gestureRecognizer:)))
        upVoteView.addGestureRecognizer(upVoteTapGestureRecognizer)
    }

    @objc fileprivate func onTapBackground(gestureRecognizer: UITapGestureRecognizer) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc fileprivate func onTapDownVote(gestureRecognizer: UITapGestureRecognizer) {
        didVote(up: false)
    }
    
    @objc fileprivate func onTapUpVote(gestureRecognizer: UITapGestureRecognizer) {
        didVote(up: true)
    }
    
    fileprivate func didVote(up: Bool) {
        onVoted?(up)
        background.isUserInteractionEnabled = false
        downVoteView.isUserInteractionEnabled = false
        upVoteView.isUserInteractionEnabled = false
        
        if up {
            if client != nil {
                self.upVoteImageView.tintColor = client!.themeColor
            } else {
                self.upVoteImageView.tintColor = UIColor.init(named: VoctagKeys.COLOR_MAIN_LIGHT)
            }
            
            UIView.animate(withDuration: 0.25, animations: {
                self.upVoteImageView.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
            }, completion: { (_) in
                UIView.animate(withDuration: 0.25, animations: {
                    self.upVoteImageView.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
                }, completion: { (_) in
                    self.dismiss(animated: true, completion: nil)
                })
            })
        } else {
            if client != nil {
                self.downVoteImageView.tintColor = client!.themeColor
            } else {
                self.downVoteImageView.tintColor = UIColor.init(named: VoctagKeys.COLOR_MAIN_LIGHT)
            }
            
            UIView.animate(withDuration: 0.25, animations: {
                self.downVoteImageView.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
            }, completion: { (_) in
                UIView.animate(withDuration: 0.25, animations: {
                    self.downVoteImageView.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
                }, completion: { (_) in
                    self.dismiss(animated: true, completion: nil)
                })
            })
        }
    }

}

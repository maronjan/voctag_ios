//
//  RecordViewController.swift
//  voctag
//
//  Created by Jann Driessen on 29.08.16.
//  Copyright © 2016 voctag. All rights reserved.
//

import UIKit

class RecordViewController: UIViewController {
    
    @IBOutlet fileprivate var circleProgressView: CircleProgressView!
    @IBOutlet fileprivate var progressLabel: UILabel!
    @IBOutlet fileprivate var sLabel: UILabel!
    @IBOutlet fileprivate var instructionsLabel: UILabel!
    @IBOutlet fileprivate var background: UIImageView!
    @IBOutlet fileprivate var recordButtonLabel: UILabel!
    @IBOutlet fileprivate var recordButton: UIButton!
    
    fileprivate var updater: CADisplayLink!
    fileprivate var startTime: Date?
    fileprivate var recordDuration: Float = 0.0
    
    override func viewDidLoad() {
        super.viewDidLoad()

        navigationItem.title = VocCreationManager.shared.parentVoc != nil ? "record_title_reply".getTranslation() : "record_title".getTranslation()
        navigationItem.initBackButton(target: self, selector: #selector(discardNoReset))
        
        instructionsLabel.text = "record_instructions_text".getTranslation()
        
        resetRecording()
        
        //init cadisplaylink
        updater = CADisplayLink(target: self, selector: #selector(updateElapsedTime))
        updater.add(to: RunLoop.main, forMode: RunLoopMode.defaultRunLoopMode)
        
        if let client = VocCreationManager.shared.parentClient {
            let color = client.themeColor
            navigationController?.navigationBar.barTintColor = color
            background.image = background.image?.withRenderingMode(.alwaysTemplate)
            background.tintColor = color
        } else {
            background.tintColor = nil
        }
        
        if VocCreationManager.shared.parentVoc != nil {
            instructionsLabel.isHidden = true
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        AudioPlayer.shared.stopAndReset()
    }

    @IBAction func onClickRecordButton(sender: UIButton) {
        if VocCreationManager.shared.isRecordingAudio() {
            stopRecording()
        } else {
            AudioRecorder.requestPermissionIfNecessary { (allowed) in
                if !allowed {
                    self.showRecordPermissionsAlert()
                } else {
                    self.startRecording()
                }
            }
        }
    }
    
    // MARK: - Private
    
    @objc fileprivate func discardNoReset() {
        discard(reset: false)
    }
    
    // Dismisses view by default
    fileprivate func discard(reset: Bool) {
        if VocCreationManager.shared.didRecordVoc() {
            let discardAlertController = UIAlertController(title:"record_discard_title".getTranslation(), message: "record_discard_text".getTranslation(), preferredStyle: .alert)
            
            let discardAction = UIAlertAction(title: "record_discard_button_discard".getTranslation(), style: .default) { _ in
                if reset {
                    self.resetRecording()
                } else {
                    VocCreationManager.shared.reset()
                    self.dismiss(animated: true, completion: nil)
                }
            }
            
            let cancelAction = UIAlertAction(title: "record_discard_button_cancel".getTranslation(), style: .cancel, handler: nil)
            
            discardAlertController.addAction(discardAction)
            discardAlertController.addAction(cancelAction)
            
            present(discardAlertController, animated: true, completion: nil)
            return
        }
        
        if VocCreationManager.shared.isRecordingAudio() {
            stopRecording()
        }
        
        VocCreationManager.shared.reset()
        self.dismiss(animated: true, completion: nil)
    }
    
    fileprivate func startRecording() {
        if VocCreationManager.shared.didRecordVoc() {
            discard(reset: true)
            return
        }
        
        circleProgressView.setProgress(progress: 0.0, animated: true)
        VocCreationManager.shared.onStartRecording = {
            self.startTime = Date()
            self.setupViews(recording: true)
            self.updateElapsedTime()
            UIApplication.shared.isIdleTimerDisabled = true
        }
        VocCreationManager.shared.startRecordingVoc()
    }
    
    fileprivate func stopRecording() {
        setupViews(recording: false)
        UIApplication.shared.isIdleTimerDisabled = false
        
        if startTime != nil {
            let endTime = Date()
            recordDuration = Float(endTime.timeIntervalSinceReferenceDate - startTime!.timeIntervalSinceReferenceDate)
            VocCreationManager.shared.stopRecordingVoc(recordDuration: recordDuration)
            let minTime = Float(AudioRecorder.recordingMinInSeconds)
            
            if recordDuration > minTime {
                navigationItem.rightBarButtonItem = UIBarButtonItem.init(image: UIImage.init(named: "next_24dp"), style: .plain, target: self, action: #selector(openPublish))
                openPublish()
                return
            }
        }
        
        resetRecording()
    }
    
    @objc fileprivate func openPublish() {
        let controller = StoryboardManager.publishViewController()
        controller.recordDuration = recordDuration
        navigationController?.pushViewController(controller, animated: true)
    }

    fileprivate func showRecordPermissionsAlert() {
        let permissionsAlertController = UIAlertController(title: "record_permission_title".getTranslation(), message: "record_permission_message".getTranslation(), preferredStyle: .alert)
        
        let settingsAction = UIAlertAction(title: "record_permission_button_settings".getTranslation(), style: .default) { _ in
            if let settingsUrl = URL(string: UIApplicationOpenSettingsURLString) {
                UIApplication.shared.open(settingsUrl, options: [:], completionHandler: nil)
            }
        }
        
        let cancelAction = UIAlertAction(title: "record_permission_button_cancel".getTranslation(), style: .cancel, handler: nil)
        
        permissionsAlertController.addAction(settingsAction)
        permissionsAlertController.addAction(cancelAction)
        
        present(permissionsAlertController, animated: true, completion: nil)
    }
    
    @objc fileprivate func updateElapsedTime() {
        if (VocCreationManager.shared.isRecordingAudio()) {
            let elapsedTime = Float(Date().timeIntervalSinceReferenceDate - startTime!.timeIntervalSinceReferenceDate)
            let maxTime = Float(AudioRecorder.recordingMaxInSeconds)
            
            if elapsedTime <= maxTime {
                setElapsedTime(elapsedTime: elapsedTime)
                updater.isPaused = false
                return
            }
            
            setElapsedTime(elapsedTime: maxTime)
            stopRecording()
        }
        updater.isPaused = true
    }
    
    fileprivate func setElapsedTime(elapsedTime: Float) {
        let remainingSeconds = AudioRecorder.recordingMaxInSeconds - Int(elapsedTime)
        progressLabel.text = "\(remainingSeconds)"
        let percentage = 1.0 / Float(AudioRecorder.recordingMaxInSeconds) * elapsedTime
        circleProgressView.setProgress(progress: CGFloat(percentage), animated: false) // progress must be between 0 and 1
    }
    
    fileprivate func setupViews(recording: Bool) {
        recordButton.setBackgroundImage(recording ? UIImage(named: "record_button_stop_white") : UIImage(named: "record_button_white"), for: .normal)
        recordButtonLabel.text = recording ? "stop".getTranslation() : "start".getTranslation()
    }
    
    fileprivate func resetRecording() {
        VocCreationManager.shared.discardRecording()
        navigationItem.rightBarButtonItem = nil
        setupViews(recording: false)
        recordDuration = 0.0
        setElapsedTime(elapsedTime: 0.0)
        circleProgressView.setProgress(progress: 1.0, animated: true)
    }
    
}

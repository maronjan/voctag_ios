//
//  RegisterViewController.swift
//  voctag
//
//  Created by Jan Maron on 22.01.18.
//  Copyright © 2018 voctag. All rights reserved.
//

import UIKit

class RegisterViewController: UIViewController {
    
    fileprivate let NICKNAME_CHARACTER_MAX = 25
    fileprivate let NICKNAME_CHARACTER_MIN = 2

    @IBOutlet fileprivate var background: UIImageView!
    @IBOutlet fileprivate var label1: UILabel!
    @IBOutlet fileprivate var label2: UILabel!
    @IBOutlet fileprivate var hintLabel: UILabel!
    @IBOutlet fileprivate var nicknameTextField: UITextField!
    @IBOutlet fileprivate var registerButton: UIButton!
    
    @IBOutlet fileprivate var termsTextLabel: UILabel!
    @IBOutlet fileprivate var termsLabel: UILabel!
    @IBOutlet fileprivate var termsAndLabel: UILabel!
    @IBOutlet fileprivate var privacyLabel: UILabel!
    @IBOutlet fileprivate var termsText2Label: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        label1.text = "register_text_1".getTranslation()
        label2.text = "register_text_2".getTranslation()
        hintLabel.text = "register_text_3".getTranslation()
        
        termsTextLabel.text = "register_terms_text".getTranslation()
        termsAndLabel.text = "register_terms_and_text".getTranslation()
        termsText2Label.text = "register_terms_text_2".getTranslation()
        
        //terms label
        termsLabel.text = "register_terms_title".getTranslation()
        let termsTap = UITapGestureRecognizer(target: self, action: #selector(showTerms))
        termsLabel.isUserInteractionEnabled = true
        termsLabel.addGestureRecognizer(termsTap)

        //privacy label
        privacyLabel.text = "register_privacy_title".getTranslation()
        let privacyTap = UITapGestureRecognizer(target: self, action: #selector(showPrivacy))
        privacyLabel.isUserInteractionEnabled = true
        privacyLabel.addGestureRecognizer(privacyTap)
        
        //nickname textfield
        nicknameTextField.layer.cornerRadius = 24
        nicknameTextField.attributedPlaceholder = NSAttributedString(string:"register_edittext_hint".getTranslation(), attributes: [NSAttributedStringKey.foregroundColor: UIColor.init(red: 1.0, green: 1.0, blue: 1.0, alpha: 0.6)])
        nicknameTextField.delegate = self
        nicknameTextField.returnKeyType = .done
        
        let leftView = UIView(frame: CGRect(x: 0.0, y: 0.0, width: 18.0, height: 2.0))
        nicknameTextField.leftView = leftView
        nicknameTextField.leftViewMode = .always
        
        //registerButton
        disableRegisterButton(true)
        registerButton.layer.cornerRadius = 24
        registerButton.setTitle("register_register".getTranslation(), for: .normal)
    }
    
    @IBAction func onClickRegister(_ sender: UIButton) {
        disableRegisterButton(true)
        guard let user = PersistDataHelper.shared.loadUser() else { return }
        if !user.acceptedTerms {
            acceptTerms()
        } else {
            registerUser()
        }
    }
    
    fileprivate func disableRegisterButton(_ disable: Bool) {
        if disable {
            registerButton.isEnabled = false
            registerButton.alpha = 0.5
        } else {
            registerButton.isEnabled = true
            registerButton.alpha = 1
        }
    }
    
    fileprivate func acceptTerms() {
        RequestHelper.shared.acceptTerms { (success) in
            if success {
                self.registerUser()
            } else {
                self.disableRegisterButton(false)
            }
        }
    }
    
    fileprivate func registerUser() {
        RequestHelper.shared.registerNickname(nickname: nicknameTextField.text!, successListener: {
            self.present(StoryboardManager.tabBarViewController(), animated: true)
        }) { (taken) in
            self.disableRegisterButton(false)
            if (taken) {
                self.hintLabel.text = "register_hint_error".getTranslation()
                self.hintLabel.textColor = UIColor.red
            }
        }
    }
    
    @objc fileprivate func showTerms() {
        let url = URL.init(string: "voctag_url".getTranslation() + "terms")!
        UIApplication.shared.open(url, options: [:], completionHandler: nil)
    }
    
    @objc fileprivate func showPrivacy() {
        let url = URL.init(string: "voctag_url".getTranslation() + "privacy")!
        UIApplication.shared.open(url, options: [:], completionHandler: nil)
    }

}

extension RegisterViewController: UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let text = textField.text else {
            return true
        }

        let set = CharacterSet(charactersIn: "QWERTZUIOPÜASDFGHJKLÖÄYXCVBNMqwertzuiopüasdfghjklöäyxcvbnm1234567890 ").inverted
        
        let charCount = text.utf16.count + string.utf16.count - range.length
        disableRegisterButton(!(charCount >= NICKNAME_CHARACTER_MIN && charCount <= NICKNAME_CHARACTER_MAX))
        return charCount < NICKNAME_CHARACTER_MAX && string.rangeOfCharacter(from: set) == nil
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.endEditing(true)
        return false
    }
    
}

//
//  CommunityViewController.swift
//  voctag
//
//  Created by Jan Maron on 12.02.18.
//  Copyright © 2018 voctag. All rights reserved.
//

import UIKit

class CommunityViewController: UIViewController {
    
    @IBOutlet fileprivate var tableView: UITableView!
    
    //fileprivate var refreshControl: UIRefreshControl!
    fileprivate var clients = [Client]()
    fileprivate var dataSource: ClientListDataSource!
    fileprivate var shouldReload = false

    override func awakeFromNib() {
        //needed for tabbar and navigationbar titles
        title = "tabbar_communities_title".getTranslation()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationController?.navigationBar.shadowImage = UIImage()
        setupTableView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        VocCreationManager.shared.parentClient = nil
        VocCreationManager.shared.parentVoc = nil
        
        if !OnboardingManager.shared.hasUserSeenOnboarding(type: .Community) {
            OnboardingManager.shared.showOnboarding(controller: self, type: .Community)
        }
        
        //if clients.isEmpty || shouldReload {
            loadData()
        //} else {
        //    setClients(self.clients)
        //}
    }
    
    fileprivate func setupTableView() {
        dataSource = ClientListDataSource.init(clients: clients, tableView: tableView, controller: self)
        dataSource.onFollowingChanged = {
            self.shouldReload = true
        }
        
        // Refresh control
        //refreshControl = UIRefreshControl()
        //refreshControl.tintColor = UIColor.init(named: VoctagKeys.COLOR_MAIN_LIGHT)
        //refreshControl.addTarget(self, action: #selector(loadData), for: .valueChanged)
        //tableView.addSubview(refreshControl)
    }
    
    func loadData() {
        //refreshControl.beginRefreshing()
        RequestHelper.shared.getClients { (clients) in
            guard let clients = clients else { return }
            //self.refreshControl.endRefreshing()
            self.setClients(clients)
        }
    }
    
    fileprivate func setClients(_ clients: [Client]) {
        self.clients = clients
        dataSource.update(clients: clients)
    }

}

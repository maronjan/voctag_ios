//
//  PlayAllViewController.swift
//  voctag
//
//  Created by Jan Maron on 12.02.18.
//  Copyright © 2018 voctag. All rights reserved.
//

import UIKit

class PlayerViewController: UIViewController {
    
    @IBOutlet fileprivate var background: UIImageView!
    @IBOutlet fileprivate var nicknameBadgeContainer: UIStackView!
    @IBOutlet fileprivate var nicknameLabel: UILabel!
    @IBOutlet fileprivate var clientBadge: UIImageView!
    @IBOutlet fileprivate var dateLocationLabel: UILabel!
    @IBOutlet fileprivate var titleLabel: UILabel!
    @IBOutlet fileprivate var repliesLabel: UILabel!
    @IBOutlet fileprivate var prevButton: UIButton!
    @IBOutlet fileprivate var playButton: UIButton!
    @IBOutlet fileprivate var downloadIndicator: UIActivityIndicatorView!
    @IBOutlet fileprivate var nextButton: UIButton!
    @IBOutlet fileprivate var slider: CustomUISlider!
    @IBOutlet fileprivate var progressLabel: UILabel!
    @IBOutlet fileprivate var repliesButton: UIButton!
    
    var vocs = [Voc]()
    var client: Client?
    fileprivate var playlistManager: PlaylistManager!
    fileprivate var firstStart = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationItem.title = "Player"
        navigationItem.initBackButton(target: self, selector: #selector(close))
        tabBarController?.tabBar.isHidden = true
        
        slider.setup(target: self, selector: #selector(updateSlider))

        repliesButton.setTitle("player_open_button_title".getTranslation(), for: .normal)
        
        if client != nil {
            if vocs.isEmpty {
                self.vocs = client!.vocs
            }
            
            let color = client!.themeColor
            navigationController?.navigationBar.barTintColor = color
            background.image = background.image?.withRenderingMode(.alwaysTemplate)
            background.tintColor = color
        } else {
            background.tintColor = nil
        }
        
        setupPlaylist()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        //VoctagAppDelegate.shared.openVoc = { voc in
        //    self.openVoc(voc)
        //}
        
        if firstStart {
            firstStart = false
            if let voc = playlistManager.getCurrentVoc() {
                playlistManager.playVoc(voc)
            }
        }
        
        updateViews()
    }
    
    @objc fileprivate func close() {
        dismiss(animated: true, completion: nil)
    }

    @IBAction fileprivate func onClickPrev(sender: UIButton) {
        playlistManager.playPrevious()
    }
    
    @IBAction fileprivate func onClickPlay(sender: UIButton) {
        if let voc = playlistManager.getCurrentVoc() {
            playlistManager.playVoc(voc)
        }
    }
    
    @IBAction fileprivate func onClickNext(sender: UIButton) {
        playlistManager.playNext()
    }
    
    @IBAction fileprivate func onClickReplies(sender: UIButton) {
        goToReplies()
    }
    
    @IBAction fileprivate func onSliderValueChanged(sender: UISlider) {
        AudioPlayer.shared.setProgress(progress: slider.value)
    }
    
    fileprivate func setupPlaylist() {
        playlistManager = PlaylistManager()
        playlistManager.addVocsToPlaylist(vocs: vocs)
        playlistManager.onPlaylistDidChange = {
            self.updateViews()
        }
    }
    
    fileprivate func updateViews() {
        guard let voc = playlistManager.getCurrentVoc() else { return }
        
        //badge & nickname
        clientBadge.isHidden = !voc.createdByClientOwner
        if let nickname = voc.userNickname, !nickname.isEmpty {
            nicknameBadgeContainer.isHidden = false
            nicknameLabel.text = nickname
            dateLocationLabel.text = voc.dateLocationString
        } else {
            nicknameBadgeContainer.isHidden = true
        }
        
        //title
        titleLabel.text = voc.titleString
        repliesLabel.text = voc.repliesString
        
        //prevButton
        prevButton.isEnabled = playlistManager.isPreviousAvailable()
        
        //playButton
        updatePlayButton(voc: voc)
        
        //nextButton
        nextButton.isEnabled = playlistManager.isNextAvailable()
        
        //slider
        slider.maximumValue = voc.duration
        updateSlider()
    }
    
    @objc fileprivate func updateSlider() {
        let progress = AudioPlayer.shared.getProgress()
        setProgressLabel(progress)
        slider.setProgress(progress: progress)
    }
    
    fileprivate func setProgressLabel(_ progress: Float) {
        guard let voc = playlistManager.getCurrentVoc() else { return }
        
        let time = voc.duration - progress
        let minutes = Int(time) / 60 % 60
        let seconds = Int(time) % 60
        progressLabel.text = String(format:"%i:%02i", minutes, seconds)
    }
    
    fileprivate func goToReplies() {
        guard let voc = playlistManager.getCurrentVoc() else { return }
        openVoc(voc)
    }
    
    fileprivate func openVoc(_ voc: Voc) {
        let detailController = StoryboardManager.feedDetailViewController()
        detailController.voc = voc
        let navigationController = UINavigationController(rootViewController: detailController)
        navigationController.setupForVoctag()
        self.present(navigationController, animated: true, completion: nil)
    }
    
    fileprivate func updatePlayButton(voc: Voc) {
        if VocDownloadManager.shared.isDownloadingURL(audioURL: voc.audioURL) {
            showDownloadAnimation()
        } else {
            hideDownloadAnimation()
            if AudioPlayer.shared.isPlaying() {
                slider.updater.isPaused = false
                playButton.setBackgroundImage(UIImage(named: "player_pause_white"), for: .normal)
            } else {
                slider.updater.isPaused = true
                playButton.setBackgroundImage(UIImage(named: "player_play_white"), for: .normal)
            }
        }
    }
    
    fileprivate func showDownloadAnimation() {
        playButton.isHidden = true
        downloadIndicator.isHidden = false
        downloadIndicator.startAnimating()
    }
    
    fileprivate func hideDownloadAnimation() {
        playButton.isHidden = false
        downloadIndicator.isHidden = true
        downloadIndicator.stopAnimating()
    }

}

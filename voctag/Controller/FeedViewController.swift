//
//  FeedViewController.swift
//  voctag
//
//  Created by Jann Driessen on 29.08.16.
//  Copyright © 2016 voctag. All rights reserved.
//

import UIKit

class FeedViewController: UIViewController {
    
    @IBOutlet fileprivate var feedTableView: UITableView!
    @IBOutlet fileprivate var emptyView: UIView!
    @IBOutlet fileprivate var emptyViewLabel: UILabel!
    @IBOutlet fileprivate var recordButton: UIButton!
    
    fileprivate var refreshControl: UIRefreshControl!
    fileprivate var vocs = [Voc]()
    fileprivate var dataSource: FeedDataSource!
    fileprivate var shouldReload = false
    
    @IBAction func onClickRecordButton(_ sender: UIButton) {
        VocCreationManager.shared.openRecordView(FromViewController: self)
    }
    
    override func awakeFromNib() {
        //needed for tabbar and navigationbar titles
        title = "tabbar_home_title".getTranslation()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationController?.navigationBar.shadowImage = UIImage()
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "filter_white_24dp"), style: .plain , target: self, action: #selector(showFilterView))
        
        let searchBarButtonItem = UIBarButtonItem(image: UIImage(named: "search_white_24dp"), style: .plain , target: self, action: #selector(showSearchView))
        let playerBarButtonItem = UIBarButtonItem(image: UIImage(named: "headphone_white_24dp"), style: .plain , target: self, action: #selector(showPlayerView))
        
        navigationItem.rightBarButtonItems = [playerBarButtonItem, searchBarButtonItem]
        
        setupTableView()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        VocDownloadManager.shared.stopDownload()
        VocCreationManager.shared.parentClient = nil
        VocCreationManager.shared.parentVoc = nil
        VocCreationManager.shared.onPublished = {
            self.shouldReload = true
        }
        
        if !OnboardingManager.shared.hasUserSeenOnboarding(type: .General) {
            OnboardingManager.shared.showOnboarding(controller: self, type: .General)
        }
        
        if vocs.isEmpty || shouldReload {
            loadData()
        } else {
            setVocs(vocs)
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        AudioPlayer.shared.stopAndReset()
    }
    
    // MARK: - NavigationBar Actions
    
    @objc fileprivate func showFilterView() {
        let feedFilterViewController = StoryboardManager.feedFilterViewController()
        feedFilterViewController.onFilterChanged = {
            self.shouldReload = true
        }
        let navigationController = UINavigationController(rootViewController: feedFilterViewController)
        navigationController.setupForVoctag()
        present(navigationController, animated: false, completion: nil)
    }
    
    @objc fileprivate func showSearchView() {
        let searchController = StoryboardManager.searchViewController()
        searchController.vocs = vocs
        let navigationController = UINavigationController(rootViewController: searchController)
        navigationController.setupForVoctag()
        present(navigationController, animated: false, completion: nil)
    }
    
    @objc fileprivate func showPlayerView() {
        let playerController = StoryboardManager.playerViewController()
        playerController.vocs = vocs
        let navigationController = UINavigationController(rootViewController: playerController)
        navigationController.setupForVoctag()
        present(navigationController, animated: false, completion: nil)
    }

    // MARK: - Private
    
    @objc fileprivate func loadData() {
        search()
    }
    
    fileprivate func loadMore(pageIndex: Int) {
        let parameters = FeedFilterManager.shared.searchParametersForCurrentFilter(query: nil, page: pageIndex)
        RequestHelper.shared.searchVocs(parameters: parameters, completion: { response in
            if response != nil {
                self.vocs.append(contentsOf: response!)
                self.dataSource.update(vocs: self.vocs, resetPageIndex: false)
            }
        })
    }
    
    fileprivate func search() {
        self.refreshControl.beginRefreshing()
        let parameters = FeedFilterManager.shared.searchParametersForCurrentFilter(query: nil)
        RequestHelper.shared.searchVocs(parameters: parameters, completion: { vocs in
            self.refreshControl.endRefreshing()
            if vocs != nil {
                self.setVocs(vocs!)
            }
        })
    }
    
    fileprivate func setVocs(_ vocs: [Voc]) {
        self.vocs = vocs
        shouldReload = false
        AudioPlayer.shared.stopAndReset()
        dataSource.update(vocs: vocs)
        showEmptyViewIfNecessary()
    }

    fileprivate func setupTableView() {
        //emptyView
        emptyView.layer.cornerRadius = 8
        emptyViewLabel.text = "feed_no_data_label".getTranslation()
        emptyView.isHidden = true
        
        dataSource = FeedDataSource.init(vocs: vocs, tableView: feedTableView, controller: self)
        dataSource.onDeletedVoc = {
            self.shouldReload = true
        }
        
        //pagination
        dataSource.onLoadMore = { pageIndex in
            self.loadMore(pageIndex: pageIndex)
        }
        
        // Refresh control
        refreshControl = UIRefreshControl()
        refreshControl.tintColor = UIColor.init(named: VoctagKeys.COLOR_MAIN_LIGHT)
        refreshControl.addTarget(self, action: #selector(loadData), for: .valueChanged)
        feedTableView.addSubview(refreshControl)
        feedTableView.contentOffset = CGPoint(x: 0, y: -refreshControl.frame.size.height) //bugfix for black refreshcontrol
    }
    
    fileprivate func showEmptyViewIfNecessary() {
        if vocs.isEmpty {
            emptyView.isHidden = false
        } else {
            emptyView.isHidden = true
        }
    }
}

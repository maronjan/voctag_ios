//
//  SplashViewController.swift
//  voctag
//
//  Created by Jan Maron on 07.06.17.
//  Copyright © 2017 voctag. All rights reserved.
//

import UIKit

class SplashViewController: UIViewController {
    
    @IBOutlet fileprivate var loadingIndicator: UIActivityIndicatorView!
    @IBOutlet fileprivate var versionLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let version = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String {
            versionLabel.text = "v\(version)"
        } else {
            versionLabel.isHidden = true
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        loadingIndicator.startAnimating()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        RequestHelper.shared.loadUser { user in
            self.loadingIndicator.stopAnimating()
            if user.acceptedTerms && user.nickname != nil {
                self.present(StoryboardManager.tabBarViewController(), animated: true)
            } else {
                self.present(StoryboardManager.registerViewController(), animated: true)
            }
        }
    }
    
}

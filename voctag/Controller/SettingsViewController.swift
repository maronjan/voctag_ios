//
//  SettingsViewController.swift
//  voctag
//
//  Created by Jann Driessen on 29.08.16.
//  Copyright © 2016 voctag. All rights reserved.
//

import UIKit
import MessageUI
import SafariServices

class SettingsViewController: UITableViewController {
    
    fileprivate enum SettingsSections: Int {
        case rating
        case legal
        case support
    }
    
    fileprivate var legalRows = [
        ["settings_cell_create_own_channel", "partner"],
        ["settings_cell_legal_codex", "codex"],
        ["settings_cell_legal_termsandconditions", "terms"],
        ["settings_cell_legal_privacypolicy", "privacy"],
        ["settings_cell_legal_imprint", "imprint"]
    ]
        
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationItem.title = "settings_title".getTranslation()
        navigationItem.initBackButton(target: self, selector: #selector(close))
        
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "settingsTableViewCellID")
        tableView.tableFooterView = UIView()
    }
    
    @objc fileprivate func close() {
        dismiss(animated: true, completion: nil)
    }
    
    // MARK: - Private

    fileprivate func openAppStoreRating() {
        if let appStoreUrl = URL(string: "itms-apps://itunes.apple.com/app/id1168720723") {
            UIApplication.shared.open(appStoreUrl, options: [:], completionHandler: nil)
        }
    }

    fileprivate func openSupportEmail() {
        if MFMailComposeViewController.canSendMail() {
            let mailComposerViewController = MFMailComposeViewController()
            mailComposerViewController.mailComposeDelegate = self
            mailComposerViewController.setToRecipients(["support@voctag.com"])
            let messageBody = deviceInfoForSupportMail()
            mailComposerViewController.setMessageBody(messageBody, isHTML: false)
            present(mailComposerViewController, animated: true, completion: nil)
        } else {
            showAlertController(withMessage: "settings_no_mail_configured_message".getTranslation())
        }
    }
    
    fileprivate func showAlertController(withMessage message: String) {
        let alertController = UIAlertController(title: nil, message: message, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "ok".getTranslation(), style: .default, handler: nil))
        present(alertController, animated: true, completion: nil)
    }
    
    fileprivate func deviceInfoForSupportMail() -> String {
        let model = UIDevice.current.model
        let systemVersion = UIDevice.current.systemVersion
        let deviceInfoString = "\n\nSupport Info\n\(model) - iOS \(systemVersion)"
        return deviceInfoString
    }
    
}

extension SettingsViewController: MFMailComposeViewControllerDelegate {
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true, completion: nil)
        if result == .sent {
            showAlertController(withMessage: "settings_mail_support_sent".getTranslation())
        } else if let error = error {
            showAlertController(withMessage: error.localizedDescription)
        }
    }
    
}

// MARK: - UITableViewDelegate

extension SettingsViewController {
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 20
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "settingsTableViewCellID")!
        cell.backgroundColor = UIColor.white
        cell.accessoryType = .disclosureIndicator
        cell.textLabel?.textColor = UIColor.init(named: VoctagKeys.COLOR_DARK)
        cell.textLabel?.font = UIFont.systemFont(ofSize: 15)
        switch SettingsSections(rawValue: (indexPath as NSIndexPath).section) {
        case .some(.rating):
            cell.textLabel?.text = "settings_cell_rating_title".getTranslation()
        case .some(.legal):
            let title = legalRows[(indexPath as NSIndexPath).row][0]
            cell.textLabel?.text = title.getTranslation()
        case .some(.support):
            cell.textLabel?.text = "settings_cell_support_title".getTranslation()
        default:
            break
        }
        return cell
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == SettingsSections.legal.rawValue {
            return legalRows.count
        } else {
            return 1
        }
    }

}

// MARK: - UITableViewDataSource
extension SettingsViewController {
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        switch indexPath.section {
        case SettingsSections.rating.rawValue:
            openAppStoreRating()
        case SettingsSections.support.rawValue:
            openSupportEmail()
        default:
            if indexPath.row == 0 {
                if let url = URL(string: "\("voctag_web_url".getTranslation())\(legalRows[indexPath.row][1])") {
                    UIApplication.shared.open(url, options: [:], completionHandler: nil)
                }
            } else {
                if let url = URL(string: "\("voctag_url".getTranslation())\(legalRows[indexPath.row][1])") {
                    UIApplication.shared.open(url, options: [:], completionHandler: nil)
                }
            }
        }
    }
    
}

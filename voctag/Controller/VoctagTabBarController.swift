//
//  VoctagTabBarController.swift
//  voctag
//
//  Created by Jann Driessen on 04.03.17.
//  Copyright © 2017 voctag. All rights reserved.
//

import UIKit

class VoctagTabBarController: UITabBarController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tabBar.shadowImage = UIImage()
        tabBar.backgroundImage = UIImage()
        tabBar.layer.shadowOffset = CGSize.init(width: 0, height: 0)
        tabBar.layer.shadowRadius = 5
        tabBar.layer.shadowColor = UIColor.black.cgColor
        tabBar.layer.shadowOpacity = 0.3
        
        tabBar.unselectedItemTintColor = UIColor.init(named: VoctagKeys.COLOR_TABICONCOLOR)
        
        VoctagAppDelegate.shared.tabBarController = self
        VoctagAppDelegate.shared.registerRemoteNotifications()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        //user opened app from notification
        if let voc = VoctagAppDelegate.shared.shouldOpenVoc {
            openVoc(voc, shouldScrollToVoc: nil, deletedVocListener: nil)
            VoctagAppDelegate.shared.shouldOpenVoc = nil
        } else if let client = VoctagAppDelegate.shared.shouldOpenClient {
            openClient(client, followingListener: nil)
            VoctagAppDelegate.shared.shouldOpenClient = nil
        }
    }
    
    func openVoc(_ voc: Voc, shouldScrollToVoc: Voc?, deletedVocListener: (() -> Void)?) {
        let detailController = StoryboardManager.feedDetailViewController()
        detailController.voc = voc
        detailController.shouldScrollToVoc = shouldScrollToVoc
        detailController.onDeletedVoc = deletedVocListener
        let naviController = self.selectedViewController as? UINavigationController
        naviController?.pushViewController(detailController, animated: false)
    }
    
    func openClient(_ client: Client, followingListener: (() -> Void)?) {
        if client.isClosed && !client.hasAccess {
            let accessController = StoryboardManager.clientAccessViewController()
            accessController.client = client
            accessController.onFollowingChanged = followingListener
            let naviController = self.selectedViewController as? UINavigationController
            naviController?.pushViewController(accessController, animated: false)
        } else {
            let detailController = StoryboardManager.clientDetailViewController()
            detailController.client = client
            detailController.onFollowingChanged = followingListener
            let naviController = self.selectedViewController as? UINavigationController
            naviController?.pushViewController(detailController, animated: false)
        }
    }

}

//
//  FeedFilterViewController.swift
//  voctag
//
//  Created by Jann Driessen on 18.09.16.
//  Copyright © 2016 voctag. All rights reserved.
//

import UIKit

class FeedFilterViewController: UITableViewController {
    
    // MARK: - Reactive Variables
    
    var onFilterChanged: (() -> Void)?
    
    // MARK: - FeedFilterViewController
    
    fileprivate let orderTypes: [Filter.OrderType] = [.Newest, .MostPlayed, .MostReplies, .MostUpVotes]
    var client: Client?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationItem.title = "feed_filter_title".getTranslation()
        navigationItem.initBackButton(target: self, selector: #selector(close))
        
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "filterTableViewCellID")
        tableView.tableFooterView = UIView()
        
        if client != nil {
            navigationController?.navigationBar.barTintColor = client!.themeColor
        }
    }
    
    // MARK: - Actions
    
    @objc fileprivate func close() {
        dismiss(animated: true, completion: nil)
    }
    
    // MARK: - UITableViewDataSource

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "filterTableViewCellID")!
        
        cell.backgroundColor = UIColor.white
        cell.tintColor = UIColor.init(named: VoctagKeys.COLOR_DARK)
        cell.textLabel?.font = UIFont.systemFont(ofSize: 15)
        cell.textLabel?.textColor = UIColor.init(named: VoctagKeys.COLOR_DARK)
        cell.textLabel?.text = orderTypes[indexPath.row].title()

        if indexPath.row == FeedFilterManager.shared.indexForCurrentlySelectedOrderType() {
            cell.accessoryType = .checkmark
        } else {
            cell.accessoryType = .none
        }
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return orderTypes.count
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return "feed_filter_section_title_order".getTranslation()
    }
    
    // MARK: - UITableViewDelegate
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        FeedFilterManager.shared.updateFilter(withOrderType: orderTypes[indexPath.row])
        tableView.reloadData()
        onFilterChanged?()
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 54.0
    }
    
    override func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        if let headerView = view as? UITableViewHeaderFooterView {
            headerView.tintColor = UIColor.init(named: VoctagKeys.COLOR_BRIGHT)
            headerView.textLabel?.font = UIFont.boldSystemFont(ofSize: 24)
            headerView.textLabel?.textColor = UIColor.init(named: VoctagKeys.COLOR_LIGHT)
        }
    }
    
}

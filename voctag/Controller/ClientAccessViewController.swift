//
//  ClientAccessViewController.swift
//  voctag
//
//  Created by Jan Maron on 22.07.18.
//  Copyright © 2018 voctag. All rights reserved.
//

import UIKit

class ClientAccessViewController: UIViewController {
    
    // MARK: - Reactive Variables
    
    var onFollowingChanged: (() -> Void)?
    
    // MARK: - ClientAccessViewController
    
    @IBOutlet fileprivate var viewHeightConstraint: NSLayoutConstraint!
    @IBOutlet fileprivate var overlay: UIView!
    @IBOutlet fileprivate var overlayBottomConstraint: NSLayoutConstraint!
    @IBOutlet fileprivate var accessCodeTextInput: UITextField!
    @IBOutlet fileprivate var background: UIView!
    @IBOutlet fileprivate var playButton: PlayButton!
    @IBOutlet fileprivate var logo: UIImageView!
    @IBOutlet fileprivate var logoHeightConstraint: NSLayoutConstraint!
    @IBOutlet fileprivate var nameLabel: UILabel!
    @IBOutlet fileprivate var descriptionLabel: UILabel!
    @IBOutlet fileprivate var redeemButton: UIButton!
    @IBOutlet fileprivate var communityLabel: UILabel!
    @IBOutlet fileprivate var infoLabel: UILabel!
    
    @IBOutlet fileprivate var getAccessLabel: UILabel!
    @IBOutlet fileprivate var getAccessCodeLabel: UILabel!
    @IBOutlet fileprivate var getAccessView: UIView!
    @IBOutlet fileprivate var getAccessViewBottomConstraint: NSLayoutConstraint!
    
    var client: Client!
    var fakeVoc: Voc?

    override func viewDidLoad() {
        super.viewDidLoad()

        navigationItem.initBackButton(target: self, selector: #selector(close))
        
        nameLabel.text = client.name
        descriptionLabel.text = client.clientDescription
        
        if UIScreen.main.nativeBounds.height == 1136 { //if iPhone 5s
            logoHeightConstraint.constant = 50
        } else if UIScreen.main.nativeBounds.height == 2436 { //if iPhone X
            getAccessViewBottomConstraint.constant = 50
            viewHeightConstraint.constant = 460
        } else {
            viewHeightConstraint.constant = 400
        }
        
        if client.mediumIconURL != nil {
            logo.kf.setImage(with: client.mediumIconURL!)
        } else {
            logo.image = nil
        }
        
        fakeVoc = client.mainAudioFakeVoc
        
        if fakeVoc != nil {
            playButton.isHidden = false
            playButton.setVoc(fakeVoc!)
        } else {
            playButton.isHidden = true
        }
        
        PlaylistManager.shared.onPlaylistDidChange = {
            if self.fakeVoc != nil {
                self.playButton.setVoc(self.fakeVoc!)
            }
        }
        
        accessCodeTextInput.delegate = self
        accessCodeTextInput.placeholder = "community_closed_textfield_placeholder".getTranslation()
        communityLabel.text = "community_closed_title".getTranslation()
        infoLabel.text = "community_closed_info_text".getTranslation()
        redeemButton.setTitle("community_closed_redeem_button_title".getTranslation(), for: .normal)
        redeemButton.layer.cornerRadius = 6.0
        
        let notificationCenter = NotificationCenter.default
        notificationCenter.addObserver(self, selector: #selector(keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        notificationCenter.addObserver(self, selector: #selector(keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        if client.salesURL != nil {
            getAccessCodeLabel.text = client.salesURL!
            getAccessLabel.text = "community_closed_sales_label_title".getTranslation()
            
            let tapGestureRecognizer = UITapGestureRecognizer.init(target: self, action: #selector(self.getAccessCodeLabelClicked(gestureRecognizer:)))
            getAccessView.addGestureRecognizer(tapGestureRecognizer)
        } else {
            getAccessView.isHidden = true
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        tabBarController?.tabBar.isHidden = true
        VocDownloadManager.shared.stopDownload()
        VocDownloadManager.shared.deleteChannelMainAudioFile()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        AudioPlayer.shared.stopAndReset()
        
        navigationController?.navigationBar.barTintColor = UIColor.init(named: VoctagKeys.COLOR_GRADIENT)
        tabBarController?.tabBar.isHidden = false
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        let path =  UIBezierPath(roundedRect: overlay.bounds, byRoundingCorners: [.topLeft, .topRight], cornerRadii: CGSize(width: 32.0, height: 32.0))
        let layer = CAShapeLayer()
        layer.fillColor = UIColor.white.cgColor
        layer.frame = overlay.bounds
        layer.path = path.cgPath
        overlay.layer.insertSublayer(layer, at: 0)
        
        overlay.layer.masksToBounds = false
        overlay.layer.shadowColor = UIColor.black.cgColor
        overlay.layer.shadowOffset = CGSize(width: 0, height: 0)
        overlay.layer.shadowRadius = 2
        overlay.layer.shadowPath = path.cgPath
        overlay.layer.shadowOpacity = 0.3
        
        let color = client.themeColor
        navigationController?.navigationBar.barTintColor = color
        background.backgroundColor = color
        redeemButton.backgroundColor = color
    }
    
    @objc fileprivate func close() {
        self.navigationController?.popViewController(animated: false)
    }
    
    @objc fileprivate func getAccessCodeLabelClicked(gestureRecognizer: UITapGestureRecognizer) {
        if validateEmail(email: client.salesURL!) {
            let url = URL.init(string: "mailto:\(client.salesURL!)")!
            UIApplication.shared.open(url)
        } else {
            let url = URL.init(string: client.salesURL!)!
            UIApplication.shared.open(url)
        }
    }
    
    fileprivate func validateEmail(email: String) -> Bool {
        let emailFormat = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailPredicate = NSPredicate(format:"SELF MATCHES %@", emailFormat)
        return emailPredicate.evaluate(with: email)
    }

    @IBAction func redeemAccessCode(_ sender: UIButton) {
        redeemButton.isEnabled = false
        RequestHelper.shared.redeemAccessCode(clientSlug: client.slug, accessCode: accessCodeTextInput.text!, successListener: {
            DispatchQueue.main.async {
                self.client.hasAccess = true
                self.client.following = true
                self.onFollowingChanged?()
                
                self.navigationController?.popViewController(animated: false)
                let controller = VoctagAppDelegate.shared.tabBarController
                controller?.openClient(self.client, followingListener: self.onFollowingChanged)
            }
        }) { (invalid) in
            DispatchQueue.main.async {
                self.redeemButton.isEnabled = true
                if invalid {
                    self.infoLabel.text = "community_closed_wrong_code".getTranslation()
                    self.infoLabel.textColor = UIColor.red
                }
            }
        }
    }
    
    @objc fileprivate func keyboardWillShow(_ notification: Notification) {
        let userInfo = (notification as NSNotification).userInfo!
        let keyboardHeight =  (userInfo[UIKeyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
        overlayBottomConstraint.constant = keyboardHeight.height
        UIView.animate(withDuration: 0.2) {
            self.view.layoutIfNeeded()
        }
    }
    
    @objc fileprivate func keyboardWillHide(_ notification: Notification) {
        overlayBottomConstraint.constant = 0
        UIView.animate(withDuration: 0.2) {
            self.view.layoutIfNeeded()
        }
    }

}

extension ClientAccessViewController: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.endEditing(true)
        return false
    }
    
}

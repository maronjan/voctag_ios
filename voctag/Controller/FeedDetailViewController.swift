//
//  FeedDetailViewController.swift
//  voctag
//
//  Created by Jann Driessen on 05.11.16.
//  Copyright © 2016 voctag. All rights reserved.
//

import UIKit

class FeedDetailViewController: UIViewController {
    
    // MARK: - Reactive Variables
    
    var onDeletedVoc: (() -> Void)?
    
    // MARK: - FeedDetailViewController
    
    @IBOutlet fileprivate var feedDetailTableView: UITableView!
    @IBOutlet fileprivate var replyButton: UIButton!
    
    @IBOutlet fileprivate var emptyView: UIView!
    @IBOutlet fileprivate var emptyViewLabel: UILabel!
    
    fileprivate var refreshControl: UIRefreshControl!
    fileprivate var dataSource: FeedDetailDataSource!
    
    var voc: Voc!
    var shouldScrollToVoc: Voc?
    fileprivate var vocDetail: Voc?
    fileprivate var shouldReload = false
    fileprivate var playerButton: UIBarButtonItem!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationItem.initBackButton(target: self, selector: #selector(close))
        playerButton = UIBarButtonItem(image: UIImage(named: "headphone_white_24dp"), style: .plain , target: self, action: #selector(showPlayer))
        navigationItem.rightBarButtonItem = playerButton
        
        replyButton.setTitle("feed_detail_reply_button_title".getTranslation(), for: .normal)
        replyButton.layer.cornerRadius = 24
        replyButton.layer.masksToBounds = false
        replyButton.layer.shadowColor = UIColor.black.cgColor
        replyButton.layer.shadowOffset = CGSize(width: 1, height: 1)
        replyButton.layer.shadowOpacity = 0.2
        
        setupTableView()
        setupPlayerButton()
        
        PersistDataHelper.shared.saveVocCountForVoc(vocID: voc.vocID, count: voc.repliesCount)
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        tabBarController?.tabBar.isHidden = true
        
        if voc.parentClient != nil {
            let color = voc.parentClient!.themeColor
            navigationController?.navigationBar.barTintColor = color
            refreshControl.tintColor = color
        }
        
        VocDownloadManager.shared.stopDownload()
        VocCreationManager.shared.parentClient = voc.parentClient
        VocCreationManager.shared.parentVoc = voc
        VocCreationManager.shared.onPublished = {
            self.shouldReload = true
        }
        
        if vocDetail == nil || shouldReload {
            loadData()
        } else {
            setVoc(vocDetail)
        }
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        AudioPlayer.shared.stopAndReset()
        
        navigationController?.navigationBar.barTintColor = UIColor.init(named: VoctagKeys.COLOR_GRADIENT)
        tabBarController?.tabBar.isHidden = false
    }

    @objc fileprivate func close() {
        self.navigationController?.popViewController(animated: false)
        //dismiss(animated: true, completion: nil)
    }
    
    @IBAction func onClickReply(_ sender: UIButton) {
        VocCreationManager.shared.openRecordView(FromViewController: self)
    }
    
    // MARK: - Private
    
    @objc fileprivate func showPlayer() {
        let playerController = StoryboardManager.playerViewController()
        playerController.vocs = vocDetail!.replies
        playerController.client = voc.parentClient
        let navigationController = UINavigationController(rootViewController: playerController)
        navigationController.setupForVoctag()
        self.present(navigationController, animated: false, completion: nil)
    }
    
    fileprivate func setVoc(_ voc: Voc?) {
        self.vocDetail = voc
        shouldReload = false
        AudioPlayer.shared.stopAndReset()
        dataSource.update(vocs: getArrayList())
        showEmptyViewIfNecessary()
        scrollToVocIfNecessary()
        setupPlayerButton()
    }
    
    fileprivate func getArrayList() -> [Voc] {
        var arr = [Voc]()
        if vocDetail != nil {
            arr.append(vocDetail!)
            arr.append(contentsOf: vocDetail!.replies)
        }
        return arr
    }
    
    fileprivate func scrollToVocIfNecessary() {
        if vocDetail == nil || vocDetail!.replies.isEmpty || shouldScrollToVoc == nil {
            return
        }
        
        let indexPath = IndexPath.init(row: getRowForVoc(shouldScrollToVoc!), section: 0)
        feedDetailTableView.scrollToRow(at: indexPath, at: .top, animated: true)
        shouldScrollToVoc = nil
    }
    
    fileprivate func getRowForVoc(_ voc: Voc) -> Int {
        var i = 0
        for v in vocDetail!.replies {
            if v.vocID == voc.vocID {
                break
            } else {
                i += 1
            }
        }
        
        return i
    }

    @objc fileprivate func loadData() {
        self.refreshControl.beginRefreshing()
        if let client = voc.parentClient {
            RequestHelper.shared.loadClientVocDetail(clientSlug: client.slug, id: voc.vocID) { [weak self] (voc) in
                guard let _self = self else { return }
                _self.setVoc(voc)
                _self.refreshControl.endRefreshing()
            }
        } else {
            RequestHelper.shared.loadVocDetail(id: voc.vocID) { [weak self] (voc) in
                guard let _self = self else { return }
                _self.setVoc(voc)
                _self.refreshControl.endRefreshing()
            }
        }
    }
    
    fileprivate func setupPlayerButton() {
        playerButton.isEnabled = vocDetail != nil && !vocDetail!.replies.isEmpty
    }
    
    fileprivate func showEmptyViewIfNecessary() {
        if getArrayList().isEmpty {
            emptyView.isHidden = false
        } else {
            emptyView.isHidden = true
        }
    }
    
    fileprivate func setupTableView() {
        //emptyView
        emptyView.layer.cornerRadius = 8
        emptyViewLabel.text = "feed_detail_no_data_label".getTranslation()
        emptyView.isHidden = true
        
        dataSource = FeedDetailDataSource.init(vocs: getArrayList(), client: voc.parentClient, tableView: feedDetailTableView, controller: self)
        dataSource.onDeletedVoc = {
            self.shouldReload = true
        }
        
        // Refresh control
        refreshControl = UIRefreshControl()
        refreshControl.tintColor = UIColor.init(named: VoctagKeys.COLOR_MAIN_LIGHT)
        refreshControl.addTarget(self, action: #selector(loadData), for: .valueChanged)
        feedDetailTableView.addSubview(refreshControl)
        feedDetailTableView.contentOffset = CGPoint(x: 0, y: -refreshControl.frame.size.height) //bugfix for black refreshcontrol
        /*
        if voc.parentClient != nil {
            let color = voc.parentClient!.themeColor
            navigationController?.navigationBar.barTintColor = color
            refreshControl.tintColor = color
        }*/
    }

}

//
//  PublishViewController.swift
//  voctag
//
//  Created by Jann Driessen on 01.09.16.
//  Copyright © 2016 voctag. All rights reserved.
//

import UIKit
import AVFoundation

class PublishViewController: UIViewController {
    
    fileprivate let TITLE_CHARACTER_MAX = 60
    
    @IBOutlet fileprivate var background: UIImageView!
    
    @IBOutlet fileprivate var titleTextField: UITextField!
    @IBOutlet fileprivate var characterCountLabel: UILabel!
    
    @IBOutlet fileprivate var slider: CustomUISlider!
    @IBOutlet fileprivate var playButton: UIButton!
    
    @IBOutlet fileprivate var secretView: UIView!
    @IBOutlet fileprivate var secretLabel: UILabel!
    @IBOutlet fileprivate var secretSwitch: UISwitch!
    @IBOutlet fileprivate var facebookLabel: UILabel!
    @IBOutlet fileprivate var facebookSwitch: UISwitch!
    @IBOutlet fileprivate var twitterLabel: UILabel!
    @IBOutlet fileprivate var twitterSwitch: UISwitch!
    
    @IBOutlet fileprivate var publishButtonContainer: UIView!
    @IBOutlet fileprivate var publishButton: UIButton!
    @IBOutlet fileprivate var publishButtonActivityIndicator: UIActivityIndicatorView!
    
    var recordDuration: Float = 0.0
    fileprivate var player: AVAudioPlayer!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let parentVoc = VocCreationManager.shared.parentVoc
        let parentClient = VocCreationManager.shared.parentClient
        
        title = parentVoc != nil ? "publish_title_reply".getTranslation() : "publish_title_voc".getTranslation()
        navigationItem.initBackButton(target: self, selector: #selector(back))
        
        characterCountLabel.text = "\(TITLE_CHARACTER_MAX)"
        titleTextField.attributedPlaceholder = NSAttributedString(string:"title_textfield_placeholder".getTranslation(), attributes: [NSAttributedStringKey.foregroundColor: UIColor.init(red: 1.0, green: 1.0, blue: 1.0, alpha: 0.6)])
        titleTextField.delegate = self
        titleTextField.returnKeyType = .done
        titleTextField.autocapitalizationType = .sentences
        secretLabel.text = parentVoc != nil ? "publish_share_private_reply".getTranslation() : "publish_share_private_voc".getTranslation()
        
        publishButtonContainer.layer.cornerRadius = 24
        animatePublishButton(false)
        
        secretLabel.text = parentVoc != nil ? "publish_share_private_reply".getTranslation() : "publish_share_private_voc".getTranslation()
        facebookLabel.text = "publish_share_facebook".getTranslation()
        twitterLabel.text = "publish_share_twitter".getTranslation()
        
        if parentVoc != nil || parentClient != nil {
            secretView.isHidden = true
        }
        
        setupPlayerViews()
        
        if parentClient != nil {
            let color = parentClient!.themeColor
            navigationController?.navigationBar.barTintColor = color
            background.image = background.image?.withRenderingMode(.alwaysTemplate)
            background.tintColor = color
            publishButton.setTitleColor(color, for: .normal)
        } else {
            background.tintColor = nil
            publishButton.setTitleColor(UIColor.init(named: VoctagKeys.COLOR_MAIN), for: .normal)
        }
        
        LocationManager.shared.getLocationIfPossible(ForController: self)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        if let parentClient = VocCreationManager.shared.parentClient {
            publishButtonActivityIndicator.color = parentClient.themeColor
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        player?.pause()
    }
    
    @IBAction func onSliderValueChanged(sender: UISlider) {
        player.currentTime = Double(slider.value)
    }
    
    @IBAction func onClickPlay(sender: UIButton) {
        if player.isPlaying {
            player.pause()
        } else {
            player.play()
        }
        updatePlayerViews()
    }
    
    @IBAction func onSecretSwitchChanged(sender: UISwitch) {
        //nothing
    }
    
    @IBAction func onFacebookSwitchChanged(sender: UISwitch) {
        if sender.isOn {
            SharingHelper.shared.loginWithFacebook(viewController: self, completion: { [weak self] (success) in
                guard let _self = self else {
                    return
                }
                _self.facebookSwitch.isOn = success
            })
        }
    }
    
    @IBAction func onTwitterSwitchChanged(sender: UISwitch) {
        if sender.isOn {
            SharingHelper.shared.loginWithTwitter(completion: { [weak self] (success) in
                guard let _self = self else {
                    return
                }
                _self.twitterSwitch.isOn = success
            })
        }
    }
    
    @IBAction func onClickPublishButton(sender: UIButton) {
        publish()
    }
    
    @objc fileprivate func back() {
        self.navigationController?.popViewController(animated: true)
    }
    
    // MARK: - Private
    
    fileprivate func publish() {
        animatePublishButton(true)
        
        if let title = titleTextField.text, !title.isEmpty {
            VocCreationManager.shared.title = title
        }
        
        let booleans = [secretSwitch.isOn, facebookSwitch.isOn, twitterSwitch.isOn]
        publishVoc(booleans: booleans) { [weak self] (success) in
            guard let _self = self else {
                return
            }
            
            if success {
                VocCreationManager.shared.onPublished?()
                _self.dismiss(animated: true, completion: nil)
            } else {
                _self.animatePublishButton(false)
            }
        }
    }
    
    fileprivate func publishVoc(booleans: [Bool], completion: @escaping (Bool) -> Void) {
        if let parameters = VocCreationManager.shared.publishingParameters(secret: booleans[0]) {
            let successListener: (Voc) -> Void = { voc in
                DispatchQueue.main.async {
                    let title = voc.shareTitleString
                    
                    if let shareWebURL = voc.shareWebURL {
                        if booleans[1] {
                            SharingHelper.shared.shareToFacebook(url: shareWebURL, title: title)
                        }
                        if booleans[2] {
                            SharingHelper.shared.shareToTwitter(url: shareWebURL, title: title)
                        }
                    }
                    
                    //wenn vocsCounts bereits abgespeichert sind dann inkrementieren
                    if let parentVoc = VocCreationManager.shared.parentVoc {
                        if let count = PersistDataHelper.shared.loadVocCountForVoc(parentVoc) {
                            PersistDataHelper.shared.saveVocCountForVoc(vocID: parentVoc.vocID, count: count+1)
                        }
                    }
                    
                    if let parentClient = VocCreationManager.shared.parentClient {
                        if let count = PersistDataHelper.shared.loadVocCountForClient(parentClient) {
                            PersistDataHelper.shared.saveVocCountForClient(slug: parentClient.slug, count: count+1)
                        }
                    }
                    
                    VocCreationManager.shared.reset()
                    completion(true)
                }
            }
            
            if let client = VocCreationManager.shared.parentClient {
                RequestHelper.shared.uploadClientVoc(clientSlug: client.slug, parameters: parameters, success: successListener, error: {
                    completion(false)
                })
            } else {
                RequestHelper.shared.uploadVoc(parameters: parameters, success: successListener) {
                    completion(false)
                }
            }
        } else {
            completion(false)
        }
    }
    
    fileprivate func animatePublishButton(_ animated: Bool) {
        if animated {
            publishButtonActivityIndicator.isHidden = false
            publishButtonActivityIndicator.startAnimating()
            publishButton.isEnabled = false
            publishButton.setTitle("", for: .normal)
        } else {
            publishButtonActivityIndicator.isHidden = true
            publishButtonActivityIndicator.stopAnimating()
            publishButton.isEnabled = true
            publishButton.setTitle("publish_button".getTranslation(), for: .normal)
        }
    }
    
    // MARK: - Player Views
    
    fileprivate func setupPlayerViews() {
        slider.maximumValue = recordDuration
        slider.setup(target: self, selector: #selector(updateSlider))
        setupMediaPlayer()
    }
    
    fileprivate func setupMediaPlayer() {
        guard let fileURL = VocCreationManager.shared.recordedAudioFileURL(), FileManager.default.fileExists(atPath: fileURL.path) else {
            LoggingHelper.logError(ForClass: PublishViewController.self, with: "no recordAudioFileURL available")
            self.dismiss(animated: true, completion: nil)
            return
        }
        
        do {
            AudioRecorder.activateAudioSession()
            self.player = try AVAudioPlayer(contentsOf: fileURL)
            self.player.delegate = self
            self.player.numberOfLoops = 0
            self.player.prepareToPlay()
        } catch {
            LoggingHelper.logError(ForClass: PublishViewController.self, with: error.localizedDescription)
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    fileprivate func updatePlayerViews() {
        updateSlider()
        if player.isPlaying {
            slider.updater.isPaused = false
            playButton.setBackgroundImage(UIImage(named: "pause_circle_white_24dp"), for: .normal)
        } else {
            slider.updater.isPaused = true
            playButton.setBackgroundImage(UIImage(named: "play_circle_white_24dp"), for: .normal)
        }
    }
    
    @objc fileprivate func updateSlider() {
        slider.setProgress(progress: Float(player.currentTime))
    }
    
}

extension PublishViewController: UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let text = textField.text else {
            return true
        }
        
        let charCount = text.utf16.count + string.utf16.count - range.length
        characterCountLabel.text = "\(TITLE_CHARACTER_MAX - charCount)"
        return charCount < TITLE_CHARACTER_MAX
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.endEditing(true)
        return false
    }
    
}

extension PublishViewController: AVAudioPlayerDelegate {
    
    func audioPlayerDidFinishPlaying(_ player: AVAudioPlayer, successfully flag: Bool) {
        updatePlayerViews()
    }
    
}

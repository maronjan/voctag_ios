//
//  VoctagKeys.swift
//  voctag
//
//  Created by Jan Maron on 09.06.17.
//  Copyright © 2017 voctag. All rights reserved.
//

import UIKit

class VoctagKeys {
    
    static let DATA = "data"
    static let DEVICE_ID = "device_id"
    static let QUERY = "query"
    static let TOKEN = "token"
    static let REFRESH_TOKEN = "refresh_token"
    static let LATITUDE = "latitude"
    static let LONGITUDE = "longitude"
    static let APNS_DEVICE_TOKEN = "apns_device_token"
    static let REASON = "reason"
    static let AUDIO = "audio"
    static let TITLE = "title"
    static let PARENT_ID = "parent_id"
    static let SECRET = "secret"
    static let STATUS = "status"

    // MARK: - Request Header
    
    static let ACCEPT = "Accept"
    static let CONTENT_TYPE = "Content-Type"
    static let AUTHORIZATION = "Authorization"
    
    // MARK: - Persist Data
    
    static let PERSIST_USER = "persist_user"
    static let PERSIST_AUTH = "persist_auth"
    static let PERSIST_FILTER = "persist_filter"
    
    // MARK: - Authentication Object
    
    static let AUTHENTICATION_USER_ID = "user_id"
    static let AUTHENTICATION_AUTH_TOKEN = "auth_token"
    static let AUTHENTICATION_AUTH_TOKEN_KEYCHAIN_ACCESS = "auth_token"
    static let AUTHENTICATION_REFRESH_TOKEN = "refresh_token"
    
    // MARK: - OnboardinTypes
    
    static let ONBOARDING_TYPE_COMMUNITY = "community"
    static let ONBOARDING_TYPE_GENERAL = "general"
    static let ONBOARDING_TYPE_PROFILE = "profile"
    
    // MARK: - Filter Object
    
    static let FILTER_ORDER = "order"
    static let FILTER_QUERY = "query"
    static let FILTER_PAGE = "page"
    
    // MARK: - User Object
    
    static let USER_ID = "id"
    static let USER_ACCEPTED_TERMS = "accepted_terms"
    static let USER_NICKNAME = "nickname"
    static let USER_POINTS = "points"
    static let USER_LATITUDE = "latitude"
    static let USER_LONGITUDE = "longitude"
    static let USER_VOCS = "vocs"
    static let USER_REPLIES = "reply_vocs"
    static let USER_FAVORITES = "followed_vocs"
    
    // MARK: - Voc Object
    
    static let VOC_ID = "id"
    static let VOC_USER_ID = "user_id"
    static let VOC_USER_NICKNAME = "user_nickname"
    static let VOC_REPLIES_COUNT = "replies_count"
    static let VOC_TITLE = "title"
    static let VOC_PLAY_COUNT = "play_count"
    static let VOC_AUDIO_URL = "audio_url"
    static let VOC_DURATION = "duration"
    static let VOC_WEB_URL = "web_url"
    static let VOC_CITY = "city"
    static let VOC_SCORE = "score"
    static let VOC_CREATED_AT = "created_at"
    static let VOC_TRANSCRIPT = "transcript"
    
    static let VOC_PARENT_ID = "parent_id"
    static let VOC_VOTED = "voted"
    static let VOC_SECRET = "secret"
    static let VOC_SECRET_REPLIES_ALLOWED = "secret_replies_allowed"
    static let VOC_FOLLOWING = "following"
    static let VOC_REPLIES = "replies"
    
    static let VOC_CREATED_BY_CLIENT_OWNER = "created_by_channel_owner"
    static let VOC_PARENT_CLIENT = "client"
    
    // MARK: - Client Object
    
    static let CLIENT_SLUG = "slug"
    static let CLIENT_NAME = "name"
    static let CLIENT_DESCRIPTION = "description"
    static let CLIENT_MEMBERSHIPS_COUNT = "memberships_count"
    static let CLIENT_ICON_URLS = "logo_urls"
    static let CLIENT_ICON_SMALL = "small"
    static let CLIENT_ICON_MEDIUM = "medium"
    static let CLIENT_AUDIO_URL = "main_audio_url"
    static let CLIENT_THEMECOLOR = "theme_color"
    static let CLIENT_FOLLOWING = "member"
    static let CLIENT_VOCS = "vocs"
    static let CLIENT_VOCS_COUNT = "vocs_count"
    static let CLIENT_IS_CLOSED = "is_closed"
    static let CLIENT_HAS_ACCESS = "has_access"
    static let CLIENT_SALES_LINK = "sales_link"
    static let CLIENT_ACCESS_CODE = "access_code"
    static let CLIENT_SCOPE = "scope"
    
    // MARK: - Color
    
    static let COLOR_DARK = "darkColor"
    static let COLOR_BRIGHT = "brightColor"
    static let COLOR_BRIGHTLIGHT = "brightLight"
    static let COLOR_LIGHT = "lightColor"
    static let COLOR_MAIN_LIGHT = "lightMain"
    static let COLOR_MAIN = "mainColor"
    static let COLOR_MID = "midColor"
    static let COLOR_TABICONCOLOR = "tabIconColor"
    static let COLOR_GRADIENT = "gradientColor"
    
}

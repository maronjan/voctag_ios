//
//  Date.swift
//  voctag
//
//  Created by Jan Maron on 19.01.18.
//  Copyright © 2018 voctag. All rights reserved.
//

import Foundation

extension Date {
    
    func convertToServerTimeZone() -> Date {
        let offset = TimeInterval(TimeZone.current.secondsFromGMT(for: self))
        return self.addingTimeInterval(offset * -1)
    }
    
}

//
//  UIBarButtonItem.swift
//  voctag
//
//  Created by Jan Maron on 12.02.18.
//  Copyright © 2018 voctag. All rights reserved.
//

import UIKit

extension UINavigationItem {
    
    func initBackButton(target: Any?, selector: Selector?) {
        self.backBarButtonItem = nil
        self.leftBarButtonItem = UIBarButtonItem.init(image: UIImage.init(named: "back_24dp"), style: .plain, target: target, action: selector)
    }
    
}

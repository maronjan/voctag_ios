//
//  ClientListDataSource.swift
//  voctag
//
//  Created by Jan Maron on 25.04.18.
//  Copyright © 2018 voctag. All rights reserved.
//

import UIKit

class ClientListDataSource: NSObject {
    
    // MARK: - Reactive Variables
    
    var onFollowingChanged: (() -> Void)?
    
    // MARK: - ClientListDataSource
    
    fileprivate var clients = [Client]()
    fileprivate var followingClients = [Client]()
    fileprivate var otherClients = [Client]()
    
    fileprivate var tableView: UITableView!
    fileprivate var controller: UIViewController!
    
    init(clients: [Client], tableView: UITableView, controller: UIViewController) {
        super.init()
        self.clients = clients
        self.tableView = tableView
        self.controller = controller
        
        tableView.register(UINib(nibName: ClientTableViewCell.cellIdentifier, bundle: nil), forCellReuseIdentifier: ClientTableViewCell.cellIdentifier)
        tableView.dataSource = self
        tableView.delegate = self
        
        updateArrays(clients: clients)
    }
    
    func update(clients: [Client]) {
        self.clients = clients
        updateArrays(clients: clients)
    }
    /*
    fileprivate func openClient(_ client: Client) {
        controller.tabBarController?.tabBar.isHidden = true
        
        let detailController = StoryboardManager.clientDetailViewController()
        detailController.client = client
        detailController.onFollowingChanged = self.onFollowingChanged
        controller.navigationController?.pushViewController(detailController, animated: false)
    }
    */
    fileprivate func updateArrays(clients: [Client]) {
        followingClients.removeAll()
        otherClients.removeAll()
        
        for client in clients {
            if client.following {
                followingClients.append(client)
            } else {
                otherClients.append(client)
            }
        }
        
        tableView.reloadData()
    }
    
}

extension ClientListDataSource: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if followingClients.isEmpty {
            return 1
        } else {
            return 2
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: ClientTableViewCell.cellIdentifier) as! ClientTableViewCell
        
        var client: Client!
        if !followingClients.isEmpty && indexPath.section == 0 {
            client = followingClients[indexPath.row]
        } else {
            client = otherClients[indexPath.row]
        }
        
        cell.setup(withClient: client, TableView: tableView, andViewController: controller)
        cell.onPressedCell = {
            let controller = VoctagAppDelegate.shared.tabBarController!
            controller.openClient(client, followingListener: self.onFollowingChanged)
        }
        cell.onFollowingChanged = {
            self.updateArrays(clients: self.clients)
        }

        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if !followingClients.isEmpty && section == 0 {
            return followingClients.count
        }
        return otherClients.count
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if !followingClients.isEmpty && section == 0 {
            return "community_divider_myclients".getTranslation()
        }
        return "community_divider_suggestions".getTranslation()
    }
    
}

extension ClientListDataSource: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return ClientTableViewCell.height
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 54.0
    }
    
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        if let headerView = view as? UITableViewHeaderFooterView {
            headerView.textLabel?.font = UIFont.boldSystemFont(ofSize: 22)
            headerView.textLabel?.textColor = UIColor.init(named: VoctagKeys.COLOR_LIGHT)
            headerView.backgroundColor = UIColor.init(named: VoctagKeys.COLOR_BRIGHT)
        }
    }
    
}

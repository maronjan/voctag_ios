//
//  FeedDetailDataSource.swift
//
//  Created by Jan Maron on 20.12.17.
//

import UIKit

class FeedDetailDataSource: NSObject {
    
    // MARK: - Reactive Variables
    
    var onDeletedVoc: (() -> Void)?
    
    // MARK: - FeedDetailDataSource
    
    fileprivate var vocs: [Voc]!
    fileprivate var client: Client?
    fileprivate var tableView: UITableView!
    fileprivate var controller: UIViewController!
    
    init(vocs: [Voc], client: Client?, tableView: UITableView, controller: UIViewController) {
        super.init()
        self.vocs = vocs
        self.client = client
        self.tableView = tableView
        self.controller = controller
        
        tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 86, right: 0)
        tableView.register(UINib(nibName: MainVocTableViewCell.cellIdentifier, bundle: nil), forCellReuseIdentifier: MainVocTableViewCell.cellIdentifier)
        tableView.register(UINib(nibName: VocTableViewCell.cellIdentifier, bundle: nil), forCellReuseIdentifier: VocTableViewCell.cellIdentifier)
        tableView.dataSource = self
        tableView.delegate = self
    }
    
    func update(vocs: [Voc]) {
        self.vocs = vocs
        tableView.reloadData()
        PlaylistManager.shared.onPlaylistDidChange = {
            self.tableView.reloadData()
        }
    }
    /*
    func openVoc(_ voc: Voc) {
        let detailController = StoryboardManager.feedDetailViewController()
        detailController.voc = voc
        let navigationController = UINavigationController(rootViewController: detailController)
        navigationController.setupForVoctag()
        controller.present(navigationController, animated: true, completion: nil)
    }
    */
}

extension FeedDetailDataSource: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let voc = vocs[indexPath.row]
        if client != nil {
            voc.parentClient = client
        }
        
        if indexPath.row == 0 {
            //main voc
            let cell = tableView.dequeueReusableCell(withIdentifier: MainVocTableViewCell.cellIdentifier) as! MainVocTableViewCell
            cell.setup(withVoc: voc, TableView: tableView, andViewController: controller)
            return cell
        } else {
            //replies
            let cell = tableView.dequeueReusableCell(withIdentifier: VocTableViewCell.cellIdentifier) as! VocTableViewCell
            cell.setup(withVoc: voc, TableView: tableView, andViewController: controller)
            cell.onPressedCell = { _ in
                VoctagAppDelegate.shared.tabBarController!.openVoc(voc, shouldScrollToVoc: nil, deletedVocListener: nil)
            }

            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return vocs.count
    }
    
}

extension FeedDetailDataSource: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0 {
            return MainVocTableViewCell.height
        } else {
            return VocTableViewCell.height
        }
    }
    
}

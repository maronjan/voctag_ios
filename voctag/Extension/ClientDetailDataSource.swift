//
//  ClientDetailDataSource.swift
//  voctag
//
//  Created by Jan Maron on 25.04.18.
//  Copyright © 2018 voctag. All rights reserved.
//

import UIKit

class ClientDetailDataSource: NSObject {
    
    // MARK: - Reactive Variables
    
    var onDeletedVoc: (() -> Void)?
    var onFollowingChanged: (() -> Void)?
    var onLoadMore: ((Int) -> Void)?
    
    // MARK: - ClientDetailDataSource
    
    fileprivate let VOCS_PER_PAGE = 25
    fileprivate var pageIndex = 1
    
    fileprivate var client: Client!
    fileprivate var tableView: UITableView!
    fileprivate var controller: UIViewController!
    
    init(client: Client, tableView: UITableView, controller: UIViewController) {
        super.init()
        self.client = client
        self.tableView = tableView
        self.controller = controller
        
        tableView.register(UINib(nibName: ClientDetailTableViewCell.cellIdentifier, bundle: nil), forCellReuseIdentifier: ClientDetailTableViewCell.cellIdentifier)
        tableView.register(UINib(nibName: VocTableViewCell.cellIdentifier, bundle: nil), forCellReuseIdentifier: VocTableViewCell.cellIdentifier)
        tableView.dataSource = self
        tableView.delegate = self
    }
    
    func update(client: Client, resetPageIndex: Bool = true) {
        if resetPageIndex {
            pageIndex = 1
        }
        
        self.client = client
        tableView.reloadData()
        PlaylistManager.shared.onPlaylistDidChange = {
            self.tableView.reloadData()
        }
    }
    /*
    func openVoc(_ voc: Voc) {
        let detailController = StoryboardManager.feedDetailViewController()
        detailController.voc = voc
        detailController.onDeletedVoc = onDeletedVoc
        let navigationController = UINavigationController(rootViewController: detailController)
        navigationController.setupForVoctag()
        controller.present(navigationController, animated: true, completion: nil)
    }
    */
}

extension ClientDetailDataSource: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: ClientDetailTableViewCell.cellIdentifier) as! ClientDetailTableViewCell
            cell.setup(withClient: client, TableView: tableView, andViewController: controller)
            //cell.onFollowingChanged = self.onFollowingChanged
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: VocTableViewCell.cellIdentifier) as! VocTableViewCell
            let voc = client.vocs[indexPath.row-1]
            cell.setup(withVoc: voc, TableView: tableView, andViewController: controller)
            cell.onPressedCell = { _ in
                VoctagAppDelegate.shared.tabBarController!.openVoc(voc, shouldScrollToVoc: nil, deletedVocListener: self.onDeletedVoc)
            }
            
            //pagination
            if client.vocs.count == indexPath.row {
                //reached last item
                if client.vocs.count == pageIndex * VOCS_PER_PAGE {
                    pageIndex += 1
                    onLoadMore?(pageIndex)
                }
            }
            
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return client.vocs.count+1
    }
    
}

extension ClientDetailDataSource: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0 {
            return ClientDetailTableViewCell.height
        } else {
            return VocTableViewCell.height
        }
    }
    
}

//
//  CustomUISlider.swift
//  voctag
//
//  Created by Jan Maron on 21.02.18.
//  Copyright © 2018 voctag. All rights reserved.
//

import UIKit

class CustomUISlider: UISlider {
    
    var updater: CADisplayLink!
    
    override func trackRect(forBounds bounds: CGRect) -> CGRect {
        var customBounds = super.trackRect(forBounds: bounds)
        customBounds.size.height = 6
        customBounds.origin.y -= 2
        return customBounds
    }
    
    func setup(target: Any, selector: Selector) {
        self.setValue(0.0, animated: false)
        self.setThumbImage(UIImage.init(named: "slider_thumb"), for: .normal)
        
        //init cadisplaylink
        updater = CADisplayLink(target: target, selector: selector)
        updater.add(to: RunLoop.current, forMode: RunLoopMode.commonModes)
        updater.isPaused = true
    }
    
    func setProgress(progress: Float) {
        self.setValue(progress, animated: false)
    }

}

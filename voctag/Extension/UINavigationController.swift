//
//  UINavigationController.swift
//  voctag
//
//  Created by Jan Maron on 27.02.18.
//  Copyright © 2018 voctag. All rights reserved.
//

import UIKit

extension UINavigationController {
    
    func setupForVoctag() {
        self.navigationBar.tintColor = UIColor.white
        self.navigationBar.barTintColor = UIColor.init(named: VoctagKeys.COLOR_GRADIENT)
        self.navigationBar.isTranslucent = false
        self.navigationBar.shadowImage = UIImage()
        self.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor : UIColor.white]
    }
    
}

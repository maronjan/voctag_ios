//
//  String.swift
//  voctag
//
//  Created by Jan Maron on 14.07.17.
//  Copyright © 2017 voctag. All rights reserved.
//

import Foundation

extension String {
    
    func getTranslation() -> String {
        return NSLocalizedString(self, comment: "")
    }
    
}

//
//  VocsFeedDetailDataSource.swift
//  voctag
//
//  Created by Jann Driessen on 19.10.16.
//  Copyright © 2016 voctag. All rights reserved.
//

import UIKit

class FeedDataSource: NSObject {
    
    // MARK: - Reactive Variables
    
    var onDeletedVoc: (() -> Void)?
    var onLoadMore: ((Int) -> Void)?
    
    // MARK: - FeedDataSource
    
    fileprivate let VOCS_PER_PAGE = 25
    fileprivate var pageIndex = 1
    
    fileprivate var vocs: [Voc]!
    fileprivate var tableView: UITableView!
    fileprivate var controller: UIViewController!
    
    init(vocs: [Voc], tableView: UITableView, controller: UIViewController) {
        super.init()
        self.tableView = tableView
        self.vocs = vocs
        self.controller = controller
        
        tableView.contentInset = UIEdgeInsets(top: 16, left: 0, bottom: 8, right: 0)
        tableView.register(UINib(nibName: VocTableViewCell.cellIdentifier, bundle: nil), forCellReuseIdentifier: VocTableViewCell.cellIdentifier)
        tableView.dataSource = self
        tableView.delegate = self
    }
    
    func update(vocs: [Voc], resetPageIndex: Bool = true) {
        if resetPageIndex {
            pageIndex = 1
        }
        
        self.vocs = vocs
        tableView.reloadData()
        PlaylistManager.shared.onPlaylistDidChange = {
            self.tableView.reloadData()
        }
    }
    /*
    func openVoc(_ voc: Voc, shouldScrollToVoc: Voc?) {
        let detailController = StoryboardManager.feedDetailViewController()
        detailController.voc = voc
        detailController.shouldScrollToVoc = shouldScrollToVoc
        detailController.onDeletedVoc = onDeletedVoc
        let navigationController = UINavigationController(rootViewController: detailController)
        navigationController.setupForVoctag()
        controller.present(navigationController, animated: true, completion: nil)
    }
    */
}

extension FeedDataSource: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: VocTableViewCell.cellIdentifier) as! VocTableViewCell
        let voc = vocs[indexPath.row]
        cell.setup(withVoc: voc, TableView: tableView, andViewController: controller)
        cell.onPressedCell = { isMyReply in
            let controller = VoctagAppDelegate.shared.tabBarController!
            if isMyReply {
                controller.openVoc(Voc.init(withVocID: voc.parentID!), shouldScrollToVoc: voc, deletedVocListener: self.onDeletedVoc)
            } else {
                controller.openVoc(voc, shouldScrollToVoc: nil, deletedVocListener: self.onDeletedVoc)
            }
        }
        
        //pagination
        if vocs.count == indexPath.row+1 {
            //reached last item
            if vocs.count == pageIndex * VOCS_PER_PAGE {
                pageIndex += 1
                onLoadMore?(pageIndex)
            }
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return vocs.count
    }
    
}

extension FeedDataSource: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return VocTableViewCell.height
    }
    
}

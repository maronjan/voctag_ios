//
//  UIColor.swift
//  voctag
//
//  Created by Jan Maron on 26.04.18.
//  Copyright © 2018 voctag. All rights reserved.
//

import UIKit

extension UIColor {
    
    convenience init(rgb: Int) {
        self.init(
            red: CGFloat((rgb & 0xFF0000) >> 16) / 0xFF,
            green: CGFloat((rgb & 0x00FF00) >> 8) / 0xFF,
            blue: CGFloat(rgb & 0x0000FF) / 0xFF,
            alpha: 1.0
        )
    }
    
}
